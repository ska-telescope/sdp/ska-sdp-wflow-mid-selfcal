.. _docker_singularity:

*****************************
Docker and singularity images
*****************************

The pipeline currently uses
`WSClean <https://wsclean.readthedocs.io/en/latest/index.html>`_ and
`DP3 <https://dp3.readthedocs.io/en/latest/>`_ to make images,
generate model visibilities from them, and perfom calibration against the
models.

If they are installed locally, the pipeline can run those programs directly
on bare metal. Otherwise, it can also run them within a singularity container,
but a singularity image file with DP3 and WSClean installed must be built
first.

We maintain both a Dockerfile and Singularity recipe for those programs.


Singularity build
=================

The recommended method is to directly use the Singularity recipe file provided.
Navigate to the base directory of the repository from a terminal and run:

.. code-block::

    cd docker/
    singularity build <MY_IMAGE_FILE> Singularity

You may have to run with ``sudo`` or add the ``--fakeroot`` option depending on how
singularity was installed on your machine.


Docker build
============

To build a docker image, navigate to the base directory of the repository from 
a terminal and run:

.. code-block::

    cd docker/
    docker build -f Dockerfile --tag <MY_IMAGE_TAG> .


Singularity recipe
==================

.. literalinclude:: ../../docker/Singularity


Dockerfile
==========

.. literalinclude:: ../../docker/Dockerfile
    :language: docker