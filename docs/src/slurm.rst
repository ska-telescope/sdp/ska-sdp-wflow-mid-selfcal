.. _slurm:

****************
Running on SLURM
****************

We maintain a tested and approved SLURM batch script for running the pipeline
on the `CSD3 cluster <https://docs.hpc.cam.ac.uk/hpc/>`_ with multiple icelake
nodes. It uses dask distribution for the calibration stage, and
MPI distribution for the imaging stage.

Feel free to adapt it to your needs.

.. literalinclude:: ../../scripts/csd3_batch_script.sh
    :language: bash
