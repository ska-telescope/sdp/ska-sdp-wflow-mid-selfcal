.. _pipeline:

**************
Pipeline Usage
**************

Introduction
============

Self-calibration is a loop between imaging and calibration stages. Imaging stages
are used to refine our knowledge of the true sky, while calibration stages
improve our estimate of the corrupting effects of the signal chain, including
those of the telescope and the ionosphere. In other words, this means alternatively:

* Fitting a model of the corrupting effects to the data, assuming a fixed model of the sky (Calibration)
* Fitting a model of the sky to the data, assuming a fixed model of the corrupting effects (Imaging)

**We define a self-calibration cycle as a Calibration stage followed by an Imaging stage.**
This means that a bootstrap sky model, i.e. a list of sources with their position and fluxes, must be
available to start self-calibration. There are two options:

1. The user can directly provide a sky model as a text file in `sourcedb format <makesourcedb_>`_, via the command line.
2. Otherwise, the pipeline runs an initial imaging stage. The resulting clean image
   is automatically thresholded, and a list of sufficiently bright sources identified
   to serve as the bootstrap sky model.

.. _makesourcedb: https://www.astron.nl/lofarwiki/doku.php?id=public:user_software:documentation:makesourcedb

This is illustrated on this simplified flowchart:

.. image:: _static/simplified_flowchart.png
   :width: 100%


Configuration
=============

Overview
--------

The pipeline needs a configuration file in YAML format to specify various options,
including options to be provided to DDECal and WSClean. While the pipeline
does forcibly set some of these options, many are freely adjustable by the user,
and independently so for each selfcal cycle.

The idea here is to provide an "expert interface" where many parameters are adjustable,
as we expect that a lot of experimentation is going to be necessary to find a good calibration
stragegy for SKA Mid.

.. note::
  
  The configuration file schema will likely be subject to backwards-incompatible
  changes in the future, as we are still early in the development process.


Config validation
-----------------

`JSON Schema <https://json-schema.org/>`_ is used to enforce validity of configuration files.
On startup, the pipeline will immediately throw an error if parameter names are misspelled,
or if incorrect choices of parameter values are provided. Individual parameters to DP3
and WSClean are covered by those checks.

.. note::

  The schema validation should cover most possible syntax mistakes,
  but does not prevent from specifying "scientifically bad" combinations of
  parameters to DP3 or WSClean for example.


Additionally, a command-line app is provided to manually check a configuration file.
See the :ref:`additional_apps` page for details. We **highly** recommend using it
before submitting jobs on an HPC cluster.


Examples
--------

We maintain valid and documented configuration file examples in the ``config/`` directory
of the repository. A model configuration file is reproduced below.

For full details on tweakable DDECal and WSClean parameters, please refer to:

* The `DP3 DDECal documentation <https://dp3.readthedocs.io/en/latest/steps/DDECal.html>`_.
* The `WSClean documentation <https://wsclean.readthedocs.io/en/latest/>`_, and the command-line help of the ``wsclean`` app.

.. literalinclude:: ../../config/aa2mid_benchmark.yml
  :language: yaml


Wideband deconvolution
======================

Wideband deconvolution is the method employed by WSClean to properly take into account
the fact that the flux of sources may vary significantly as a function of frequency.
When enabled, WSClean internally separates the data into several sub-bands which are
imaged independently but then jointly deconvolved.

The method is explained `in the WSClean documentation. <wideband_deconvolution_>`_

.. _wideband_deconvolution: https://wsclean.readthedocs.io/en/latest/wideband_deconvolution.html

The wideband deconvolution parameters are automatically set by the pipeline based
on the configuration parameter ``max_fractional_bandwidth``.
Fractional bandwidth is the ratio between total bandwidth and centre frequency.

The behaviour is:

* If the fractional bandwidth of the data exceeds ``max_fractional_bandwidth``, choose
  the WSClean parameter ``-channels-out`` so that every imaging sub-band has a
  fractional bandwidth below that threshold.
* Otherwise, do not enable wideband deconvolution


Description of parameters
=========================

The help text for the app can be obtained by running
``mid-selfcal-dd --help``, and is reproduced below:

.. code-block:: none

  usage: mid-selfcal-dd [-h] [--version] [--singularity-image SINGULARITY_IMAGE] [--dask-scheduler DASK_SCHEDULER]
                        [--mpi-hosts MPI_HOSTS [MPI_HOSTS ...]] [--outdir OUTDIR] --config CONFIG [--sky-model SKY_MODEL] --num-pixels
                        NUM_PIXELS --pixel-scale PIXEL_SCALE
                        input_ms

  Launch the SKA Mid direction-dependent self-calibration pipeline

  positional arguments:
    input_ms              Input measurement set.

  optional arguments:
    -h, --help            show this help message and exit
    --version             show program's version number and exit
    --singularity-image SINGULARITY_IMAGE
                          Optional path to a singularity image file with both WSClean and DP3 installed. If specified, run WSClean and
                          DP3 inside singularity containers; otherwise, run them on bare metal. (default: None)
    --dask-scheduler DASK_SCHEDULER
                          Optional dask scheduler address to which to submit jobs. If specified, any eligible pipeline step will be
                          distributed on the associated Dask cluster. (default: None)
    --mpi-hosts MPI_HOSTS [MPI_HOSTS ...]
                          List of hostnames on which to run MPI-eligible pipeline step. If the list is of length 1 or less, MPI is not
                          used. (default: None)
    --outdir OUTDIR       Directory path in which to write data products and temporary files; it will be created if necessary, with
                          all its parents. If not specified, create a uniquely named subdir in the current working directory, named
                          selfcal_YYYYMMDD_HHMMSS_<microseconds> (default: None)
    --config CONFIG       Path to the pipeline configuration file. (default: None)
    --sky-model SKY_MODEL
                          Optional path to bootstrap sky model file in sourcedb format. If provided, use this sky model to bootstrap
                          calibration. Otherwise, the pipeline will run an initial imaging stage to make such a bootstrap sky model.
                          NOTE: the source fluxes must be **apparent** fluxes, that is with the primary beam attenuation already
                          applied. (default: None)
    --num-pixels NUM_PIXELS
                          Output image size in pixels. (default: None)
    --pixel-scale PIXEL_SCALE
                          Scale of a pixel in arcseconds. (default: None)


Input and output paths
----------------------

The input measurement set file is specified as the only positional argument.

The user has the choice of specifying an output directory for the pipeline to store its products, as well as any temporary files created by DP3 and WSClean.
If ``--outdir`` is not provided, a uniquely-named sub-directory of the current working directory will be created. Currently,
the naming pattern is ``selfcal_YYYMMDD_HHMMSS_<MICROSECONDS>``, i.e. based on the date and time the pipeline started processing.
If ``--outdir`` is a directory that does not exist yet, it is created with its parent directories.


Optional: running WSClean and DP3 within singularity containers
---------------------------------------------------------------

By default, the pipeline assumes that WSClean and DP3 are installed on the host and attempts to run them on bare metal.
Alternatively, it is possible to specify a singularity image file via ``--singularity-image`` inside which both WSClean and DP3 are expected to be installed.
In that case, the pipeline will run WSClean and DP3 inside singularity containers spun up from that image file,
i.e. automatically generate and execute the right singularity commands with the appropriate bind mount points.

Optional: dask distribution
---------------------------

The pipeline runs on a single node by default, but may use an existing multi-node dask cluster to distribute some of the computation.
To enable this, pass the address of the dask scheduler via ``--dask-scheduler`` as a string ``HOSTNAME:PORT``.
The only processing stage currently eligible for dask distribution is the calibration stage, where every dask worker processes distinct time chunks of the data.
This scales very well with the number of nodes involved.

.. note::

  Launching a dask cluster on your machine or an HPC facility is done separately; for the latter, please follow the instructions on the :ref:`slurm` page.
  It is implicitly assumed that all the nodes / dask workers involved have access to a common filesystem, as is customary on most HPC facilities.


Optional: MPI distribution
--------------------------

Imaging is eligible for MPI distribution, where every node processes distinct frequency bands of the data.
To enable this, pass the list of host names to use as a space-separated list via ``--mpi-hosts``.


Configuration file
------------------

A custom YAML configuration file for the pipeline **must** be provided (see above section).
All the parameters of DP3's DDECal and WSClean can be freely tweaked. This is where for example one might select:

* What constraints are made on the Jones matrices (e.g. scalar vs. diagonal vs. full Jones), or the solution intervals in time and frequency.
* The weighting mode used for imaging (e.g. uniform vs. briggs), whether to use multiscale deconvolution, etc.


Optional: Initial sky model file
---------------------------------

By default, the pipeline starts by imaging the field without any direction-dependent corrections, in order to make
an initial sky model to bootstrap direction-dependent selfcal. However, it is possible to skip that stage and instead
supply a custom sky model file in `sourcedb format <makesourcedb_>`_ via the ``--sky-model`` option.

.. note::

  The source fluxes provided in this file must be the **apparent** fluxes, that is after applying the primary beam attenuation factor.


Image size and scale
--------------------

The parameters of the output image must be specified via:

* ``--num-pixels``, the width and height of the image in pixels. Only square images can be made at this time.
* ``--pixel-scale``, the angular scale of a pixel in arcseconds.


Data Products
=============

Temporary and superfluous files are automatically cleaned up at the end of the run,
even in case of a crash. Intermediate outputs for each self-calibration cycle are stored in their own
subdirectories; most of the files they contain are preserved for inspection and troubleshooting.

Final data products are written in the base output directory of the pipeline.
The following files are always produced in a successful run:

.. csv-table:: Description of output files
   :file: ./data_products_table.csv
   :widths: 50, 50
   :header-rows: 1

