import atexit
import signal
from contextlib import contextmanager
from typing import Any, Callable

from ska_sdp_wflow_mid_selfcal import __version__
from ska_sdp_wflow_mid_selfcal.logutils import LOGGER


@contextmanager
def graceful_pipeline_shutdown(cleanup_func: Callable[[], Any]):
    """
    This context manager guarantees the execution of a custom cleanup function
    and proper handling of exceptions raised within the context. It also
    listens for SIGINT and SIGTERM signals for graceful shutdown.

    Args:
        cleanup_func: A custom function that takes no
            arguments and is responsible for cleanup operations, such as
            deleting temporary files.

    Raises:
        SystemExit: If a SIGINT or SIGTERM signal is received, it raises
            SystemExit with the corresponding signal number.
        Exception: Otherwise, any exception raised within the context is
            re-raised.
    """

    def exit_handler(signum, __):
        signame = signal.Signals(signum).name
        LOGGER.warning(f"{signame} received, shutting down")
        raise SystemExit(signum)

    signal.signal(signal.SIGTERM, exit_handler)
    signal.signal(signal.SIGINT, exit_handler)
    atexit.register(cleanup_func)

    try:
        LOGGER.info(f"Running version: {__version__}")
        yield  # Runs code within context manager's scope
        LOGGER.info("Pipeline run: SUCCESS")

    # pylint: disable=broad-exception-caught
    except (Exception, SystemExit) as err:
        LOGGER.exception(f"Error: {err!r}")
        LOGGER.error("Pipeline run: FAIL")
        raise
