from __future__ import annotations

import math
from dataclasses import dataclass
from typing import Any, Iterable, Iterator, Sequence

import astropy.units as u
import numpy as np
from astropy.coordinates import SkyCoord
from numpy.typing import NDArray
from sklearn.cluster import KMeans

from ska_sdp_wflow_mid_selfcal.coordinate_conversions import (
    orthographic_from_sky,
    sky_from_orthographic,
)
from ska_sdp_wflow_mid_selfcal.field import Field
from ska_sdp_wflow_mid_selfcal.skymodel import SkyModel
from ska_sdp_wflow_mid_selfcal.voronoi import apply_voronoi


@dataclass
class Patch:
    """
    Defines a polygonal region on the sky for the purposes of calibration.
    """

    name: str
    """
    Name of the Patch, enforced to be unique.
    Used for exporting to sourcedb format.
    """

    calibration_direction: SkyCoord
    """The coordinates of the calibration direction for this Patch, as a scalar
    SkyCoord object (i.e. containing a single point). In practice this can
    be any reference point inside the patch, such as its brightest source."""

    vertices: SkyCoord
    """Vertices of the Patch, enumerated along its circumference, as a vector
    SkyCoord object (i.e. containing multiple points)."""

    vertices_lm: NDArray
    """
    Vertices in (l, m) coordinates, centered on the imaging centre, as an array
    of shape (num_vertices, 2).
    """

    def as_sourcedb_entry(self) -> dict[str, Any]:
        """
        Convert to a dictionary for exporting to sourcedb file. See SourceDB
        class for specifications of said dictionary.
        """
        return {
            "Patch": self.name,
            "Ra": float(self.calibration_direction.ra.deg),
            "Dec": float(self.calibration_direction.dec.deg),
        }

    def __hash__(self) -> int:
        return hash(self.name)


class Tesselation:
    """
    Represents a set of calibration Patches that constitute a partition of the
    imaging field.

    Exposes the basic interface of a set (len, iter, membership test) but not
    binary set operators, which would not make sense.
    """

    def __init__(
        self,
        calibration_directions: Sequence[SkyCoord],
        polygons_sky: Sequence[SkyCoord],
        origin: SkyCoord,
    ) -> Tesselation:
        """
        Create a new Tesselation instance from the given calibration directions
        and corresponding polygons on sky. Patches are given names of the form
        `patch_{INDEX}`.

        Args:
            calibration_directions: Sequence of N calibration directions, can
                be given as a single SkyCoord object. A calibration direction
                can be any reference point inside a calibration patch, for
                example its brightest source.
            polygons_sky: Corresponding sequence of sky polygons. Each element
                is a SkyCoord wrapping the coordinates of a polygon's vertices,
                which must be enumerated along its circumference (enumeration
                direction does not matter).
            origin: SkyCoord of the tangent plane origin, i.e. the imaging
                centre.
        """
        if not len(calibration_directions) == len(polygons_sky):
            raise ValueError(
                "The same number of calibration directions and of polygons "
                "must be provided"
            )

        patches = []
        for index, (cal_dir, vertices) in enumerate(
            zip(calibration_directions, polygons_sky)
        ):
            patch = Patch(
                name=f"patch_{index:03d}",
                calibration_direction=cal_dir,
                vertices=vertices,
                vertices_lm=orthographic_from_sky(vertices, origin),
            )
            patches.append(patch)

        self._origin = origin
        # Use a dict as an ordered set. We do this to enforce that the
        # the iteration order remains the same as in the `patches` argument
        # passed to this function. Makes testing easier.
        self._patches = dict.fromkeys(patches)

    def __iter__(self) -> Iterator[Patch]:
        return iter(self._patches)

    def __len__(self) -> int:
        return len(self._patches)

    def __contains__(self, patch: Patch) -> bool:
        return patch in self._patches

    @property
    def origin(self) -> SkyCoord:
        """
        SkyCoord of the tangent plane origin, i.e. the imaging centre.
        """
        return self._origin

    def as_ds9_text(self) -> str:
        """
        Convert to text in DS9 region format. Calibration directions are
        exported as points, and patch vertices as polygons.
        """
        lines = [
            'global color=green width=1 font="helvetica 12 normal roman"',
            "fk5",
        ]
        for patch in self:
            # First, write vertices as a polygon
            ra_arr = patch.vertices.ra.to(u.deg).value
            dec_arr = patch.vertices.dec.to(u.deg).value

            # NOTE: WSClean cannot read numbers in scientific notation, so we
            # enforce a fixed-digit representation.
            # np.format_float_positional() is good here because it chooses the
            # smallest possible number of digits.
            radec_str = ",".join(
                np.format_float_positional(x)
                for x in _interleave(ra_arr, dec_arr)
            )
            lines.append(f"polygon({radec_str})")

            # Then, write associated calibration direction as a point
            ra = patch.calibration_direction.ra.to(u.deg).value
            dec = patch.calibration_direction.dec.to(u.deg).value
            ra_str = np.format_float_positional(ra)
            dec_str = np.format_float_positional(dec)
            lines.append(f'point({ra_str},{dec_str}) # text="{patch.name}"')

        return "\n".join(lines) + "\n"

    def save_ds9(self, fname: str) -> None:
        """
        Save this Tesselation as a DS9 region file. Calibration directions are
        exported as points, and patch vertices as polygons.
        """
        with open(fname, "w") as fobj:
            fobj.write(self.as_ds9_text())

    @classmethod
    def voronoi_brightest(
        cls, field: Field, sky_model: SkyModel, num_patches: int
    ) -> Tesselation:
        """
        Create a voronoi tesselation of the field around the `num_patches`
        brightest sources in `sky_model`.
        """
        filtered_model = sky_model.within_box(
            field.imaging_centre, field.fov_deg, field.fov_deg
        ).brightest(num_patches)
        calibration_directions = filtered_model.source_coords()
        return create_voronoi_tesselation(field, calibration_directions)

    @classmethod
    def kmeans(
        cls, field: Field, sky_model: SkyModel, num_patches: int
    ) -> Tesselation:
        """
        Create a voronoi tesselation of the field around `num_patches`
        centroids determined with k-means clustering of the `sky_model`.
        """
        filtered_model = sky_model.within_box(
            field.imaging_centre, field.fov_deg, field.fov_deg
        )

        # eliminating non-physical negative fluxes from skymodel
        source_fluxes_positive = np.maximum(
            0.0, filtered_model.source_fluxes()
        )

        # number of patches cannot exceed number of sky sources
        num_patches = min(num_patches, len(source_fluxes_positive))

        kmeans = KMeans(n_clusters=num_patches, init="k-means++", n_init=1)
        kmeans.fit(
            filtered_model.source_coords_lm(field.imaging_centre),
            sample_weight=source_fluxes_positive,
        )
        calibration_directions = sky_from_orthographic(
            kmeans.cluster_centers_, field.imaging_centre
        )
        return create_voronoi_tesselation(field, calibration_directions)

    @classmethod
    def square_grid(cls, field: Field, num_patches: int) -> Tesselation:
        """
        Create a N x N square grid tesselation of the field, choosing `N` so
        that N^2 is the smallest square number larger than `num_patches`.

        The square grid is defined in the tangent plane around the field's
        imaging centre using an orthographic projection.
        """
        return create_square_grid_tesselation(field, num_patches)

    @classmethod
    def create(
        cls,
        field: Field,
        sky_model: SkyModel,
        num_patches: int,
        method: str = "voronoi_brightest",
    ) -> Tesselation:
        """
        One-stop method to create a new Tesselation using the specified method.
        """
        dispatch_dict = {
            "voronoi_brightest": (
                Tesselation.voronoi_brightest,
                (field, sky_model, num_patches),
            ),
            "square_grid": (Tesselation.square_grid, (field, num_patches)),
            "kmeans": (Tesselation.kmeans, (field, sky_model, num_patches)),
        }
        if method not in dispatch_dict:
            valid_methods = list(dispatch_dict.keys())
            raise ValueError(
                f"Tesselation method must be one of {valid_methods!r}"
            )
        func, args = dispatch_dict[method]
        return func(*args)


def _interleave(it1: Iterable, it2: Iterable) -> Iterable:
    """
    Alternatively yield an element of it1 then of it2.
    """
    for x, y in zip(it1, it2):
        yield x
        yield y


def create_square_grid_tesselation(
    field: Field, num_patches: int
) -> Tesselation:
    """
    Make a Tesselation by splitting the field into a N x N square grid, where
    N^2 is the smallest square number larger than `num_patches`.

    The square grid is defined in the tangent plane around the imaging centre
    using an orthographic projection.
    """
    num_patches = max(num_patches, 1)
    num_patches_sqrt = math.ceil(math.sqrt(num_patches))
    num_patches = num_patches_sqrt**2

    # Define patch centres in (l, m) coordinates
    width = 2 * field.l_max
    step = width / num_patches_sqrt
    l_min = m_min = -(width - step) / 2

    patch_centres_lm = []
    for i in range(0, num_patches_sqrt):
        for j in range(0, num_patches_sqrt):
            centre_l = l_min + i * step
            centre_m = m_min + j * step
            patch_centres_lm.append((centre_l, centre_m))

    patch_centres_sky = sky_from_orthographic(
        patch_centres_lm, field.imaging_centre
    )
    return create_voronoi_tesselation(field, patch_centres_sky)


def create_voronoi_tesselation(
    field: Field, calibration_directions: SkyCoord
) -> Tesselation:
    """
    Create a Tesselation of the given field, by making a Voronoi mesh whose
    cells are centered around the given calibration directions.

    The Voronoi mesh is calculated in the tangent plane around the imaging
    centre using an orthographic projection. The tangent plane coordinates of
    the voronoi cell vertices are then deprojected onto the sky.
    """
    directions_lm = orthographic_from_sky(
        calibration_directions, field.imaging_centre
    )

    vertices_lm = apply_voronoi(
        directions_lm,
        x_min=-field.l_max,
        y_min=-field.m_max,
        x_max=field.l_max,
        y_max=field.m_max,
    )

    polygons_sky = [
        sky_from_orthographic(item, field.imaging_centre)
        for item in vertices_lm
    ]

    return Tesselation(
        calibration_directions, polygons_sky, field.imaging_centre
    )
