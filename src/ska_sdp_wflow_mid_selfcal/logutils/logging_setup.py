import logging
import logging.config
from pathlib import Path
from typing import Final

LOGGER_NAME: Final[str] = "mid-selfcal"
""" Name for the top-level pipeline logger """

LOGGER = logging.getLogger(LOGGER_NAME)
""" Top-level pipeline logger """


def make_config(logfile_basepath: Path) -> dict:
    """
    Create a configuration dictionary for the logging module.
    """
    text_format = "[%(levelname)s - %(asctime)s] %(message)s"
    return {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "text": {"format": text_format},
            "json": {
                # Weird syntax to specify a custom formatter class
                # https://docs.python.org/3/library/logging.config.html#logging-config-dict-userdef
                "()": "ska_sdp_wflow_mid_selfcal.logutils.JSONFormatter"
            },
        },
        "handlers": {
            "stdout": {
                "class": "logging.StreamHandler",
                "formatter": "text",
                "stream": "ext://sys.stdout",
            },
            "logfile_text": {
                "class": "logging.FileHandler",
                "formatter": "text",
                "filename": str(
                    logfile_basepath.resolve().with_suffix(".txt")
                ),
            },
            "logfile_jsonl": {
                "class": "logging.FileHandler",
                "formatter": "json",
                "filename": str(
                    logfile_basepath.resolve().with_suffix(".jsonl")
                ),
            },
        },
        "loggers": {
            LOGGER_NAME: {
                "level": "DEBUG",
                "handlers": ["stdout", "logfile_text", "logfile_jsonl"],
            },
        },
    }


def configure_pipeline_logger(logfile_basepath: Path) -> None:
    """
    Configure the top-level pipeline logger to print to stdout and to save all
    logs to the given file path.
    """
    logging.config.dictConfig(make_config(logfile_basepath))
