import json
import logging
from datetime import datetime, timezone


class JSONFormatter(logging.Formatter):
    """
    Logging formatter that emits records as dictionaries in JSON format.
    This is quite useful to procedurally parse logs.
    """

    EXTRA_KEYS: list[str] = [
        "subprocess",
        "worker_name",
        "stage",
        "cycle",
        "task_index",
    ]

    def format(self, record: logging.LogRecord) -> str:
        # Remove the trailing "+HH:MM" for easier parsing later, we already
        # know this is UTC time
        utc_str = (
            datetime.fromtimestamp(record.created, tz=timezone.utc)
            .isoformat()
            .split("+", maxsplit=1)[0]
        )
        items = {
            "utc": utc_str,
            "level": record.levelname,
            "message": record.getMessage(),
        }

        items.update(
            {
                key: val
                for key in self.EXTRA_KEYS
                if (val := getattr(record, key, None)) is not None
            }
        )

        if record.exc_info is not None:
            items["exc_info"] = self.formatException(record.exc_info)
        if record.stack_info is not None:
            items["stack_info"] = self.formatStack(record.stack_info)

        return json.dumps(items, default=str)
