import argparse
from pathlib import Path

from ska_sdp_wflow_mid_selfcal.image_processing import FITSImage


def parse_args() -> argparse.Namespace:
    """
    Parse command-line arguments into a convenient object.
    """
    parser = argparse.ArgumentParser(
        description="Compute basic FITS image statistics",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "files",
        nargs="+",
        type=Path,
        help="WSClean FITS files.",
    )
    return parser.parse_args()


def main():
    """
    Entry point for image_stats app.
    """
    args = parse_args()

    for fname in args.files:
        fits_image = FITSImage(fname)
        peak_flux = fits_image.stokes_i_data().max()
        print(fits_image.path)
        print(f"RMS       : {1.0e6 * fits_image.rms():.2f} uJy/beam")
        print(f"Robust RMS: {1.0e6 * fits_image.robust_rms():.2f} uJy/beam")
        print(f"Peak S/N  : {peak_flux / fits_image.robust_rms():.1f}")
        print()


if __name__ == "__main__":
    main()
