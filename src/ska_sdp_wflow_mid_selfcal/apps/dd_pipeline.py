import argparse
import shutil
import sys
from pathlib import Path

import dask.distributed
import yaml

from ska_sdp_wflow_mid_selfcal import __version__
from ska_sdp_wflow_mid_selfcal.cleanup import run_cleanup
from ska_sdp_wflow_mid_selfcal.config import validate_config
from ska_sdp_wflow_mid_selfcal.graceful_pipeline_shutdown import (
    graceful_pipeline_shutdown,
)
from ska_sdp_wflow_mid_selfcal.logutils import (
    LOGGER,
    configure_pipeline_logger,
)
from ska_sdp_wflow_mid_selfcal.measurement_set import MeasurementSet
from ska_sdp_wflow_mid_selfcal.output_directory_creation import (
    create_output_directory,
)
from ska_sdp_wflow_mid_selfcal.pipeline_dd import selfcal_pipeline_dd
from ska_sdp_wflow_mid_selfcal.skymodel import SkyModel


def get_parser() -> argparse.ArgumentParser:
    """
    Parse command-line arguments into a convenient object.
    """
    parser = argparse.ArgumentParser(
        description=(
            "Launch the SKA Mid direction-dependent self-calibration pipeline"
        ),
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument("--version", action="version", version=__version__)
    parser.add_argument(
        "--singularity-image",
        type=Path,
        default=None,
        help=(
            "Optional path to a singularity image file with both WSClean "
            "and DP3 installed. If specified, run WSClean and DP3 inside "
            "singularity containers; otherwise, run them on bare metal."
        ),
    )
    parser.add_argument(
        "--dask-scheduler",
        type=str,
        default=None,
        help=(
            "Optional dask scheduler address to which to submit jobs. "
            "If specified, any eligible pipeline step will be distributed on "
            "the associated Dask cluster."
        ),
    )
    parser.add_argument(
        "--mpi-hosts",
        type=str,
        nargs="+",
        default=None,
        help=(
            "List of hostnames on which to run MPI-eligible pipeline step. "
            "If the list is of length 1 or less, MPI is not used."
        ),
    )
    parser.add_argument(
        "--outdir",
        type=Path,
        help=(
            "Directory path in which to write data products and "
            "temporary files; it will be created if necessary, with all its "
            "parents. If not specified, create a uniquely named "
            "subdir in the current working directory, named "
            "selfcal_YYYYMMDD_HHMMSS_<microseconds>"
        ),
    )
    parser.add_argument(
        "--config",
        type=Path,
        required=True,
        help=("Path to the pipeline configuration file. "),
    )
    parser.add_argument(
        "--sky-model",
        type=Path,
        help=(
            "Optional path to bootstrap sky model file in sourcedb format. "
            "If provided, use this sky model to bootstrap calibration. "
            "Otherwise, the pipeline will run an initial imaging stage to "
            "make such a bootstrap sky model. "
            "NOTE: the source fluxes must be **apparent** fluxes, that is "
            "with the primary beam attenuation already applied."
        ),
    )
    parser.add_argument(
        "--num-pixels",
        type=int,
        required=True,
        help="Output image size in pixels.",
    )
    parser.add_argument(
        "--pixel-scale",
        type=float,
        required=True,
        help=("Scale of a pixel in arcseconds."),
    )
    parser.add_argument(
        "input_ms",
        type=Path,
        help="Input measurement set.",
    )
    return parser


def run_program(cli_args: list[str]) -> int:
    """
    Runs the DD selfcal pipeline. Returns an exit code.
    """
    args = get_parser().parse_args(cli_args)

    outdir = create_output_directory(args.outdir)
    logfile_path = outdir / "logfile.txt"
    configure_pipeline_logger(logfile_path)

    # Start logging things once the logger is fully configured
    LOGGER.info("Called with arguments:")
    for key, val in vars(args).items():
        LOGGER.info(f"    {key}: {val}")

    LOGGER.info(f"Created output directory: {outdir}")
    shutil.copy(args.config, outdir / "config.yml")

    with graceful_pipeline_shutdown(lambda: run_cleanup(outdir)):
        input_ms = MeasurementSet(args.input_ms)
        sky_model = (
            SkyModel.load_sourcedb(args.sky_model) if args.sky_model else None
        )

        with open(args.config, "r") as fobj:
            config_dict = yaml.safe_load(fobj)
            validate_config(config_dict)

        dask_client = (
            dask.distributed.Client(args.dask_scheduler)
            if args.dask_scheduler
            else None
        )

        selfcal_pipeline_dd(
            input_ms,
            sky_model=sky_model,
            num_pixels=args.num_pixels,
            pixel_scale_asec=args.pixel_scale,
            config_dict=config_dict,
            outdir=outdir,
            singularity_image=args.singularity_image,
            dask_client=dask_client,
            mpi_hosts=args.mpi_hosts,
        )

    return 0


def main() -> int:
    """
    Entry point for the DD selfcal pipeline app. Returns an exit code.
    """
    return run_program(sys.argv[1:])


if __name__ == "__main__":
    sys.exit(main())
