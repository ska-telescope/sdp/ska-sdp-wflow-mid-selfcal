import argparse
import sys
from pathlib import Path

from ska_sdp_wflow_mid_selfcal.config import validate_config_file


def make_parser() -> argparse.ArgumentParser:
    """
    Parse command-line arguments into a convenient object.
    """
    parser = argparse.ArgumentParser(
        description=(
            "Validate a pipeline YAML configuration file. If the "
            "configuration is valid, the app returns quietly. Otherwise an "
            "exception message will be printed with an explanation."
        ),
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "config",
        type=Path,
        default=1.0,
        help="YAML Configuration file to validate",
    )
    return parser


def run_program(cli_args: list[str]) -> int:
    """
    Run the program, return an exit code.
    """
    args = make_parser().parse_args(cli_args)
    try:
        validate_config_file(args.config)
        return 0
    # pylint: disable=broad-exception-caught
    except Exception as err:
        print(err)
        return 1


def main() -> int:
    """
    Entry point for app.
    """
    run_program(sys.argv[1:])


if __name__ == "__main__":
    sys.exit(main())
