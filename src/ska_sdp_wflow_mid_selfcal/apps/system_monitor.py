import argparse
import json
import signal
import sys
from datetime import datetime, timezone

import psutil


def parse_args() -> argparse.Namespace:
    """
    Parse command-line arguments into a convenient object.
    """
    parser = argparse.ArgumentParser(
        description=(
            "Get system usage metrics at regular intervals, and print them "
            "to standard output as a JSON dictionary. "
            "NOTE: it only makes sense to run this inside a SLURM allocation "
            "if the entire node is allocated to the job -- we don't want the "
            "metrics to be affected by others."
        ),
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--interval",
        type=float,
        default=1.0,
        help="Interval in seconds between each checkpoint.",
    )
    return parser.parse_args()


def main():
    """
    Entry point for the mid-selfcal-system-monitor app.
    """

    def signal_handler(signum, __):
        now_str = utc_now_isoformat_notz()
        signame = signal.Signals(signum).name
        # NOTE: print to stderr, the only thing we print to stdout are metrics
        print(
            f"{now_str} System monitor interrupted by signal: {signame}",
            file=sys.stderr,
        )
        raise KeyboardInterrupt

    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    args = parse_args()
    try:
        main_loop(args.interval)
    except KeyboardInterrupt:
        pass


def utc_now_isoformat_notz() -> str:
    """
    Present UTC time as an isoformat string, without the +HH:MM time zone
    specifier at the end.
    """
    now = datetime.now(tz=timezone.utc)
    # Remove the trailing "+HH:MM" for easier parsing later, we already
    # know this is UTC time
    return now.isoformat().split("+", maxsplit=1)[0]


def main_loop(interval: float):
    """
    Self-explanatory.
    """
    disk_io_prev = psutil.disk_io_counters(perdisk=False)
    netw_io_prev = psutil.net_io_counters(pernic=True)

    while True:
        # Use UTC time for consistency with pipeline logger
        now_str = utc_now_isoformat_notz()
        disk_io_now = psutil.disk_io_counters(perdisk=False)
        netw_io_now = psutil.net_io_counters(pernic=True)
        cpu_percent_list = psutil.cpu_percent(interval=interval, percpu=True)
        mem_gb = psutil.virtual_memory().used / 1024**3

        result = {
            "utc": now_str,
            "cpu_average": sum(cpu_percent_list) / len(cpu_percent_list),
            "memory_used_gb": mem_gb,
        }

        # Individual CPU load
        result.update(
            {
                f"cpu_{index:03d}": value
                for index, value in enumerate(cpu_percent_list)
            }
        )

        # Network bytes sent/received since last checkpoint
        netw_io_diff = _subtract_namedtuple_dicts(
            netw_io_now, netw_io_prev, ["bytes_sent", "bytes_recv"]
        )
        result.update(netw_io_diff)

        # Disk bytes read/written since last checkpoint
        disk_io_diff = _subtract_namedtuples(
            disk_io_now, disk_io_prev, ["read_bytes", "write_bytes"]
        )
        result.update(disk_io_diff)

        # NOTE: flush=True is important when redirecting the output to file
        # on some machines. Otherwise the output gets buffered and may not
        # be flushed to file when the program is terminated (e.g. by SLURM).
        print(json.dumps(result), flush=True)
        disk_io_prev = disk_io_now
        netw_io_prev = netw_io_now


def _subtract_namedtuples(left, right, attr_list: list[str]) -> dict:
    return {
        attr: getattr(left, attr) - getattr(right, attr) for attr in attr_list
    }


def _subtract_namedtuple_dicts(left, right, attr_list: list[str]) -> dict:
    keys = set(left.keys()).intersection(right.keys())
    return {
        f"{attr}_{key}": getattr(left[key], attr) - getattr(right[key], attr)
        for key in keys
        for attr in attr_list
    }


if __name__ == "__main__":
    main()
