import os
from pathlib import Path
from typing import Union

import astropy.units as u
import numpy as np
from astropy.coordinates import SkyCoord
from casacore.tables import table
from numpy.typing import NDArray


class MeasurementSet:
    """
    Stores basic parameters of a MeasurementSet.
    """

    def __init__(self, path: Union[str, os.PathLike]):
        """
        Create a new MeasurementSet instance, reading useful metadata from an
        MSv2 on disk. Only measurement sets with a single spectral window and
        evenly spaced channels are supported.
        """
        if not isinstance(path, os.PathLike):
            path = Path(path)

        if not path.is_dir():
            raise FileNotFoundError(f"{path!s} is not a directory")

        self._path = path.resolve()
        self._phase_centre = read_phase_centre(self.path)
        (
            self._bottom_freq_hz,
            self._channel_width_hz,
            self._num_channels,
        ) = read_band_information(self.path)

    @property
    def path(self) -> Path:
        """
        Absolute path on disk
        """
        return self._path

    @property
    def phase_centre(self) -> SkyCoord:
        """
        Phase centre as an astropy SkyCoord object.
        """
        return self._phase_centre

    @property
    def num_channels(self) -> int:
        """
        Number of frequency channels.
        """
        return self._num_channels

    @property
    def bottom_freq_hz(self) -> float:
        """
        Bottom frequency of the band in Hz. Note that this is smaller than the
        centre frequency of the lowest channel.
        """
        return self._bottom_freq_hz

    @property
    def top_freq_hz(self) -> float:
        """
        Top frequency of the band in Hz. Note that this is greater than the
        centre frequency of the highest channel.
        """
        return self.bottom_freq_hz + self.total_bandwidth_hz

    @property
    def channel_width_hz(self) -> float:
        """
        Width of every channel in Hz.
        """
        return self._channel_width_hz

    @property
    def total_bandwidth_hz(self) -> float:
        """
        Total bandwidth in Hz.
        """
        return self.num_channels * self.channel_width_hz

    def __repr__(self) -> str:
        clsname = type(self).__name__
        fbot_mhz = self.bottom_freq_hz / 1e6
        ftop_mhz = fbot_mhz + self.total_bandwidth_hz / 1e6
        phase_centre_str = self.phase_centre.to_string("hmsdms", precision=2)
        return (
            f"{clsname}(path={self.path!s}, "
            f"fbot={fbot_mhz:.2f} MHz, "
            f"ftop={ftop_mhz:.2f} MHz, "
            f"channels={self.num_channels}, "
            f"phase_centre={phase_centre_str})"
        )


def read_phase_centre(mset: Path) -> SkyCoord:
    """
    Returns the phase centre of the given MeasurementSet. This function expects
    a single PHASE_DIR that is constant in time, and will raise ValueError
    otherwise.

    NOTE: Assumes that the coordinate frame is ICRS, but we should almost
    certainly use FK5 (a.k.a. J2000).
    """
    table_name = str(mset) + "::FIELD"
    phase_dir: NDArray = table(table_name, ack=False).getcol("PHASE_DIR")
    if not phase_dir.shape == (1, 1, 2):
        raise ValueError(
            "Error reading PHASE_DIR: multiple rows "
            "or polynomial in time not supported"
        )
    ra_rad, dec_rad = phase_dir.ravel()
    return SkyCoord(ra_rad, dec_rad, unit=(u.rad, u.rad), frame="icrs")


def read_band_information(mset: Path) -> tuple[float, float, int]:
    """
    Reads band information from the SPECTRAL_WINDOW table. Returns a 3-tuple
    (bottom_freq, chan_width, num_channels). Note that bottom_freq is the
    frequency of the bottom edge of the band, which is less than the centre
    frequency of the lowest channel.
    """
    tab = table(str(mset) + "::SPECTRAL_WINDOW", ack=False)

    # NOTE: most of these arrays have a shape of
    # (num_spectral_windows, num_channels)
    spw_num_channels: NDArray[np.int32] = tab.getcol("NUM_CHAN")
    if not len(spw_num_channels) == 1:
        raise ValueError(
            "MeasurementSets with multiple spectral windows are not supported"
        )
    num_channels = int(spw_num_channels.ravel()[0])

    spw_chan_width: NDArray[np.float64] = tab.getcol("CHAN_WIDTH")
    spw_chan_width = spw_chan_width.ravel()
    if not len(set(spw_chan_width)) == 1:
        raise ValueError(
            "MeasurementSets with non-uniform frequency resolution are not "
            "supported"
        )
    chan_width = float(spw_chan_width.ravel()[0])

    # NOTE: MSv2 docs specify that the channel frequencies may be in ascending
    # or descending frequency order
    spw_chan_freq: NDArray[np.float64] = tab.getcol("CHAN_FREQ")
    bottom_freq = float(spw_chan_freq.min() - 0.5 * chan_width)
    return bottom_freq, chan_width, num_channels
