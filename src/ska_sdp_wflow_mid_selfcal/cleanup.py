import re
import shutil
from pathlib import Path

from ska_sdp_wflow_mid_selfcal.logutils import LOGGER


def run_cleanup(directory: Path) -> None:
    """
    Remove all unnecessary files and directories created by running the
    pipeline with the given output directory.
    """

    if not directory.is_dir():
        return

    directory = directory.resolve()
    LOGGER.info(f"Running cleanup in directory: {directory!r}")

    subdirs_iter = filter(Path.is_dir, directory.iterdir())
    for subdir in subdirs_iter:
        remove_files_with_suffix(subdir, ".tmp")
        remove_calibration_solution_chunks(subdir)
        remove_unnecessary_fits_files(subdir)


def remove_calibration_solution_chunks(directory: Path) -> None:
    """
    Remove the calibration solution chunks produced by distributed DDECal runs.
    """
    directory = directory.resolve()
    pattern = r"(.*?)sol(.*?)\.part(\d+)"
    for path in directory.iterdir():
        if re.match(pattern, path.name):
            path.unlink()
            LOGGER.debug(f"Deleted file: {path}")


def remove_unnecessary_fits_files(directory: Path) -> None:
    """
    Remove any temporary or intermediate .fits files that wsclean-mp creates
    in multi-node runs. In particular, wsclean-mp writes a set of FITS files
    for every frequency band before doing multi-frequency synthesis (MFS).
    This is to avoid using massive amounts of disk space.
    """
    directory = directory.resolve()

    patterns = [
        r"(.*?)-(\d{4})-(dirty|image|model|psf|residual)\.fits",
        r"(.*?)-(\d{4})-(image-pb|model-pb|residual-pb)\.fits",
        r"(.*?)-(\d{4})-beam-\d+\.fits",
        r"(.*?)-(dirty|image|model|psf|residual)-(\d{4})-tmp\.fits",
    ]

    for path in directory.iterdir():
        if any(re.match(pat, path.name) for pat in patterns):
            path.unlink()
            LOGGER.debug(f"Deleted file: {path}")


def remove_files_with_suffix(parent_dir: Path, suffix: str):
    """
    Remove files in `parent_dir` with the given suffix, emit a logging message
    for each deleted file. Suffix must include the leading dot.
    """
    for path in parent_dir.iterdir():
        if path.suffix == suffix and path.is_file():
            path.unlink()
            LOGGER.debug(f"Deleted file: {path}")


def remove_directory(directory: Path):
    """
    Remove given path if it is a directory, and emit a log message if deleting
    it.
    """
    if directory.is_dir():
        shutil.rmtree(str(directory))
        LOGGER.debug(f"Deleted directory: {directory}")
