import numpy
from numpy.typing import NDArray
from scipy.stats import norm as normal_distribution


# pylint:disable=too-many-locals
def make_noise_map(
    image: NDArray, num_cells: tuple[int, int] = (10, 10)
) -> NDArray:
    """
    Estimate the noise level across an input image.

    Args:
        image: A 2D NumPy array representing the input image.
        num_cells: A tuple of integers specifying the number of cells along
            the x and y directions into which the image will be divided.

    Returns:
        An array of the same shape as the input image, where each
        pixel represents the estimated noise level in the corresponding
        location of the original image.

    Notes:
        This function divides the input image into a grid of cells, calculates
        a robust estimate of the root mean square (RMS) noise within each cell,
        and returns a noise map indicating the noise level across the image.
    """
    # Crop image to dimensions that are multiples of num_cells in both x and y
    # directions
    total_pixels_x, total_pixels_y = image.shape
    num_cells_x, num_cells_y = num_cells

    # Dimensions of a rectangular cell in pixels
    cell_pixels_x = total_pixels_x // num_cells_x
    cell_pixels_y = total_pixels_y // num_cells_y

    # Dimensions of the cropped image
    cropped_pixels_x = cell_pixels_x * num_cells_x
    cropped_pixels_y = cell_pixels_y * num_cells_y

    # Coordinates of upper left corner of cropped image within total image
    pixel_offset_x = (total_pixels_x - cropped_pixels_x) // 2
    pixel_offset_y = (total_pixels_y - cropped_pixels_y) // 2

    cropped_image = image[
        pixel_offset_x : (pixel_offset_x + cropped_pixels_x),
        pixel_offset_y : (pixel_offset_y + cropped_pixels_y),
    ]

    # Calculate two data percentiles per cell, from which we can estimate
    # robust noise RMS, assuming the underlying pixel value distribution
    # is Gaussian
    tensor = cropped_image.reshape(
        num_cells_x, num_cells_y, cell_pixels_x, cell_pixels_y
    )
    cell_low_percentiles, cell_high_percentiles = numpy.percentile(
        tensor, (10, 50), axis=(2, 3)
    )
    cell_robust_rms = (cell_high_percentiles - cell_low_percentiles) / abs(
        normal_distribution.isf(0.5) - normal_distribution.isf(0.1)
    )

    # Nearest interpolation back to original image before cropping
    result = numpy.empty_like(image)

    def _get_slice(index, stop_index, pixel_offset, cell_pixels):
        start_pixel = index * cell_pixels + pixel_offset
        stop_pixel = start_pixel + cell_pixels
        if index == 0:
            start_pixel = 0
        if index == stop_index - 1:
            stop_pixel = None
        return slice(start_pixel, stop_pixel)

    for i in range(num_cells_x):
        slice_x = _get_slice(i, num_cells_x, pixel_offset_x, cell_pixels_x)
        for j in range(num_cells_y):
            slice_y = _get_slice(j, num_cells_y, pixel_offset_y, cell_pixels_y)
            result[slice_x, slice_y] = cell_robust_rms[i, j]

    return result
