import functools
import math
import os
import shutil
from pathlib import Path
from typing import Union

import numpy as np
from astropy.coordinates import SkyCoord
from astropy.io import fits
from numpy.typing import NDArray

from .functions import make_noise_map


class FITSImage:
    """
    A class representing FITS images saved by WSClean.
    """

    def __init__(self, path: Union[str, os.PathLike]) -> None:
        """
        Initializes WSCleanFITS from the given path on disk.
        """
        self._path = Path(path).resolve()
        with fits.open(path) as hdu_list:
            # pylint: disable=no-member
            self._header: fits.Header = hdu_list[0].header

    @property
    def path(self) -> Path:
        """
        The absolute path to the FITS file.
        """
        return self._path

    @property
    def shape(self) -> tuple[int, ...]:
        """
        Shape of the main data numpy array.
        """
        return (
            self._header["NAXIS4"],  # pol
            self._header["NAXIS3"],  # freq
            self._header["NAXIS2"],  # dec
            self._header["NAXIS1"],  # ra
        )

    @property
    def centre_x(self) -> int:
        """
        X pixel coordinate of the image centre / reference pixel.
        The X axis is aligned with -l (i.e. -RA), because l and RA increase
        towards the East, which is to the left in images.
        """
        # NOTE: CRPIXn is 1-indexed (first pixel has index 1), but we work with
        # 0-indexed data in Python/numpy
        # https://fits.gsfc.nasa.gov/users_guide/users_guide/node22.html
        return int(self._header["CRPIX1"] - 1)

    @property
    def centre_y(self) -> int:
        """
        Y pixel coordinate of the image centre / reference pixel.
        The Y axis is aligned with +m (i.e. +Dec).
        """
        return int(self._header["CRPIX2"] - 1)

    @property
    def pixel_scale_lm(self) -> float:
        """
        Size of a pixel in (l, m) space.
        """
        pixel_scale_deg = abs(self._header["CDELT1"])
        return math.sin(math.radians(pixel_scale_deg))

    @property
    def centre_sky(self) -> SkyCoord:
        """
        Sky coordinates of the image centre / reference pixel.
        """
        return SkyCoord(
            self._header["CRVAL1"], self._header["CRVAL2"], unit=("deg", "deg")
        )

    @functools.lru_cache(maxsize=1)
    def stokes_i_data(self) -> NDArray:
        """
        Stokes I data as a two-dimensional array. The first axis is aligned
        with +Dec / +m, the second axis is aligned with -RA / -l.
        """
        # PrimaryHDU is the first in the list
        # `data` has shape (npol, nchan, npix_dec, npix_ra)
        # pylint: disable=no-member
        with fits.open(self.path) as hdu_list:
            return hdu_list[0].data[0, 0]

    @functools.lru_cache(maxsize=1)
    def rms(self) -> float:
        """
        RMS in Jy/beam.
        """
        return self.stokes_i_data().std()

    def robust_rms(self) -> float:
        """
        Returns the robust standard deviation of the given data, assuming the
        underlying distribution is approximately Gaussian.

        See: https://en.wikipedia.org/wiki/Interquartile_range#Distributions
        """
        data = self.stokes_i_data().ravel()
        pct_25, pct_75 = np.percentile(data, (25, 75))
        return (pct_75 - pct_25) / 1.349

    @functools.lru_cache(maxsize=1)
    def noise_map(self, num_cells: tuple[int, int] = (10, 10)) -> NDArray:
        """
        Estimate the background noise level across the stokes I image.

        Args:
            num_cells: A tuple of integers specifying the number of cells along
                the x and y directions into which the image will be divided.

        Returns:
            An array where each pixel represents the estimated noise level in
            the corresponding location of the original image.
        """
        return make_noise_map(self.stokes_i_data(), num_cells)

    @functools.lru_cache(maxsize=1)
    def snr_map(self, num_cells: tuple[int, int] = (10, 10)) -> NDArray:
        """
        Estimate S/N across across the stokes I image.

        Args:
            num_cells: A tuple of integers specifying the number of cells along
                the x and y directions into which the image will be divided
                for the purpose of background noise calculation.

        Returns:
            An array where each pixel represents the estimated S/N level in
            the corresponding location of the original image.
        """
        data = self.stokes_i_data()
        noise = self.noise_map(num_cells)
        return data / noise

    def save_with_replaced_data(
        self, new_data: NDArray, outfile: Union[str, os.PathLike]
    ) -> None:
        """
        Save a new FITS file with identical header but with data replaced.

        Args:
            new_data: Replacement data array. Dimensions must be compatible.
            outfile: Path to the output FITS file.

        Raises:
            ValueError: If the shape of the new data array does not match the
                expected shape specified in the header of this FITS file.
        """
        # We allow adding dummy dimensions of length 1 to the new data.
        # Most of the files we deal with have data shape (1, 1, npix, npix)
        try:
            new_data = new_data.reshape(self.shape)
        except ValueError as err:
            raise ValueError(
                f"New data array has shape {new_data.shape} which is "
                f"incompatible with expected shape {self.shape}"
            ) from err

        shutil.copy(self.path, outfile)

        with fits.open(outfile, mode="update") as hdu_list:
            # pylint: disable=no-member
            hdu_list[0].data[:] = new_data.astype("float32")
