{
    "title": "wsclean",
    "description": "WSClean parameters",
    "type": "object",
    "additionalProperties": false,
    "properties": {
        "apply-facet-beam": {
            "type": "boolean"
        },
        "apply-primary-beam": {
            "type": "boolean"
        },
        "compound-tasks": {
            "type": "boolean"
        },
        "diagonal-solutions": {
            "type": "boolean"
        },
        "even-timesteps": {
            "type": "boolean"
        },
        "gap-channel-division": {
            "type": "boolean"
        },
        "grid-with-beam": {
            "type": "boolean"
        },
        "iuwt": {
            "type": "boolean"
        },
        "iuwt-snr-test": {
            "type": "boolean"
        },
        "join-polarizations": {
            "type": "boolean"
        },
        "local-rms": {
            "type": "boolean"
        },
        "log-time": {
            "type": "boolean"
        },
        "mf-weighting": {
            "type": "boolean"
        },
        "multiscale": {
            "type": "boolean"
        },
        "negative": {
            "type": "boolean"
        },
        "no-dirty": {
            "type": "boolean"
        },
        "no-fast-subminor": {
            "type": "boolean"
        },
        "no-mf-weighting": {
            "type": "boolean"
        },
        "no-min-grid-resolution": {
            "type": "boolean"
        },
        "no-multiscale-fast-subminor": {
            "type": "boolean"
        },
        "no-negative": {
            "type": "boolean"
        },
        "no-reorder": {
            "type": "boolean"
        },
        "no-update-model-required": {
            "type": "boolean"
        },
        "no-work-on-master": {
            "type": "boolean"
        },
        "odd-timesteps": {
            "type": "boolean"
        },
        "predict": {
            "type": "boolean"
        },
        "reorder": {
            "type": "boolean"
        },
        "save-aterms": {
            "type": "boolean"
        },
        "save-first-residual": {
            "type": "boolean"
        },
        "save-psf-pb": {
            "type": "boolean"
        },
        "save-source-list": {
            "type": "boolean"
        },
        "save-uv": {
            "type": "boolean"
        },
        "save-weights": {
            "type": "boolean"
        },
        "scalar-beam": {
            "type": "boolean"
        },
        "squared-channel-joining": {
            "type": "boolean"
        },
        "stop-negative": {
            "type": "boolean"
        },
        "store-imaging-weights": {
            "type": "boolean"
        },
        "update-model-required": {
            "type": "boolean"
        },
        "use-differential-lofar-beam": {
            "type": "boolean"
        },
        "use-weights-as-taper": {
            "type": "boolean"
        },
        "verbose": {
            "type": "boolean"
        },
        "abs-auto-mask": {
            "type": "number",
            "exclusiveMinimum": 0
        },
        "abs-mem": {
            "type": "number",
            "exclusiveMinimum": 0
        },
        "abs-threshold": {
            "type": "number",
            "exclusiveMinimum": 0
        },
        "aterm-config": {
            "type": "string"
        },
        "aterm-kernel-size": {
            "type": "number"
        },
        "auto-mask": {
            "type": "number",
            "exclusiveMinimum": 0
        },
        "auto-threshold": {
            "type": "number",
            "exclusiveMinimum": 0
        },
        "baseline-averaging": {
            "type": "number"
        },
        "beam-aterm-update": {
            "type": "integer"
        },
        "beam-mode": {
            "enum": [
                "array_factor",
                "element",
                "full"
            ]
        },
        "beam-model": {
            "type": "string"
        },
        "beam-normalisation-mode": {
            "type": "string"
        },
        "casa-mask": {
            "type": "string"
        },
        "channel-division-frequencies": {
            "type": "string"
        },
        "channel-range": {
            "items": {
                "type": "integer"
            },
            "maxItems": 2,
            "minItems": 2,
            "type": "array"
        },
        "channel-to-node": {
            "type": "string"
        },
        "channels-out": {
            "type": "integer",
            "minimum": 1
        },
        "clean-border": {
            "type": "number"
        },
        "data-column": {
            "type": "string"
        },
        "dd-psf-grid": {
            "type": "string"
        },
        "deconvolution-channels": {
            "type": "integer",
            "minimum": 1
        },
        "deconvolution-threads": {
            "type": "integer",
            "minimum": 1
        },
        "facet-beam-update": {
            "type": "number",
            "exclusiveMinimum": 0
        },
        "feather-size": {
            "type": "integer"
        },
        "field": {
            "type": "string"
        },
        "fit-spectral-log-pol": {
            "type": "integer",
            "minimum": 1
        },
        "fit-spectral-pol": {
            "type": "integer",
            "minimum": 1
        },
        "fits-mask": {
            "type": "string"
        },
        "force-spectrum": {
            "type": "string"
        },
        "gain": {
            "type": "number",
            "exclusiveMinimum": 0,
            "maximum": 1
        },
        "gridder": {
            "enum": [
                "direct-ft",
                "idg",
                "wgridder",
                "tuned-wgridder",
                "wstacking"
            ]
        },
        "horizon-mask": {
            "type": "string"
        },
        "idg-mode": {
            "type": "string"
        },
        "interval": {
            "items": {
                "type": "integer"
            },
            "maxItems": 2,
            "minItems": 2,
            "type": "array"
        },
        "j": {
            "type": "integer",
            "minimum": 1
        },
        "link-polarizations": {
            "type": "string"
        },
        "local-rms-method": {
            "enum": [
                "rms",
                "rms-with-min"
            ]
        },
        "local-rms-window": {
            "type": "number",
            "exclusiveMinimum": 0
        },
        "max-mpi-message-size": {
            "type": "string"
        },
        "maxuv-l": {
            "type": "number",
            "exclusiveMinimum": 0
        },
        "maxuvw-m": {
            "type": "number",
            "exclusiveMinimum": 0
        },
        "maxw": {
            "type": "number",
            "exclusiveMinimum": 0
        },
        "mem": {
            "type": "number",
            "exclusiveMinimum": 0
        },
        "mgain": {
            "type": "number",
            "exclusiveMinimum": 0,
            "maximum": 1
        },
        "minuv-l": {
            "type": "number",
            "exclusiveMinimum": 0
        },
        "minuvw-m": {
            "type": "number",
            "exclusiveMinimum": 0
        },
        "moresane-arg": {
            "type": "string"
        },
        "moresane-ext": {
            "type": "string"
        },
        "moresane-sl": {
            "type": "string"
        },
        "multiscale-convolution-padding": {
            "type": "number"
        },
        "multiscale-gain": {
            "type": "number",
            "exclusiveMinimum": 0,
            "maximum": 1
        },
        "multiscale-max-scales": {
            "type": "integer",
            "minimum": 1
        },
        "multiscale-scale-bias": {
            "type": "number"
        },
        "multiscale-scales": {
            "type": "string"
        },
        "multiscale-shape": {
            "type": "string"
        },
        "mwa-path": {
            "type": "string"
        },
        "niter": {
            "type": "integer",
            "minimum": 1
        },
        "nmiter": {
            "type": "integer",
            "minimum": 1
        },
        "padding": {
            "type": "number"
        },
        "parallel-deconvolution": {
            "type": "number",
            "minimum": 1
        },
        "parallel-gridding": {
            "type": "integer",
            "minimum": 1
        },
        "parallel-reordering": {
            "type": "integer",
            "minimum": 1
        },
        "pb-grid-size": {
            "type": "integer"
        },
        "pol": {
            "type": "string"
        },
        "primary-beam-limit": {
            "type": "number"
        },
        "python-deconvolution": {
            "type": "string"
        },
        "reuse-dirty": {
            "type": "string"
        },
        "reuse-psf": {
            "type": "string"
        },
        "shift": {
            "items": {
                "type": "number"
            },
            "maxItems": 2,
            "minItems": 2,
            "type": "array"
        },
        "simulate-baseline-noise": {
            "type": "string"
        },
        "simulate-noise": {
            "type": "number"
        },
        "spectral-correction": {
            "items": {
                "type": "number"
            },
            "minItems": 2,
            "type": "array"
        },
        "spws": {
            "type": "string"
        },
        "super-weight": {
            "minimum": 0,
            "type": "number"
        },
        "taper-edge": {
            "type": "number"
        },
        "taper-edge-tukey": {
            "type": "number"
        },
        "taper-gaussian": {
            "minimum": 0,
            "type": "number"
        },
        "taper-inner-tukey": {
            "type": "number"
        },
        "taper-tukey": {
            "type": "number"
        },
        "visibility-weighting-mode": {
            "type": "string"
        },
        "weight": {
            "anyOf": [
                {
                    "enum": [
                        "natural",
                        "uniform"
                    ]
                },
                {
                    "prefixItems": [
                        {
                            "enum": [
                                "briggs"
                            ]
                        },
                        {
                            "maximum": 2.0,
                            "minimum": -2.0,
                            "type": "number"
                        }
                    ],
                    "type": "array"
                }
            ]
        },
        "weighting-rank-filter": {
            "type": "number"
        },
        "weighting-rank-filter-size": {
            "minimum": 0,
            "type": "integer"
        },
        "wgridder-accuracy": {
            "type": "number",
            "exclusiveMinimum": 0,
            "exclusiveMaximum": 1
        },
        "wstack-grid-mode": {
            "enum": [
                "kb",
                "nn",
                "rect",
                "kb-no-sinc",
                "gaus",
                "bn"
            ]
        },
        "wstack-kernel-size": {
            "type": "integer"
        },
        "wstack-nwlayers": {
            "type": "integer"
        },
        "wstack-nwlayers-factor": {
            "type": "number"
        },
        "wstack-nwlayers-for-size": {
            "items": {
                "type": "integer"
            },
            "maxItems": 2,
            "minItems": 2,
            "type": "array"
        },
        "wstack-oversampling": {
            "type": "integer"
        }
    }
}