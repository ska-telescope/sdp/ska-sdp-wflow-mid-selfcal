import json
from pathlib import Path

import yaml
from jsonschema import Draft202012Validator
from referencing import Registry, Resource
from referencing.jsonschema import DRAFT202012

SCHEMAS_DIR = Path(__file__).parent / "schemas"


def retrieve_from_filesystem(uri: str) -> Resource:
    """
    Helper function to retrieve schemas referenced in others.

    See:
    https://python-jsonschema.readthedocs.io/en/latest/referencing/#resolving-references-from-the-file-system
    """
    path = SCHEMAS_DIR / uri.removeprefix("http://localhost/")
    contents = json.loads(path.read_text())
    return DRAFT202012.create_resource(contents)


def load_config_schema_dict() -> dict:
    """
    Load the schema dictionary for a pipeline config file.
    """
    path = SCHEMAS_DIR / "config.json"
    return json.loads(path.read_text())


REGISTRY = Registry(retrieve=retrieve_from_filesystem)
VALIDATOR = Draft202012Validator(load_config_schema_dict(), registry=REGISTRY)


def validate_config(config: dict) -> None:
    """
    Validate a pipeline configuration dictionary.

    Args:
        config: The pipeline configuration dictionary to validate.

    Raises:
        jsonschema.exceptions.ValidationError: If the configuration
            does not conform to the schema.

    Notes:
        Setting some DP3 and WSClean options may be refused; for example, if
        they are strictly under the control of the pipeline.
    """
    VALIDATOR.validate(instance=config)


def validate_config_file(config_file: Path) -> None:
    """
    Validate a pipeline configuration file in YAML format.
    Same as `validate_config()` otherwise.
    """
    text = config_file.read_text()
    config = yaml.safe_load(text)
    validate_config(config)
