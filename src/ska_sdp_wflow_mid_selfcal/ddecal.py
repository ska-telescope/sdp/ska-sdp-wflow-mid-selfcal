import atexit
import itertools
from pathlib import Path
from typing import Any, Final, Iterator, Optional

import casacore.tables
import dask.distributed
import numpy as np
from astropy.time import Time
from numpy.typing import NDArray

from ska_sdp_wflow_mid_selfcal.logutils import LOGGER, LOGGER_NAME
from ska_sdp_wflow_mid_selfcal.shellcmd import (
    DP3Command,
    PrefixModifier,
    SingularityExec,
    execute_with_logger_redirection,
)
from ska_sdp_wflow_mid_selfcal.solution_tables import (
    SolutionFile,
    concatenate_solution_files,
)

MAX_DDECAL_TASKS: Final[int] = 512
"""
When running DDECal in distributed mode, split the input data into at most
this many time intervals. Ultimately, the solution interval determines the
minimum time interval size.
"""


def run_ddecal(
    input_ms: Path,
    sourcedb: Path,
    output_solution_path: Path,
    *,
    ddecal_options: dict[str, Any],
    singularity_image: Optional[Path] = None,
    log_extra: Optional[dict[str, Any]] = None,
    dask_client: Optional[dask.distributed.Client] = None,
) -> SolutionFile:
    """
    Run DDECal either locally or on a Dask cluster.

    Args:
        input_ms: Path to the input measurement set.
        sourcedb: Path to the source list to calibrate against.
        output_solution_path: Path to save the output solution table.
        ddecal_options: Dictionary of user options for DDECal.
        singularity_image: Optional Path to the Singularity image inside which
            to run DP3; if None, run on the host directly.
        log_extra: Optional dictionary of logging record attributes to add onto
            any logging record generated within this function, including those
            captured from DP3.
        dask_client: Optional dask client for distributed computation. If
            provided, split the calibration problem along the time axis, and
            run the resulting calibration tasks concurrently on dask workers.

    Returns:
        SolutionFile: Object representing the solution file.
    """
    LOGGER.info("Running DDECal stage", extra=log_extra)
    modifiers = (
        [SingularityExec(singularity_image)] if singularity_image else []
    )
    if log_extra is None:
        log_extra = {}

    args = (input_ms, sourcedb, output_solution_path)
    kwargs = {
        "ddecal_options": ddecal_options,
        "modifiers": modifiers,
        "log_extra": log_extra,
    }

    if dask_client is None:
        result = run_ddecal_locally(*args, **kwargs)
    else:
        result = run_ddecal_distributed(
            *args, **kwargs, dask_client=dask_client
        )

    LOGGER.info("DDECal stage complete", extra=log_extra)
    return result


def run_ddecal_locally(
    input_ms: Path,
    sourcedb: Path,
    output_solution_path: Path,
    *,
    ddecal_options: dict[str, Any],
    modifiers: list[PrefixModifier],
    log_extra: dict[str, Any],
) -> SolutionFile:
    """
    Run DDECal on the host.
    """
    command = make_ddecal_command(
        input_ms, sourcedb, output_solution_path, ddecal_options
    )
    execute_with_logger_redirection(command, modifiers, extra=log_extra)
    return SolutionFile(output_solution_path)


def worker_run_ddecal_locally(
    input_ms: Path,
    sourcedb: Path,
    output_solution_path: Path,
    *,
    ddecal_options: dict[str, Any],
    modifiers: list[PrefixModifier],
    keep_running: dask.distributed.Variable,
    log_extra: dict[str, Any],
) -> SolutionFile:
    """
    Same as run_ddecal_locally, but to be executed on a dask worker.
    """
    worker = dask.distributed.get_worker()
    ddecal_options.update({"numthreads": worker.state.nthreads})
    command = make_ddecal_command(
        input_ms, sourcedb, output_solution_path, ddecal_options
    )

    # NOTE: setting level to DEBUG to makes sure that all the logs will be
    # forwarded to the dask client
    # (see docs of dask client .forward_logging() method)
    LOGGER.setLevel("DEBUG")

    # NOTE: "worker" is a log record attribute reserved by dask, that contains
    # the tcp address of the worker, so we use "worker_name"
    extra = log_extra | {"worker_name": worker.name}
    execute_with_logger_redirection(
        command, modifiers, keep_running_fn=keep_running.get, extra=extra
    )
    return SolutionFile(output_solution_path)


def cancel_all_ddecal_tasks(
    keep_running: dask.distributed.Variable,
    futures: list[dask.distributed.Future],
) -> None:
    """
    Self-explanatory.
    """
    # First, cancel futures that have not started running
    for fut in futures:
        fut.cancel()

    # Set condition variable to signal to end running tasks immediately
    keep_running.set(False)

    # Block until all tasks have exited
    for fut in futures:
        # NOTE: future.result() re-raises any exception produced
        # during its execution
        try:
            fut.result()
        except dask.distributed.CancelledError:
            LOGGER.info(f"Task cancelled, key = {fut.key}")
        except Exception:  # pylint: disable=broad-exception-caught
            LOGGER.exception("Task raised exception, details below")


# pylint:disable=too-many-locals
def run_ddecal_distributed(
    input_ms: Path,
    sourcedb: Path,
    output_solution_path: Path,
    *,
    ddecal_options: dict[str, Any],
    modifiers: list[PrefixModifier],
    dask_client: dask.distributed.Client,
    log_extra: dict[str, Any],
) -> SolutionFile:
    """
    Run DDECal distributed along the time axis of the data.
    """
    dask_client.forward_logging(logger_name=LOGGER_NAME)
    keep_running = dask.distributed.Variable(name="keep_running_ddecal")
    keep_running.set(True)

    solint = ddecal_options.get("solve.solint", 1)

    # Split the work, make significantly more tasks than workers to achieve
    # better load balancing
    partition_it = partition_measurement_set_in_time(
        input_ms, num_parts=MAX_DDECAL_TASKS, solint=solint
    )

    futures: list[dask.distributed.Future] = []
    worker_solution_paths: list[Path] = []
    task_index = 0
    for tstart, ntimes in partition_it:
        worker_ddecal_options = ddecal_options | {
            "msin.starttime": tstart,
            "msin.ntimes": ntimes,
        }
        worker_solution_path = Path(
            str(output_solution_path) + f".part{task_index:03d}"
        )
        worker_solution_paths.append(worker_solution_path)

        args = (input_ms, sourcedb, worker_solution_path)
        kwargs = {
            "ddecal_options": worker_ddecal_options,
            "modifiers": modifiers,
            "keep_running": keep_running,
            "log_extra": log_extra | {"task_index": task_index},
        }

        # NOTE: We implicitly assume that the dask workers each have 1 of the
        # "subprocess_slots" resource. This is a mechanism to ensure that only
        # one subprocess may run at a given time on a worker.
        fut = dask_client.submit(
            worker_run_ddecal_locally,
            *args,
            **kwargs,
            resources={"subprocess_slots": 1},
        )
        futures.append(fut)
        task_index += 1

    LOGGER.info(f"Submitted {task_index} DDECal tasks")

    atexit.register(cancel_all_ddecal_tasks, keep_running, futures)

    # Block until we've got all results
    for num_finished, fut in enumerate(
        dask.distributed.as_completed(futures), start=1
    ):
        # NOTE: explicitly request the result even if nothing is returned.
        # This is to ensure than any exception from the task is re-raised
        fut.result()
        LOGGER.debug(
            f"{num_finished}/{len(futures)} DDECal tasks complete",
            extra=log_extra,
        )
    atexit.unregister(cancel_all_ddecal_tasks)

    result = concatenate_solution_files(
        worker_solution_paths, output_solution_path
    )
    for path in worker_solution_paths:
        path.unlink()
        LOGGER.debug(f"Deleted file: {path}", extra=log_extra)
    return result


def default_ddecal_options() -> dict[str, Any]:
    """
    A dictionary containing the default DDECal options. These can be overriden
    by the user later on.
    """
    return {
        "solve.mode": "scalarphase",
        "solve.llssolver": "qr",
        "solve.maxiter": 150,
        "solve.solveralgorithm": "hybrid",
        "solve.tolerance": 1e-3,
    }


def make_ddecal_command(
    input_ms: Path,
    sourcedb: Path,
    output_solution_path: Path,
    ddecal_options: dict[str, Any],
) -> DP3Command:
    """
    Self-explanatory.
    """
    options = default_ddecal_options()
    options.update(ddecal_options)
    options.update(
        {
            "msin": input_ms,
            # NOTE: DDECal wants an empty msout argument
            "msout": "",
            "steps": ["solve"],
            "solve.type": "ddecal",
            "solve.sourcedb": sourcedb,
            "solve.h5parm": output_solution_path,
        }
    )
    return DP3Command(options)


def partition_iter(num_elements: int, group_size: int, num_parts: int):
    """
    Calculate the part offsets and sizes that would create the most balanced
    possible partition of a sequence of `total_size` elements into
    `num_parts` chunks, with the constraint that chunk sizes should be
    multiples of `group_size`. Yields tuples (chunk_offset, chunk_size).

    Notes:
        Yields part sizes where all except the last are guaranteed to be
        multiples of `group_size`. The last part contains the remaining
        elements that cannot constitute a full group.
        Larger parts are yielded first.
        May return fewer parts than requested.
        Does not yield zero-sized parts.

    Example:
    >>> num_elements = 105
    >>> group_size = 20
    >>> num_parts = 4
    >>> for off, size in partition_iter(num_elements, group_size, num_parts):
    ...     print(f"offset = {off:3d}, size = {size:2d}")
    offset =   0, size = 40
    offset =  40, size = 40
    offset =  80, size = 20
    offset = 100, size = 5
    """
    if num_elements < group_size:
        yield (0, num_elements)
        return

    num_full_groups, remainder = divmod(num_elements, group_size)

    # If there is an incomplete group:
    # * Solve the easier problem where there is no remainder
    # * Yield the remainder last
    if remainder:
        yield from partition_iter(
            num_full_groups * group_size, group_size, num_parts - 1
        )
        yield (num_elements - remainder, remainder)
        return

    # ... From this point onwards, group_size divides num_elements
    # We must have at least one full group per part
    num_parts = min(num_full_groups, num_parts)

    # Idea here is:
    # * `num_large_parts` will contain `groups_per_part + 1` groups
    # * The remaining parts will contain `groups_per_part` groups
    groups_per_part, num_large_parts = divmod(num_full_groups, num_parts)
    num_small_parts = num_parts - num_large_parts

    part_size_iter = itertools.chain(
        itertools.repeat((groups_per_part + 1) * group_size, num_large_parts),
        itertools.repeat(groups_per_part * group_size, num_small_parts),
    )

    part_offset = 0
    for part_size in part_size_iter:
        yield (part_offset, part_size)
        part_offset += part_size


def partition_measurement_set_in_time(
    mset: Path, num_parts: int, solint: int = 1
) -> Iterator[tuple[str, int]]:
    """
    Iterator that calculates a partition of a MeasurementSet in approximately
    equal time chunks. Yields tuples (start_time, part_size);
    start_time is an ISOT string, chunk_size an int that represents the
    number of time samples in the chunk.

    All parts except the last will contain a number of time samples that
    is a multiple of `solint`. May yield fewer parts than requested.

    NOTE: DP3/CASA happily take datetime strings in ISOT format as input.
    """
    day_seconds = 86400.0
    # TIME column contains timestamps in seconds elapsed since the MJD
    # reference epoch, on the UTC scale.
    # Timestamps are repeated once per baseline, keep unique only
    times: NDArray = np.unique(
        casacore.tables.table(str(mset), ack=False).getcol("TIME")
    )

    # Subtract a small (< 0.5) fraction of the sampling time to all timestamps,
    # to avoid them being rounded up when converted to ISOT strings.
    # Rounding up causes DP3 to start processing from one sample later,
    # which will cause trouble and serious casacore warnings for the last time
    # chunk. Rounding down or specifying a time less than 0.5 sampling times
    # earlier is no issue.
    fractional_sampling_time_offset = 0.25
    sampling_time = np.diff(times).min()
    times = times - fractional_sampling_time_offset * sampling_time

    times = Time(times / day_seconds, format="mjd", scale="utc")

    for offset, size in partition_iter(len(times), solint, num_parts):
        yield (times[offset].isot, size)
