"""
Contains function to perform coordinate conversions, in particular between
sky and tangent plane coordinates.
"""

import astropy.units as u
import numpy as np
from astropy.coordinates import SkyCoord
from numpy.typing import ArrayLike, NDArray


def orthographic_from_sky(coords_sky: SkyCoord, origin: SkyCoord) -> NDArray:
    """
    Returns the orthographic tangent plane coordinates of the given sky
    coordinates. These are the usual `l` and `m` coordinates, both within
    [-1.0, +1.0].

    Args:
        coords_sky: Sky coordinates of one or multiple points as a single
            SkyCoord object.
        origin: Origin of the tangent plane as a SkyCoord object. This point's
            coordinates in the tangent plane are (0, 0) by definition.

    Returns:
        Orthographic tangent plane coordinates as a numpy array of shape
        (num_points, 2). The columns contain the `l` and `m` coordinates
        in that order. The `l` axis points towards East of North, and the `m`
        axis towards North.
    """
    sep = origin.separation(coords_sky).to(u.rad).value
    pos = origin.position_angle(coords_sky).to(u.rad).value
    r = np.sin(sep)
    # NOTE: a position angle of zero corresponds to North, 90 deg is East
    l = r * np.sin(pos)  # noqa: E741, pylint: disable=invalid-name
    m = r * np.cos(pos)
    return np.vstack((l, m)).T


def sky_from_orthographic(coords_tp: ArrayLike, origin: SkyCoord) -> SkyCoord:
    """
    Returns the sky coordinates corresponding to the given orthographic tangent
    plane coordinates. The latter are the usual `l` and `m` coordinates, both
    within [-1.0, +1.0].

    Args:
        coords_tp: Orthographic tangent plane coordinates as a numpy array, or
            array-like of shape (num_points, 2). The columns are expected to
            contain the `l` and `m` coordinates in that order. The `l` axis
            points towards East of North, and the `m` axis towards North.
        origin: Origin of the tangent plane as a SkyCoord object. This point's
            coordinates in the tangent plane are (0, 0) by definition.

    Returns:
        Sky coordinates of the given points as a single SkyCoord object
        wrapping an array of num_points.
    """
    coords_tp = np.asarray(coords_tp)
    l, m = coords_tp.T  # noqa: E741, pylint: disable=invalid-name
    r = np.hypot(l, m)
    sep = np.arcsin(r)
    # NOTE: a position angle of zero corresponds to North, 90 deg is East
    pos = np.arctan2(l, m)
    return origin.directional_offset_by(pos * u.rad, sep * u.rad)
