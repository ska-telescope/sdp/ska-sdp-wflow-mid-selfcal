"""
Classes and functions to load from / save to sourcedb files.

The sourcedb format specification can be found here:
https://www.astron.nl/lofarwiki/doku.php?id=public:user_software:documentation:makesourcedb
"""
from __future__ import annotations

import io
import json
import re
from collections import OrderedDict
from dataclasses import dataclass
from pathlib import Path
from typing import Any, Callable

import astropy.units as u
import numpy
from astropy.coordinates import Angle
from numpy.typing import NDArray


class SourceDB:
    """
    Stores the contents of a sourcedb file in a convenient format that converts
    to other classes. Responsible for loading from / saving to sourcedb files.

    Rows are parsed into dictionaries whose values have their "natural" type
    (e.g. int, float, list of float, string, etc.), and default values are
    given according to what the header specifies (The "FORMAT" line).
    No keys/fields are added beyond what is specified in the header.
    No default values other than specified in the header are enforced.
    No further data cleaning or replacement is attempted.

    Units for Ra and Dec are degrees. I, Q, U, V are in Jy. MajorAxis and
    MinorAxis are in arcseconds. Orientation is in degrees. ReferenceFrequency
    is in Hz.
    """

    def __init__(
        self,
        sources: list[dict],
        patches: list[dict],
    ) -> None:
        """
        NOTE: Use classmethods instead, unless you know what you are doing.

        Create a SourceDB instance from lists of dictionary entries. The first
        element in `sources` determines which keys are expected in all other
        source entries. Very little data validation is performed at this stage.

        Args:
            sources: list of dictionaries, each representing the attributes
                of a source, in their natural types and units.
            patches: list of dictionaries, each representing the attributes
                of a patch, in their natural types and units.
        """
        if not sources:
            raise ValueError("No sources provided")

        first_source = sources[0]

        required_keys = {"Name", "Type", "Ra", "Dec", "I"}
        if not required_keys.issubset(set(first_source.keys())):
            raise ValueError(
                "First source does not contain the minimum required set of "
                f"keys: {required_keys}"
            )

        self._format_spec = FormatSpec.infer_from_source_entry(first_source)
        self._sources = sources
        self._patches = patches

        # When we parse from sourcedb files, patch entries have `Name` and
        # `Type` set to None. We enforce this assumption here for consistency.
        for p in self._patches:
            p.update({"Name": None, "Type": None})

    @property
    def sources(self) -> list[dict]:
        """
        List of dictionaries, each representing a source entry.
        """
        return self._sources

    @property
    def patches(self) -> list[dict]:
        """
        List of dictionaries, each representing a patch entry.
        """
        return self._patches

    def as_text(self) -> str:
        """
        Convert to sourcedb text, ready to be saved to a sourcedb file.
        """
        keys = self._format_spec.keys
        lines = [self._format_spec.to_string()]

        lines.append("")
        for patch in self.patches:
            lines.append(patch_entry_to_sourcedb_line(patch, keys))

        lines.append("")
        for source in self.sources:
            lines.append(source_entry_to_sourcedb_line(source, keys))

        lines.append("")  # insert newline at the end
        return "\n".join(lines)

    def save(self, path: Path):
        """
        Save as a file in sourcedb format. Output path is overwritten if it
        already exists. Extension must be either .txt or .skymodel, otherwise
        DP3 will refuse to parse it.
        """
        path = Path(path)
        valid_extensions = {".txt", ".skymodel"}
        if path.suffix not in valid_extensions:
            raise ValueError(
                f"File extension must be one of {valid_extensions!r}"
            )

        with open(path, "w") as file:
            file.write(self.as_text())

    @classmethod
    def load(cls, path: Path) -> SourceDB:
        """
        Load from a file in sourcedb format.
        """
        with open(path, "r") as file:
            return cls.from_sourcedb_text(file.read())

    @classmethod
    def from_sourcedb_text(cls, text: str) -> SourceDB:
        """
        Load from text in sourcedb format.
        """
        lines = text.split("\n")

        format_line, *body_lines = [
            line.strip()
            for line in lines
            if line and not line.isspace() and not line.startswith("#")
        ]
        format_spec = FormatSpec.from_string(format_line)

        patches = []
        sources = []
        for line in body_lines:
            entry = entry_dict_from_line(line, format_spec)
            if is_patch(entry):
                patches.append(entry)
            else:
                sources.append(entry)

        # NOTE: we do not pass the FormatSpec we just read from the file,
        # because we don't want to use the default value feature of sourcedb
        # when later exporting to file. Keep it simple.
        return SourceDB(sources, patches)

    @classmethod
    def from_json_file(cls, path: Path) -> SourceDB:
        """
        Load from JSON file. Used in test code.
        """
        with open(path, "r") as file:
            items = json.load(file)

        # Cast SpectralIndex field from list[float] to tuple[float]
        # We store SpectralIndex as a tuple[float] by design choice.
        for source_dict in items["sources"]:
            if val := source_dict.get("SpectralIndex", None):
                source_dict["SpectralIndex"] = tuple(val)

        return cls(items["sources"], items["patches"])

    def sources_as_csv(self) -> str:
        """
        Convert source entries to CSV text, ready to be written to file or
        parsed by Python libraries.
        """
        keys = self._format_spec.keys
        lines = [",".join(self._format_spec.keys)]
        for source in self.sources:
            lines.append(source_entry_to_csv_line(source, keys))
        lines.append("")  # insert newline at the end
        return "\n".join(lines)

    def sources_as_structured_array(self) -> NDArray:
        """
        Return source data as a numpy structured array.
        """
        return numpy.genfromtxt(
            io.StringIO(self.sources_as_csv()),
            delimiter=",",
            dtype=None,
            names=True,
            encoding=None,
        )


@dataclass(frozen=True)
class FormatSpec:
    """
    Captures the list and order of fields in a specific sourcedb file, and
    the optional default values for some fields.
    """

    default_values: OrderedDict[str, Any]
    """
    Dictionary {field_name: default_value} that serves as a parsing plan for
    a given sourcedb file.
    """

    @property
    def keys(self) -> tuple[str]:
        """
        Tuple of field names in the attached sourcedb file, in the right order.
        """
        return tuple(self.default_values.keys())

    @classmethod
    def from_string(cls, string: str) -> FormatSpec:
        """
        Create a FormatSpec object from the header line of a sourcedb file.
        """
        match = re.match(r"(format|FORMAT|Format)(\s*=\s*)(.*)", string)
        if match is None:
            raise ValueError(
                "SourceDB format string must start with "
                "'(format|FORMAT|Format) = '"
            )

        string = match.group(3)
        field_specifiers = split_outside_single_quotes(string, ",")
        field_specifiers = list(map(str.strip, field_specifiers))

        default_values = []
        for fspec in field_specifiers:
            if "=" not in fspec:
                key = fspec
                val = None
            else:
                key, val = fspec.split("=")
                val = val.strip("'")
                val = FIELD_PARSERS[key](val)

            default_values.append((key, val))
        return cls(OrderedDict(default_values))

    @classmethod
    def infer_from_source_entry(cls, entry: dict[str, Any]) -> FormatSpec:
        """
        Create a FormatSpec based on the keys in given source entry.

        By design choice, the returned FormatSpec will NOT have any default
        values. Keys are reasonably re-ordered (e.g. Name and Type will appear
        first).
        """
        keys = [key for key in KEY_ORDER if key in entry]
        keys = keys + list(set(entry.keys()).difference(keys))
        default_values = [(key, None) for key in keys]
        return cls(OrderedDict(default_values))

    def to_string(self) -> str:
        """
        Convert to a string, ready to be written as the header line of a
        sourcedb file.
        """
        specifiers = []
        for key, val in self.default_values.items():
            if val is None:
                spec = key
            else:
                spec = f"{key}='{val}'"
            specifiers.append(spec)
        specifier_string = ", ".join(specifiers)
        return f"FORMAT = {specifier_string}"


def _parse_bool(text: str) -> bool:
    if text not in ("true", "false"):
        raise ValueError("Not a valid boolean string representation")
    return text == "true"


def _parse_spectral_index(text: str) -> tuple[float]:
    """
    NOTE: Parsing SpectralIndex into a tuple of floats rather than a list of
    floats is a design choice, because we want our source entry dictionaries
    to contain only hashable types, which then makes unit testing / comparing
    such entry dictionaries easier.

    The downside is that we need to be a bit more careful when parsing
    from / converting to sourcedb text or JSON. sourcedb stores spectral index
    with square brackets, JSON has no concept of tuple, only of list.
    """
    if not (stripped := text.strip("[]")):
        return []
    return tuple(map(float, stripped.split(",")))


def _format_spectral_index(spectral_index: tuple[float]) -> str:
    """
    Convert spectral index tuple to sourcedb string representation in square
    brackets.
    """
    return str(list(spectral_index))


def _parse_mvangle(text: str) -> float:
    """
    Parse MVAngle string to a value in degrees.

    Angles can be given using the MVAngle format, which includes:
    * Sexagesimal HMS format like 12:34:56.78 or 12h34m56.78.
      Note that using colons means hours. It is not allowed to use colons in
      declination values.
    * Sexagesimal DMS format like 12.34.56.78 or 12d34m56.78. Using only points
      means degrees.
    * Floating-point value optionally followed by a unit like 12.34deg or
      12.34rad. If no unit is given, radians are used.
    """
    # astropy's Angle class handles the following out of the box:
    # Angle("1.23deg") -> deg
    # Angle("1.23rad") -> rad
    # Angle("1h23m45.67") -> hourangle
    # Angle("1h23m45.67s") -> hourangle
    # Angle("1d23m45.67") -> deg
    # Angle("1d23m45.67s") -> deg
    # Which means we only need to take care of:
    # "1:23:45.67" -> hourangle
    # "1.23.45.67" -> deg
    chars = set(text)
    if not chars.intersection(set("drh")):
        if ":" in text:
            return Angle(text, unit=u.hourangle).to(u.deg).value

        text = text.replace(".", ":", 2)
        return Angle(text, unit=u.deg).value

    return Angle(text).to(u.deg).value


# Parsing function for each field
FIELD_PARSERS: dict[str, Callable[[str], Any]] = {
    "Name": str,
    "Type": str,
    "Patch": str,
    "Ra": _parse_mvangle,
    "Dec": _parse_mvangle,
    "I": float,
    "Q": float,
    "U": float,
    "V": float,
    "SpectralIndex": _parse_spectral_index,
    "LogarithmicSI": _parse_bool,
    "ReferenceFrequency": float,
    "MajorAxis": float,
    "MinorAxis": float,
    "Orientation": float,
}

# When exporting a sourcedb file, this is the desired order for the field
# names in each entry. Fields not specified below will be written in arbritrary
# order.
# NOTE: the code relies on the assumption that all the mandatory fields for a
# patch appear first in this list (Name, Type, Patch, Ra, Dec)
KEY_ORDER = [
    "Name",
    "Type",
    "Patch",
    "Ra",
    "Dec",
    "I",
    "Q",
    "U",
    "V",
    "MajorAxis",
    "MinorAxis",
    "Orientation",
    "LogarithmicSI",
    "SpectralIndex",
    "ReferenceFrequency",
]

# Set of keys that must be written out for a patch entry
MANDATORY_PATCH_KEYS = {"Name", "Type", "Patch", "Ra", "Dec"}


def split_outside_single_quotes(string: str, delimiter_char: str) -> list[str]:
    """
    Split string on single-character delimiter, but do not split if the
    delimiter is inside a section enclosed in single quotes.
    """
    buffer = ""
    result = []
    within_quotes = False
    for char in string:
        if char == "'":
            within_quotes = not within_quotes
        if not within_quotes and char == delimiter_char:
            result.append(buffer)
            buffer = ""
            continue
        buffer = buffer + char

    result.append(buffer)
    return result


def split_outside_square_brackets(
    string: str, delimiter_char: str
) -> list[str]:
    """
    Split string on single-character delimiter, but do not split if the
    delimiter is inside a section enclosed in square brackets.
    """
    buffer = ""
    result = []
    depth = 0
    for char in string:
        if char == "[":
            depth += 1
        if char == "]":
            depth = max(depth - 1, 0)
        if not depth and char == delimiter_char:
            result.append(buffer)
            buffer = ""
            continue
        buffer = buffer + char

    result.append(buffer)
    return result


def is_patch(entry: dict) -> bool:
    """
    Whether the given entry corresponds to a patch.
    """
    return entry["Name"] is None


def entry_dict_from_line(line: str, format_spec: FormatSpec) -> dict:
    """
    Parse a line for a sourcedb file into a dictionary.
    `format_spec` defines the order of the fields and how to handle missing
    values.
    """
    # Work around the fact that SpectralIndex may contain commas within
    # square brackets, but we don't want to split on those
    unparsed_values = split_outside_square_brackets(line, ",")
    unparsed_values = list(map(str.strip, unparsed_values))

    # Make an iterator because we may consume it only partially at first
    key_defval_iter = iter(format_spec.default_values.items())

    result = {}
    for unparsed_value, (key, default_value) in zip(
        unparsed_values, key_defval_iter
    ):
        if not unparsed_value or unparsed_value.isspace():
            result[key] = default_value
        else:
            parser = FIELD_PARSERS[key]
            result[key] = parser(unparsed_value)

    if is_patch(result):
        return result

    # If source entry, add any leftover default values whose keys do not
    # appear in `line`. This happens when FORMAT contains one or more fields
    # with default values at the end. In this case, it is allowed to NOT
    # specify those in `line`.
    for key, default_value in key_defval_iter:
        result[key] = default_value

    return result


def source_entry_to_sourcedb_line(entry: dict, keys: list[str]) -> str:
    """
    Convert source entry into a string, ready to be written as a line in a
    sourcedb file. `keys` provides the order in which to write out the fields.
    """
    formatted_values = []
    for key in keys:
        val = entry[key]
        # We want to fail if either Ra or Dec is None
        # Also, force 12 decimal places, because we get less by default.
        # Not only do we need milli-arcsec precision, but the unit tests
        # depend on this.
        if key in ("Ra", "Dec"):
            val_str = f"{val:.12f}deg"
        # Handle None case before anything else
        elif val is None:
            val_str = ""
        elif key == "LogarithmicSI":
            val_str = str(val).lower()
        elif key == "SpectralIndex":
            # We store SpectralIndex as a tuple[float], but must format it
            # as a list in square brackets
            val_str = _format_spectral_index(val)
        else:
            val_str = str(val)
        formatted_values.append(val_str)
    return ", ".join(formatted_values)


def patch_entry_to_sourcedb_line(entry: dict, keys: list[str]) -> str:
    """
    Convert patch entry into a string, ready to be written as a line in a
    sourcedb file. `keys` provides the order in which to write out the fields.
    """
    # For patches, we don't need or want to write all the keys that would be
    # mandatory for a source.
    keys = [key for key in keys if key in MANDATORY_PATCH_KEYS]
    return source_entry_to_sourcedb_line(entry, keys)


def source_entry_to_csv_line(entry: dict, keys: list[str]) -> str:
    """
    Convert source entry into a string, ready to be written as a line in a
    CSV file. `keys` provides the order in which to write out the fields.
    """
    formatted_values = []
    for key in keys:
        val = entry[key]
        if key in ("Ra", "Dec"):
            val_str = f"{val:.12f}"
        elif val is None:
            # 'NaN' seems to be handled as missing data by both
            # numpy.genfromtxt and pandas.read_csv
            val_str = "NaN"
        elif key == "SpectralIndex":
            # Must enclose comma-separated list in double quotes
            # We store SpectralIndex as a tuple[float], but must format it
            # as a list in square brackets
            val_str = f'"{_format_spectral_index(val)}"'
        else:
            val_str = str(val)
        formatted_values.append(val_str)
    return ",".join(formatted_values)
