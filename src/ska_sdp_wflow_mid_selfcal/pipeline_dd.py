from __future__ import annotations

import json
import shutil
from pathlib import Path
from typing import Optional, Sequence

import dask.distributed

from ska_sdp_wflow_mid_selfcal.calibration_sky_model import CalibrationSkyModel
from ska_sdp_wflow_mid_selfcal.ddecal import run_ddecal
from ska_sdp_wflow_mid_selfcal.field import Field
from ska_sdp_wflow_mid_selfcal.image_processing import FITSImage
from ska_sdp_wflow_mid_selfcal.imaging import (
    DEFAULT_MAX_FRACTIONAL_BANDWIDTH,
    ImagingFactory,
    intersect_sky_model_with_thresholded_clean_image,
)
from ska_sdp_wflow_mid_selfcal.logutils import LOGGER
from ska_sdp_wflow_mid_selfcal.measurement_set import MeasurementSet
from ska_sdp_wflow_mid_selfcal.skymodel import SkyModel
from ska_sdp_wflow_mid_selfcal.tesselation import Tesselation


# pylint: disable=too-many-locals
def selfcal_pipeline_dd(
    input_ms: MeasurementSet,
    *,
    num_pixels: int,
    pixel_scale_asec: float,
    config_dict: dict,
    outdir: Path,
    sky_model: Optional[SkyModel] = None,
    singularity_image: Optional[Path] = None,
    dask_client: Optional[dask.distributed.Client] = None,
    mpi_hosts: Optional[Sequence[str]] = None,
) -> None:
    """
    Run the direction-dependent (DD) self-calibration pipeline.

    Args:
        input_ms: input MeasurementSet.
        sky_model: optional initial skymodel to bootstrap calibration.
            NOTE: the source fluxes provided must be *apparent* fluxes, that is
            with the primary beam attenuation already applied.
        num_pixels: output image size in pixels.
        pixel_scale_asec: angular scale of the image pixels in arcseconds.
        config_dict: a dictionary of parameters parsed from the YAML
            configuration file for the pipeline.
        outdir: the directory path where temporary files and data products will
            be written.
        singularity_image: path to the Singularity image if using
            containerization for processing. If not specified, run DP3 and
            WSClean on bare metal.
        dask_client: optional Dask client via which to submit jobs to a Dask
            cluster. If specified, any eligible pipeline step will be
            distributed on the associated Dask cluster.
        mpi_hosts: optional sequence of host names on which to run wsclean-mp.
            If None, or if the sequence is of length 1 or less, just run the
            basic wsclean executable.
    """
    LOGGER.info(f"Input MeasurementSet: {input_ms!r}")
    LOGGER.info(
        f"Configuration dictionary:\n{json.dumps(config_dict, indent=4)}"
    )

    # NOTE: Paths must be made absolute now. The risk is that some code will
    # be executed on remote dask workers, and it may refer to those paths
    # while the python interpreter runs from a different working directory.
    # NOTE: Resolve symlinks because if running within singularity container,
    # we need to mount the directory/ies that contain the actual MS(es)
    outdir = outdir.resolve()
    singularity_image = (
        singularity_image.resolve() if singularity_image else None
    )
    field = Field(input_ms.phase_centre, num_pixels, pixel_scale_asec)
    max_fractional_bandwidth = float(
        config_dict.get(
            "max_fractional_bandwidth", DEFAULT_MAX_FRACTIONAL_BANDWIDTH
        )
    )
    LOGGER.info(f"Using max_fractional_bandwidth = {max_fractional_bandwidth}")

    imaging_factory = ImagingFactory(
        input_ms,
        field,
        mpi_hosts=mpi_hosts,
        singularity_image=singularity_image,
        max_fractional_bandwidth=max_fractional_bandwidth,
    )

    # Make a bootstrap sky model if none provided
    if not sky_model:
        LOGGER.info("Running initial imaging stage")
        stage_outdir = outdir / "initial_imaging"
        stage_outdir.mkdir(parents=True)

        initial_imaging = imaging_factory.create_initial_imaging_stage(
            stage_outdir,
            user_options=config_dict["initial_imaging"]["wsclean"],
            log_extra={"stage": "image", "cycle": 0},
        )
        initial_imaging.execute()
        sky_model = SkyModel.load_sourcedb(initial_imaging.source_list_path)
        sky_model = intersect_sky_model_with_thresholded_clean_image(
            sky_model, initial_imaging.clean_image_path
        )

    # Save the initial sky model
    LOGGER.info(f"Input SkyModel: {len(sky_model)} sources")
    sky_model.save_sourcedb(outdir / "initial_skymodel.txt")
    sky_model.save_ds9(outdir / "initial_skymodel.reg")

    num_cycles = len(config_dict["selfcal_cycles"])

    for icycle, cycle_params in enumerate(
        config_dict["selfcal_cycles"], start=1
    ):
        LOGGER.info(f"Starting self-calibration cycle {icycle}/{num_cycles}")

        cycle_outdir = outdir / f"dd_cycle_{icycle:02d}"
        cycle_outdir.mkdir(parents=True)

        sky_model.save_ds9(cycle_outdir / "input_skymodel.reg")
        sky_model.save_sourcedb(cycle_outdir / "input_skymodel.txt")

        tesselation = Tesselation.create(
            field,
            sky_model,
            num_patches=cycle_params["tesselation"]["num_patches"],
            method=cycle_params["tesselation"]["method"],
        )

        calibration_sky_model = CalibrationSkyModel(sky_model, tesselation)
        calibration_sky_model.print_patch_stats(consumer=LOGGER.info)

        # NOTE: DP3 expects the extension of a sourcedb file to be either
        # .skymodel or .txt
        calibration_sourcedb = cycle_outdir / "calibration_skymodel.txt"
        calibration_sky_model.save_sourcedb(calibration_sourcedb)
        solutions = cycle_outdir / "solutions.h5parm"

        # Run DDECal
        solution_file = run_ddecal(
            input_ms.path,
            calibration_sourcedb,
            solutions,
            ddecal_options=cycle_params["ddecal"],
            singularity_image=singularity_image,
            dask_client=dask_client,
            log_extra={"stage": "calibrate", "cycle": icycle},
        )

        # Run Imaging
        imaging = imaging_factory.create_direction_dependent_imaging_stage(
            tesselation,
            solution_file,
            cycle_outdir,
            user_options=cycle_params["wsclean"],
            log_extra={"stage": "image", "cycle": icycle},
        )
        imaging.execute()

        # Use output sky model as input for next calibration cycle
        # Sort by decreasing flux for human readability
        sky_model = sky_model.sorted_by_decreasing_flux()

        # Save diagnostic plots
        sky_model = SkyModel.load_sourcedb(imaging.source_list_path)
        sky_model.plot(field.imaging_centre).savefig(
            cycle_outdir / "output_sources.png"
        )

        # Log RMS residual
        residual_image = FITSImage(imaging.residual_image_path)
        LOGGER.info(f"Computing statistics of: {residual_image.path!s}")
        LOGGER.info(f"RMS residual: {1e6 * residual_image.rms():.2f} uJy")
        LOGGER.info(
            f"Robust RMS residual: {1e6 * residual_image.robust_rms():.2f} uJy"
        )

    # Rename / move final data products
    shutil.move(imaging.clean_image_path, outdir / "final_image.fits")
    shutil.move(imaging.residual_image_path, outdir / "final_residual.fits")
    shutil.move(imaging.source_list_path, outdir / "final_skymodel.txt")
    sky_model.save_ds9(outdir / "final_skymodel.reg")
