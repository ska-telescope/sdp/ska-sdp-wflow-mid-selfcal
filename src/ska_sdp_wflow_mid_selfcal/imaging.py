import abc
import json
import logging
import math
from pathlib import Path
from typing import Any, Final, Iterator, Optional, Sequence

import numpy as np
from numpy.typing import NDArray
from scipy.spatial import KDTree

from ska_sdp_wflow_mid_selfcal.field import Field
from ska_sdp_wflow_mid_selfcal.image_processing import FITSImage
from ska_sdp_wflow_mid_selfcal.logutils import LOGGER
from ska_sdp_wflow_mid_selfcal.measurement_set import MeasurementSet
from ska_sdp_wflow_mid_selfcal.shellcmd import (
    Mpirun,
    SingularityExec,
    WSCleanCommand,
    execute_with_logger_redirection,
)
from ska_sdp_wflow_mid_selfcal.skymodel import SkyModel
from ska_sdp_wflow_mid_selfcal.solution_tables import SolutionFile
from ska_sdp_wflow_mid_selfcal.tesselation import Tesselation

DEFAULT_MAX_FRACTIONAL_BANDWIDTH: Final[float] = 0.05


# pylint:disable=too-many-instance-attributes
class Imaging(abc.ABC):
    """
    Base class for imaging stages. Some functionality is implemented only in
    derived classes.
    """

    def __init__(
        self,
        measurement_set: MeasurementSet,
        field: Field,
        output_directory: Path,
        *,
        user_options: Optional[dict[str, Any]] = None,
        mpi_hosts: Optional[Sequence[str]] = None,
        singularity_image: Optional[Path] = None,
        log_extra: Optional[dict[str, Any]] = None,
        max_fractional_bandwidth: float = DEFAULT_MAX_FRACTIONAL_BANDWIDTH,
    ) -> None:
        """
        Create a new Imaging instance.
        """
        self._measurement_set = measurement_set
        self._field = field
        self._user_options = user_options if user_options else {}
        self._output_directory = output_directory
        self._num_nodes = len(mpi_hosts) if mpi_hosts else 1
        self._log_extra = log_extra

        # Set command modifiers
        self._modifiers = []
        if mpi_hosts:
            self._modifiers.append(Mpirun(mpi_hosts))
        if singularity_image:
            self._modifiers.append(SingularityExec(singularity_image))

        # Set wideband deconvolution options if necessary
        self._wideband_options = {}
        self._set_wideband_options(max_fractional_bandwidth)

    def _set_wideband_options(self, max_fractional_bandwidth: float) -> None:
        mset = self._measurement_set
        min_deconv_channels = compute_min_deconv_channels(
            mset.bottom_freq_hz,
            mset.total_bandwidth_hz,
            mset.num_channels,
            max_fractional_bandwidth=max_fractional_bandwidth,
        )
        self.log(
            f"Minimum required deconvolution channels: {min_deconv_channels}"
        )
        self.log(f"Nodes provided: {self._num_nodes}")
        self._wideband_options = compute_wideband_options(
            channels_in=mset.num_channels,
            min_deconv_chans=min_deconv_channels,
            num_nodes=self._num_nodes,
        )
        self.log(
            "Calculated wideband options:\n"
            f"{json_dumps_dict(self._wideband_options)}"
        )

    def log(self, msg: str, level: int = logging.INFO) -> None:
        """
        Emit log message to pipeline logger at given level, adding the
        adequate extra records.
        """
        LOGGER.log(level, msg, extra=self._log_extra)

    @property
    def wideband_enabled(self) -> bool:
        """
        Whether wideband deconvolution is enabled on the WSClean call.
        Affects the name of output FITS files, which will contain "MFS" if
        enabled.
        """
        return self._wideband_options.get("join-channels", False)

    def _fits_image_path(self, denomination: str) -> Path:
        mfs = "-MFS" if self.wideband_enabled else ""
        fname = f"{self.wsclean_basename}{mfs}-{denomination}.fits"
        return self.output_directory / fname

    @property
    def output_directory(self) -> Path:
        """
        Output directory for WSClean.
        """
        return self._output_directory

    @property
    def clean_image_path(self) -> Path:
        """
        Expected Path to the output clean image.
        """
        return self._fits_image_path("image")

    @property
    def residual_image_path(self) -> Path:
        """
        Expected Path to the residual clean image.
        """
        return self._fits_image_path("residual")

    @property
    def source_list_path(self) -> Path:
        """
        Expected Path to source list saved by WSClean.
        """
        return self.output_directory / f"{self.wsclean_basename}-sources.txt"

    @property
    def wsclean_basename(self) -> str:
        """
        Base name for WSClean outputs, passed to the -name option.
        """
        return "wsclean"

    @abc.abstractmethod
    def default_options(self) -> dict[str, Any]:
        """
        Reasonable default options for this imaging stage.
        """
        raise NotImplementedError

    @abc.abstractmethod
    def special_options(self) -> dict[str, Any]:
        """
        Special options for this stage that are enforced *after* the user
        options have been set.
        """
        raise NotImplementedError

    def image_and_path_options(self) -> dict[str, Path]:
        """
        Options pertaining to paths and image dimensions that are always
        set last.
        """
        npix = self._field.num_pixels
        scale = self._field.pixel_scale_asec
        return {
            "size": (npix, npix),
            "scale": f"{scale}asec",
            "name": self.output_directory / self.wsclean_basename,
            "temp-dir": self.output_directory,
        }

    def options(self) -> dict[str, Any]:
        """
        Complete dictionary of options to call WSClean with.
        """
        opts = {}
        opts.update(self.default_options())
        opts.update(self._user_options)
        opts.update(self._wideband_options)
        opts.update(self.special_options())
        opts.update(self.image_and_path_options())
        return opts

    def run_wsclean(self) -> None:
        """
        Run WSClean with the right configuration, including usage of
        singularity, mpirun, logging redirection, etc.
        """
        cmd = WSCleanCommand(
            [self._measurement_set.path], options=self.options()
        )
        self.log(
            "WSClean options dictionary:\n" f"{json_dumps_dict(cmd.options)}"
        )
        execute_with_logger_redirection(
            cmd, self._modifiers, extra=self._log_extra
        )

    @abc.abstractmethod
    def execute(self) -> Any:
        """
        Execute the stage. Involves running WSClean, with some pre- and
        post-processing steps left to the discretion of the derived classes.
        """
        raise NotImplementedError


class DirectionDependentImaging(Imaging):
    """
    Represents a WSClean imaging stage that uses facets.
    """

    # pylint:disable=too-many-arguments
    def __init__(
        self,
        measurement_set: MeasurementSet,
        field: Field,
        tesselation: Tesselation,
        solution_file: SolutionFile,
        output_directory: Path,
        *,
        user_options: Optional[dict[str, Any]] = None,
        mpi_hosts: Optional[Sequence[str]] = None,
        singularity_image: Optional[Path] = None,
        log_extra: Optional[dict[str, Any]] = None,
        max_fractional_bandwidth: float = DEFAULT_MAX_FRACTIONAL_BANDWIDTH,
    ) -> None:
        """
        Create a new DirectionDependentImaging instance. Both a Tesselation and
        SolutionFile must be provided on top of the base Imaging parameters.
        """
        super().__init__(
            measurement_set,
            field,
            output_directory,
            user_options=user_options,
            mpi_hosts=mpi_hosts,
            singularity_image=singularity_image,
            log_extra=log_extra,
            max_fractional_bandwidth=max_fractional_bandwidth,
        )
        self._tesselation = tesselation
        self._solution_file = solution_file

    @property
    def facet_regions_path(self) -> Path:
        """
        Path where the facets region file passed to WSClean is saved.
        """
        return self.output_directory / "facet_regions.reg"

    def default_options(self) -> dict[str, Any]:
        return {
            "auto-mask": 5.0,
            "auto-threshold": 1.0,
            "mgain": 0.8,
            "weight": ("briggs", +0.5),
            "multiscale": True,
            # Limit number of multiscale clean scales, otherwise we easily
            # run out of RAM
            "multiscale-max-scales": 4,
            "niter": 10_000_000,
            "gridder": "wgridder",
        }

    def special_options(self) -> dict[str, Any]:
        return {
            # NOTE: wsclean expects the names of the solution tabs to apply as
            # a comma-separated list, e.g. "amplitude000,phase000"
            "apply-facet-solutions": (
                self._solution_file.path,
                ",".join(self._solution_file.solution_table_names()),
            ),
            "facet-regions": self.facet_regions_path,
            "no-update-model-required": True,
            "save-source-list": True,
        }

    def execute(self) -> None:
        self._tesselation.save_ds9(self.facet_regions_path)
        self.run_wsclean()


class InitialImaging(Imaging):
    """
    Represents the initial, direction-independent imaging stage used to infer
    a bootstrap sky model for DD selfcal.
    """

    # NOTE: no need to define __init__ here, since we want it to have the
    # sme signature as that of the base class.

    def default_options(self) -> dict[str, Any]:
        return {
            "mgain": 0.8,
            # Use a weighting that enhances long baselines for better
            # localisation
            "weight": ("briggs", -1.0),
            "multiscale": True,
            # Limit number of multiscale clean scales, otherwise we easily
            # run out of RAM
            "multiscale-max-scales": 4,
            "niter": 10_000_000,
            "gridder": "wgridder",
        }

    def special_options(self) -> dict[str, Any]:
        return {
            "auto-mask": 5.0,
            "auto-threshold": 1.0,
            "no-update-model-required": True,
            "save-source-list": True,
        }

    def execute(self) -> None:
        self.run_wsclean()


class ImagingFactory:
    """
    Creates imaging stage instances for a given pipeline run.
    This class is used to factorise all the common parameters to every imaging
    stage.
    """

    def __init__(
        self,
        measurement_set: MeasurementSet,
        field: Field,
        *,
        mpi_hosts: Optional[Sequence[str]] = None,
        singularity_image: Optional[Path] = None,
        max_fractional_bandwidth: float = DEFAULT_MAX_FRACTIONAL_BANDWIDTH,
    ) -> None:
        """
        Create a new ImagingStageFactory.
        This class is used to factorise all the common parameters to every
        imaging stage.
        """
        self._measurement_set = measurement_set
        self._field = field
        self._mpi_hosts = mpi_hosts
        self._singularity_image = singularity_image
        self._max_fractional_bandwidth = max_fractional_bandwidth

    def create_initial_imaging_stage(
        self,
        output_directory: Path,
        *,
        user_options: Optional[dict[str, Any]] = None,
        log_extra: Optional[dict[str, Any]] = None,
    ) -> InitialImaging:
        """
        Create an InitialImaging instance with all common parameters
        initialised to adequate values.
        """
        return InitialImaging(
            self._measurement_set,
            self._field,
            output_directory,
            user_options=user_options,
            mpi_hosts=self._mpi_hosts,
            singularity_image=self._singularity_image,
            log_extra=log_extra,
            max_fractional_bandwidth=self._max_fractional_bandwidth,
        )

    def create_direction_dependent_imaging_stage(
        self,
        tesselation: Tesselation,
        solution_file: SolutionFile,
        output_directory: Path,
        *,
        user_options: Optional[dict[str, Any]] = None,
        log_extra: Optional[dict[str, Any]] = None,
    ) -> DirectionDependentImaging:
        """
        Create an DirectionDependentImaging instance with all common parameters
        initialised to adequate values.
        """
        return DirectionDependentImaging(
            self._measurement_set,
            self._field,
            tesselation,
            solution_file,
            output_directory,
            user_options=user_options,
            mpi_hosts=self._mpi_hosts,
            singularity_image=self._singularity_image,
            log_extra=log_extra,
            max_fractional_bandwidth=self._max_fractional_bandwidth,
        )


def compute_wideband_options(
    *, channels_in: int, min_deconv_chans: int, num_nodes: int
) -> dict[str, Any]:
    """
    Compute valid deconvolution_channels and channels_out parameters.
    If possible, find a combination where deconvolution_channels divides
    channels_out, and channels_out divides channels_in.

    If wideband deconvolution is not required, returns an empty dictionary.
    Otherwise, returns a dictionary of the form:
    ```
    {
        "channels-out": int,
        "deconvolution-channels": int,
        "fit-spectral-pol": int,
        "join-channels": True
    }
    ```
    """

    num_nodes = min(num_nodes, channels_in)

    def clipped_inclusive_range(start: int, stop: int) -> range:
        start = min(start, channels_in)
        stop = min(stop, channels_in) + 1
        return range(start, stop)

    def iterate_combinations() -> Iterator[tuple[int, int]]:
        # Try to keep deconvolution_channels to a minimum, but explore a range
        # to find values that divide channels_in and channels_out
        deconv_chans_range = clipped_inclusive_range(
            min_deconv_chans, 2 * min_deconv_chans
        )
        for deconv_chans in deconv_chans_range:
            channels_out_range = clipped_inclusive_range(
                max(deconv_chans, 2 * num_nodes),
                max(4 * min_deconv_chans, 4 * num_nodes),
            )
            for channels_out in channels_out_range:
                yield deconv_chans, channels_out

    def score_combination(combination: tuple[int, int]) -> int:
        deconv_chans, channels_out = combination
        return sum(
            (
                channels_in % deconv_chans == 0,
                channels_in % channels_out == 0,
                channels_out % deconv_chans == 0,
            )
        )

    deconv_chans, channels_out = max(
        iterate_combinations(), key=score_combination
    )
    if channels_out <= 1:
        return {}

    return {
        "channels-out": channels_out,
        "deconvolution-channels": deconv_chans,
        "fit-spectral-pol": min(deconv_chans, 3),
        "join-channels": True,
    }


def compute_min_deconv_channels(
    bottom_freq: float,
    total_bandwidth: float,
    num_channels: int,
    *,
    max_fractional_bandwidth: float = 0.05,
) -> int:
    """
    Compute the minimum adequate value for `-deconvolution-channels`.

    Args:
        bottom_freq: Bottom frequency of the band to be imaged.
        total_bandwidth: Total bandwidth to be imaged, in the same units as
            `bottom_freq`.
        num_channels: Number of input frequency channels in the band to be
            imaged.
        max_fractional_bandwidth: The maximum allowed fractional
            bandwidth for each deconvolution sub-band.

    Returns:
        The appropriate minium integer value for `-deconvolution-channels`.

    See also:
        https://wsclean.readthedocs.io/en/latest/wideband_deconvolution.html
    """
    # For every deconvolution sub-band we want:
    # band_bw / band_centre_freq < max_fractional_bandwidth
    # The formula below gives us the smallest number of deconvolution channels
    # that respects the condition above.
    eps = max_fractional_bandwidth
    result = math.ceil((2.0 - eps) * total_bandwidth / (2 * eps * bottom_freq))
    result = max(1, result)
    result = min(num_channels, result)
    return result


def json_dumps_dict(options: dict[str, Any]) -> str:
    """
    Serialize dictionary to JSON format, handling any Path objects.
    """

    def make_serializable(val: Any) -> str:
        """
        Custom function used to make Path objects serializable.
        """
        if isinstance(val, Path):
            return repr(val)
        return val

    return json.dumps(
        options, indent=4, sort_keys=True, default=make_serializable
    )


# pylint:disable=too-many-locals
def intersect_sky_model_with_thresholded_clean_image(
    smod: SkyModel, clean_image_path: Path
) -> SkyModel:
    """
    Given a SkyModel and a clean image, do the following:
    - Compute a S/N map
    - Threshold the S/N map to obtain a binary mask. The S/N threshold is
      chosen as the mid-point in log scale between 10.0 and the peak S/N.
    - Return a new SkyModel containing only sources whose centre lie within
      the binary mask pixels.
    """
    # Make a S/N map
    fits_image = FITSImage(clean_image_path)
    snr_map = fits_image.snr_map()

    # Choose S/N threshold, threshold the image, get a binary mask
    snr_max = snr_map.max()
    LOGGER.info(f"Initial image peak S/N = {snr_max:.1f}")

    log10_dynamic_range = np.log10(snr_max) - np.log10(10.0)
    snr_threshold = 10 ** (np.log10(snr_max) - 0.5 * log10_dynamic_range)
    LOGGER.info(f"Initial image S/N threshold = {snr_threshold:.1f}")
    mask = snr_map >= snr_threshold

    # Only keep those components within the mask
    LOGGER.info(f"Initial image components detected: {len(smod):d}")
    smod_lm = smod.source_coords_lm(fits_image.centre_sky)

    mask_y, mask_x = np.where(mask)
    step = fits_image.pixel_scale_lm
    mask_l = -step * (mask_x - fits_image.centre_x)
    mask_m = +step * (mask_y - fits_image.centre_y)
    mask_lm = np.vstack((mask_l, mask_m)).T

    kdtree = KDTree(mask_lm)
    distances, __ = kdtree.query(smod_lm)

    # NOTE: sources returned by WSClean are always located at pixel centres,
    # so a radius of 0.5 pixels is what we want here
    to_keep: NDArray = distances < 0.5 * step

    # Save said sources
    smod_trimmed = SkyModel(
        [source for source, flag in zip(smod, to_keep) if flag]
    )
    smod_trimmed = smod_trimmed.flux_thresholded(0.0)
    LOGGER.info(f"Initial image components kept: {len(smod_trimmed)}")
    return smod_trimmed
