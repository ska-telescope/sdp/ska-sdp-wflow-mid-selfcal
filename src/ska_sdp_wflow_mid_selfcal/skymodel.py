from __future__ import annotations

import collections.abc
import math
from dataclasses import dataclass
from pathlib import Path
from typing import Any, Callable, Iterable, Optional

import astropy.units as u
import matplotlib.pyplot as plt
import numpy as np
from astropy.coordinates import SkyCoord
from matplotlib.colors import LogNorm
from numpy.typing import NDArray

from ska_sdp_wflow_mid_selfcal.coordinate_conversions import (
    orthographic_from_sky,
)
from ska_sdp_wflow_mid_selfcal.sourcedb import SourceDB


@dataclass
class Shape:
    """
    Parameters for a Gaussian-shaped source. A source with a major axis FWHM
    of zero is considered a point source.

    Args:
        major_fwhm_asec: Major full-width at half-maximum in arcseconds
        minor_fwhm_asec: Minor full-width at half-maximum in arcseconds
        position_angle_deg: Position angle of the major axis, in degrees
            East of North
    """

    major_fwhm_asec: float
    minor_fwhm_asec: float
    position_angle_deg: float

    def __post_init__(self) -> None:
        if not self.major_fwhm_asec >= 0:
            raise ValueError("Major axis FWHM must be >= 0")
        if not self.major_fwhm_asec >= self.minor_fwhm_asec:
            raise ValueError("Major axis FWHM must be >= minor axis FWHM")

    @property
    def is_point(self) -> bool:
        """
        Whether the source is a point source.
        """
        return self.major_fwhm_asec == 0

    @property
    def sourcedb_type(self) -> str:
        """
        Source Type string for exporting to sourcedb format.
        """
        return "POINT" if self.is_point else "GAUSSIAN"

    @classmethod
    def point(cls) -> Shape:
        """
        Make a new Shape representing a point.
        """
        return cls(0.0, 0.0, 0.0)


@dataclass
class FluxModel:
    """
    Flux parameters for a source. See the WSClean docs for an explanation of
    the meaning of the fields:
    https://wsclean.readthedocs.io/en/latest/component_list.html

    Args:
        stokes_i: Stokes I flux in Jy, at the reference frequency
        reference_frequency_hz: Frequency at which the flux and spectral index
            polynomial expansion coefficients are defined.
        spectral_index: tuple of coefficients of a logarithmic or ordinary
            polynomial that describe the source flux as a function of
            frequency.
        logarithmic_si: Whether `spectral_index` should be interpreted as a
            list of coefficients for a polynomial in `log(nu/nu_0)`,
            where `nu_0` is the reference frequency (see below).
    """

    stokes_i: float
    reference_frequency_hz: float
    spectral_index: tuple[float]
    logarithmic_si: bool

    def __post_init__(self):
        self.spectral_index = tuple(self.spectral_index)


@dataclass
class Source:
    """
    Data class for source parameters. Points and Gaussians are supported, where
    any source with a major FWHM of zero is considered to be a point.
    """

    name: str
    ra_deg: float
    dec_deg: float
    shape: Shape
    flux_model: FluxModel

    @classmethod
    def from_sourcedb_entry(cls, entry: dict[str, Any]) -> Source:
        """
        Create a new Source instance from an entry dictionary parsed from a
        sourcedb file.
        """
        # Internally we consider that any source with a major fwhm of zero is a
        # point. We ensure consistency, by considering that the "Type" field
        # overrules the others.
        # NOTE: When WSClean outputs its component list, it leaves the shape
        # parameter fields empty for POINT sources.
        if entry["Type"] == "GAUSSIAN":
            shape = Shape(
                major_fwhm_asec=entry["MajorAxis"],
                minor_fwhm_asec=entry["MinorAxis"],
                position_angle_deg=entry["Orientation"],
            )
        else:
            shape = Shape.point()

        flux_model = FluxModel(
            stokes_i=entry["I"],
            reference_frequency_hz=entry["ReferenceFrequency"],
            spectral_index=entry["SpectralIndex"],
            logarithmic_si=entry["LogarithmicSI"],
        )

        return cls(
            name=entry["Name"],
            ra_deg=entry["Ra"],
            dec_deg=entry["Dec"],
            shape=shape,
            flux_model=flux_model,
        )

    def as_sourcedb_entry(self) -> dict[str, Any]:
        """
        Convert to a dictionary for exporting to sourcedb file. See SourceDB
        class for specifications of said dictionary.
        """
        return {
            "Name": self.name,
            "Type": self.shape.sourcedb_type,
            "Ra": self.ra_deg,
            "Dec": self.dec_deg,
            "I": self.flux_model.stokes_i,
            "ReferenceFrequency": self.flux_model.reference_frequency_hz,
            "SpectralIndex": self.flux_model.spectral_index,
            "LogarithmicSI": self.flux_model.logarithmic_si,
            "MajorAxis": self.shape.major_fwhm_asec,
            "MinorAxis": self.shape.minor_fwhm_asec,
            "Orientation": self.shape.position_angle_deg,
        }


class SkyModel(collections.abc.Sequence[Source]):
    """
    Represents a list of sources, and exposes the interface of a Sequence.
    """

    def __init__(self, sources: Iterable[Source]) -> None:
        """
        Create a new SkyModel from a list of Source objects.
        """
        self._sources = sources if isinstance(sources, list) else list(sources)

    def __getitem__(self, index):
        if isinstance(index, int):
            return self._sources[index]
        # If index is not an int, assume we are given a slice object.
        # In this case, we want to return a SkyModel, not a list.
        return SkyModel(self._sources[index])

    def __len__(self) -> int:
        return len(self._sources)

    def __add__(self, other: SkyModel) -> SkyModel:  # concatenation
        return SkyModel(self._sources + other._sources)

    def flux_thresholded(self, threshold_jy: float) -> SkyModel:
        """
        Returns another SkyModel without the sources whose stokes I flux is
        strictly less than the specified value.
        """
        return SkyModel(
            [s for s in self if s.flux_model.stokes_i >= threshold_jy]
        )

    def absolute_flux_thresholded(self, threshold_jy: float) -> SkyModel:
        """
        Returns another SkyModel without the sources whose **absolute** stokes
        I flux is strictly less than the specified value.
        """
        return SkyModel(
            [s for s in self if abs(s.flux_model.stokes_i) >= threshold_jy]
        )

    def sorted(
        self,
        key: Optional[Callable[[Source], Any]] = None,
        reverse: bool = False,
    ) -> SkyModel:
        """
        Returns another SkyModel where the underlying source list has been
        sorted. Same interface as the builtin `sorted()`.
        """
        return SkyModel(sorted(self._sources, key=key, reverse=reverse))

    def sorted_by_decreasing_flux(self) -> SkyModel:
        """
        Returns another SkyModel where the underlying source list has been
        sorted by decreasing stokes I flux.
        """
        return self.sorted(key=lambda s: s.flux_model.stokes_i, reverse=True)

    def source_coords(self) -> SkyCoord:
        """
        Returns the sky coordinates of all the sources, wrapped in a single
        SkyCoord object.
        """
        ra = [s.ra_deg for s in self]
        dec = [s.dec_deg for s in self]
        return SkyCoord(ra, dec, unit=(u.deg, u.deg))

    def source_coords_lm(self, origin: SkyCoord) -> NDArray:
        """
        Returns the orthographic tangent plane coordinates (l, m) of all the
        sources, considering a tangent plance centered on `origin`. Coordinates
        are returned as a numpy array with shape (num_sources, 2).
        """
        return orthographic_from_sky(self.source_coords(), origin)

    def source_fluxes(self) -> NDArray:
        """
        Returns the fluxes of all sources as a numpy array.
        """
        return np.asarray([s.flux_model.stokes_i for s in self])

    def within_box(
        self, centre: SkyCoord, ew_extent_deg: float, ns_extent_deg: float
    ) -> SkyModel:
        """
        Returns a new SkyModel instance containing only the sources that are
        within the specified rectangular box on sky.
        The source-in-box check is performed in the tangent plane around
        `centre` using an orthographic (sine) projection.

        Args:
            centre: SkyCoord for the centre of the box
            ew_extent_deg: Opening angle of the box along the local East-West
                direction, in degrees
            ns_extent_deg: Opening angle of the box along the local North-South
                direction, in degrees

        Returns:
            SkyModel instance containing only the sources within the box.
        """
        coords_lm = self.source_coords_lm(centre)

        l_max = math.sin(math.radians(ew_extent_deg / 2.0))
        m_max = math.sin(math.radians(ns_extent_deg / 2.0))
        mask_within_box = (abs(coords_lm[:, 0]) <= l_max) & (
            abs(coords_lm[:, 1]) <= m_max
        )
        sources_within_box = [
            s for s, inside in zip(self, mask_within_box) if inside
        ]
        return SkyModel(sources_within_box)

    def brightest(self, n: int) -> SkyModel:
        """
        Returns a new SkyModel instance containing only the `n` brightest
        sources.
        """
        return self.sorted_by_decreasing_flux()[:n]

    def as_ds9_text(self) -> str:
        """
        Convert to text in DS9 region format. Every source is exported as a
        point.
        """
        lines = [
            ('global font="helvetica 12 bold roman"'),
            "fk5",
        ]
        for source in self:
            flux_mjy = 1.0e3 * source.flux_model.stokes_i
            if source.shape.is_point:
                line = (
                    f"point({source.ra_deg},{source.dec_deg}) "
                    "# color=#00ddff "
                    f'text="{source.name} ({flux_mjy:.1f} mJy)"'
                )
            else:
                # NOTE: wsclean only deconvolves circular gaussian components,
                # so we don't bother with minor axis and orientation
                radius_deg = 0.5 * source.shape.major_fwhm_asec / 3600.0
                line = (
                    f"circle({source.ra_deg},{source.dec_deg},{radius_deg}) "
                    "# color=#e34242 "
                    f'text="{source.name} ({flux_mjy:.1f} mJy)"'
                )
            lines.append(line)
        return "\n".join(lines) + "\n"

    def save_ds9(self, path: Path) -> None:
        """
        Save this SkyModel as a DS9 region file. Every source is exported as
        a point.
        """
        with open(path, "w") as fobj:
            fobj.write(self.as_ds9_text())

    def as_sourcedb(self) -> SourceDB:
        """
        Convert to SourceDB object. No Patch column is added.
        """
        return SourceDB(
            sources=[s.as_sourcedb_entry() for s in self], patches=[]
        )

    def save_sourcedb(self, path: Path) -> None:
        """
        Save to sourcedb file. No Patch column is added.
        """
        self.as_sourcedb().save(path)

    def plot(self, origin: SkyCoord) -> plt.Figure:
        """
        Returns a plot of the sources in the tangent plane around `origin`,
        as a matplotlib Figure object.
        """
        # Make sure that faint sources get plotted last, overlaid on top of the
        # brighter ones (which have bigger markers)
        sm_sorted = self.sorted_by_decreasing_flux()

        arcmins_per_radian = np.degrees(1) * 60.0
        coords_lm = sm_sorted.source_coords_lm(origin)
        l_arcmin, m_arcmin = coords_lm.T * arcmins_per_radian
        stokes_i_mjy = np.asarray(
            [1.0e3 * s.flux_model.stokes_i for s in sm_sorted]
        )
        marker_sizes = 4.0e3 * abs(stokes_i_mjy) / abs(stokes_i_mjy).max()

        fig = plt.figure(figsize=(18, 14))
        axes = fig.gca()
        scatter = axes.scatter(
            l_arcmin,
            m_arcmin,
            s=marker_sizes,
            c=stokes_i_mjy,
            marker="o",
            norm=LogNorm(),
            alpha=0.6,
        )

        # This is to match the convention that RA increases towards the left
        axes.invert_xaxis()
        axes.set_xlabel("$l$ (arcmin)")
        axes.set_ylabel("$m$ (arcmin)")
        axes.axis("equal")
        axes.set_title(f"Total sources: {len(self)}")
        axes.grid()
        cbar = fig.colorbar(scatter)
        cbar.ax.set_ylabel(
            f"Absolute Stokes I Flux (mJy)\n"
            f"Brightest = {abs(stokes_i_mjy).max():.1f} mJy"
        )
        fig.tight_layout()
        return fig

    @classmethod
    def from_sourcedb(cls, sourcedb: SourceDB) -> SkyModel:
        """
        Load from a SourceDB object. Only sources are loaded, patch
        entries and the Patch column are ignored.
        """
        return cls(
            [Source.from_sourcedb_entry(entry) for entry in sourcedb.sources]
        )

    @classmethod
    def load_sourcedb(cls, path: Path) -> SkyModel:
        """
        Load from a file in sourcedb format. Only sources are loaded, patch
        entries and the Patch column are ignored.
        """
        return cls.from_sourcedb(SourceDB.load(path))
