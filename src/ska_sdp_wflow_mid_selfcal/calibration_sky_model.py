import collections.abc
from pathlib import Path
from typing import Callable, Iterator, NamedTuple

from ska_sdp_wflow_mid_selfcal.point_in_polygon import point_in_polygon
from ska_sdp_wflow_mid_selfcal.skymodel import SkyModel, Source
from ska_sdp_wflow_mid_selfcal.sourcedb import SourceDB
from ska_sdp_wflow_mid_selfcal.tesselation import Patch, Tesselation


class PatchStats(NamedTuple):
    """
    Per-patch statistics as a named tuple.
    """

    name: str
    num_sources: int
    flux_mjy: float
    fraction_of_assigned_flux: float


class CalibrationSkyModel(collections.abc.Mapping[Patch, SkyModel]):
    """
    Carries the information as to what sources lie in each calibration Patch.
    This class exposes the usual interface of a dictionary, where the keys
    are Patch objects, and the values SkyModel objects.
    """

    def __init__(self, sky_model: SkyModel, tesselation: Tesselation) -> None:
        """
        Create a new CalibrationSkyModel from a SkyModel and a Tesselation,
        determining which sources in `sky_model` lie in which patch of
        `tesselation`.

        Args:
            sky_model: SkyModel object from which tesselation was created.
            tesselation: Tesselation object whose patches constitute a
                partition of the field.
        """
        # Dictionary {patch_name: Patch}
        self._patches: dict[str, Patch] = {}

        # Dictionary {patch_name: SkyModel with sources inside patch}
        self._mapping: dict[str, SkyModel] = {}

        # SkyModel with sources not inside any patch
        self._unassigned: SkyModel = None

        self._initialise(sky_model, tesselation)
        self._assert_no_empty_patches()

    def __getitem__(self, patch: Patch) -> SkyModel:
        return self._mapping[patch.name]

    def __iter__(self) -> Iterator[Patch]:
        for patch_name in self._mapping:
            yield self._patches[patch_name]

    def __len__(self) -> int:
        return len(self._mapping)

    def _initialise(self, sky_model: SkyModel, tesselation: Tesselation):
        sources_lm = sky_model.source_coords_lm(tesselation.origin)

        # Create an empty list for each patch, so we can detect empty patches
        # The key 'None' is used to store unassigned sources
        patch_contents: dict[str, list[Source]] = {None: []}
        for patch in tesselation:
            patch_contents[patch.name] = []

        for point, source in zip(sources_lm, sky_model):
            x, y = point
            parent_name = None
            for patch in tesselation:
                if point_in_polygon(x, y, patch.vertices_lm):
                    parent_name = patch.name
                    break
            patch_contents[parent_name].append(source)

        unassigned_sources = patch_contents.pop(None, [])

        self._patches = {patch.name: patch for patch in tesselation}
        self._mapping = {
            patch_name: SkyModel(sources)
            for patch_name, sources in patch_contents.items()
        }
        self._unassigned = SkyModel(unassigned_sources)

    def _assert_no_empty_patches(self) -> None:
        for patch, skymod in self.items():
            enclosed_flux = sum(s.flux_model.stokes_i for s in skymod)
            if not enclosed_flux > 0:
                raise ValueError(
                    f"Patch {patch.name!r} encloses zero flux. "
                    "This may happen when using a square grid tesselation. "
                    "Adjust tesselation parameters and/or image size."
                )

    @property
    def unassigned(self) -> SkyModel:
        """
        SkyModel object containing the sources that were not assigned to any
        calibration patch.
        """
        return self._unassigned

    def patch_stats(self) -> list[PatchStats]:
        """
        Return a list of per-patch statistics, such as enclosed flux and number
        of sources.
        """
        result: list[PatchStats] = []
        for patch, skymod in self.items():
            flux_mjy = 1.0e3 * sum(
                source.flux_model.stokes_i for source in skymod
            )
            result.append(
                PatchStats(
                    patch.name,
                    num_sources=len(skymod),
                    flux_mjy=flux_mjy,
                    fraction_of_assigned_flux=None,  # calculated below
                )
            )

        total_assigned_flux = sum(s.flux_mjy for s in result)
        result = [
            s._replace(
                fraction_of_assigned_flux=s.flux_mjy / total_assigned_flux
            )
            for s in result
        ]
        return result

    def print_patch_stats(
        self, consumer: Callable[[str], None] = print
    ) -> None:
        """
        Print flux per patch and other statistics. Replace the `consumer`
        argument by e.g. `log.debug` if you want to send the output to a
        logger instead.
        """
        header_str = (
            f"{'Name':<12s} {'Sources':>8s} {'Enclosed flux (mJy)':>22s} "
            f"{'Fraction of assigned flux':>28s}"
        )
        consumer(header_str)
        consumer(len(header_str) * "-")

        patch_stats = sorted(
            self.patch_stats(), key=lambda s: s.flux_mjy, reverse=True
        )

        for source in patch_stats:
            consumer(
                f"{source.name:<12s} {source.num_sources:>8d} "
                f"{source.flux_mjy:>22.2f} "
                f"{source.fraction_of_assigned_flux:>28.2%}"
            )
        consumer(
            f"{len(self._unassigned)} sources were not assigned to any patch"
        )

    def as_sourcedb(self) -> SourceDB:
        """
        Convert to SourceDB object. This is not a reversible operation, as
        SourceDB carries strictly less information.
        """
        source_entries = []
        patch_entries = []

        for patch, skymod in self.items():
            patch_entries.append(patch.as_sourcedb_entry())

            for source in skymod:
                source_entry = source.as_sourcedb_entry()
                source_entry["Patch"] = patch.name
                source_entries.append(source_entry)

        source_entries = sorted(
            source_entries, key=lambda entry: entry["I"], reverse=True
        )
        return SourceDB(source_entries, patch_entries)

    def save_sourcedb(self, path: Path) -> None:
        """
        Export to a sourcedb file. This is not a reversible operation, as
        a sourcedb file carries strictly less information.
        """
        self.as_sourcedb().save(path)
