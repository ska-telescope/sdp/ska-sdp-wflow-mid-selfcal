import importlib.metadata
import os
import subprocess
from pathlib import Path


def _pep440_version_string() -> str:
    cwd = Path.cwd()
    thisdir = Path(__file__).parent
    try:
        os.chdir(thisdir)
        cmd = ["git", "describe", "--always", "--dirty"]
        # git describe --always --dirty will produce:
        # TAG[-DISTANCE-gHEX[-dirty]]
        # PEP440 version string should be like:
        # TAG[+DISTANCE.gHEX[.dirty]]
        return (
            subprocess.check_output(cmd, stderr=subprocess.DEVNULL)
            .decode()
            .strip()
            .replace("-", "+", 1)
            .replace("-", ".")
        )
    # If the git command fails, then the code was not installed in development
    # mode (i.e. it does not live inside a git repo), and it's thus fine to
    # fall back onto the last release version.
    except subprocess.CalledProcessError:
        return importlib.metadata.version(__package__)
    finally:
        os.chdir(cwd)


__version__ = _pep440_version_string()
