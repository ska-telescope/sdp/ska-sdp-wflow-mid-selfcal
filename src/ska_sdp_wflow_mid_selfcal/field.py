from dataclasses import dataclass

import numpy as np
from astropy.coordinates import SkyCoord


@dataclass
class Field:
    """
    Parameters of the imaging field.
    """

    imaging_centre: SkyCoord
    """Imaging centre, usually the same as the phase centre"""

    num_pixels: int
    """Number of pixels across the image"""

    pixel_scale_asec: float
    """Scale of a pixel in arcseconds"""

    @property
    def fov_deg(self) -> float:
        """
        Field of view in degrees.
        """
        return self.num_pixels * self.pixel_scale_asec / 3600.0

    @property
    def l_max(self) -> float:
        """
        Returns the max `l` coordinate inside an image of this field.
        This is the max abscissa in the tangent plane, using the orthographic
        (sine) projection.
        """
        half_fov_rad = np.radians(self.fov_deg / 2.0)
        return np.sin(half_fov_rad)

    @property
    def m_max(self) -> float:
        """
        Returns the max `m` coordinate inside an image of this field.
        This is the max ordinate in the tangent plane, using the orthographic
        (sine) projection.
        """
        return self.l_max
