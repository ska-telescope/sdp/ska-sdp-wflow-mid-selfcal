from __future__ import annotations

from collections import defaultdict
from dataclasses import dataclass
from pathlib import Path
from typing import Callable, Literal, Optional, Sequence, TypeVar, Union

import h5py
import numpy as np
from numpy.typing import NDArray

H5pyObject = Union[h5py.File, h5py.Group, h5py.Dataset]


def concatenate_solution_files(
    input_paths: Sequence[Path], output_path: Path
) -> SolutionFile:
    """
    Concatenate H5Parm solution tables along the time axis.

    Args:
        input_paths: list or sequence of input file paths
        output_path: path where the result of the concatenation should be
            written

    Returns:
        SolutionFile object representing the concatenated output.
    """
    return SolutionFile.concatenate(
        [SolutionFile(path) for path in input_paths], output_path
    )


def _raise_mismatched_attr_error(cls_name: str, attr_name: str) -> None:
    raise ValueError(
        f"Expected all {cls_name} objects to have the same "
        f"{attr_name!r} attribute"
    )


T = TypeVar("T")


def _assert_scalar_attr_identical(items: Sequence[T], name: str) -> None:
    _assert_attr_identical(items, name, lambda x, y: x == y)


def _assert_array_attr_identical(items: Sequence[T], name: str) -> None:
    _assert_attr_identical(items, name, np.array_equal)


def _assert_attr_identical(
    items: Sequence[T], name: str, equal: Callable[[T, T], bool]
) -> None:
    first = items[0]
    ref_value = getattr(first, name)
    if not all(equal(getattr(item, name), ref_value) for item in items):
        _raise_mismatched_attr_error(type(first).__name__, name)


# pylint:disable=too-many-instance-attributes
@dataclass(repr=False)
class SolutionTable:
    """
    Maps to a soltab, e.g. 'sol000/phase000'.
    """

    name: str
    """Name of the soltab, e.g. 'sol000/phase000'"""

    soltype: Literal["amplitude", "phase"]
    """
    Type of the solution. This maps to the `TITLE`
    attribute of the solution table's HDF5 group. For tables produced by
    DDECal, this can only be 'phase' or 'amplitude', even when it is called
    in `scalarphase` solving mode.

    The complete list of soltypes, according to the losoto docs, can be found
    here: https://support.astron.nl/LOFARImagingCookbook/losoto.html
    """

    axes: list[str]
    """
    List of axis names, which describe the axis order of the `val` and `weight`
    attributes.
    """

    ant: NDArray
    dir: NDArray
    freq: NDArray
    time: NDArray
    val: NDArray
    weight: NDArray

    @property
    def start_time(self) -> float:
        """
        Timestamp of the first time sample, in seconds since the MJD reference
        epoch.
        """
        return self.time[0]

    @property
    def end_time(self) -> float:
        """
        Timestamp of the last time sample, in seconds since the MJD reference
        epoch.
        """
        return self.time[-1]

    def write_to_hdf5_group(self, file: h5py.File) -> None:
        """
        Create a new sub-group in given HDF5 file, and write the SolutionTable
        into said sub-group.
        """
        group = file.create_group(self.name)
        # NOTE: any string attributes must be written as np.bytes_, otherwise
        # WSClean/DP3 won't be able to correctly read them.
        group.attrs["TITLE"] = np.bytes_(self.soltype)

        def write_column(colname: str, attrs: Optional[dict] = None) -> None:
            dset = group.create_dataset(colname, data=getattr(self, colname))
            if attrs:
                dset.attrs.update(attrs)

        # `val` and `weight` datasets must carry the axis order as an attribute
        axes_string = np.bytes_(",".join(self.axes))
        multidim_array_attrs = {"AXES": axes_string}

        write_column("ant")
        write_column("dir")
        write_column("freq")
        write_column("time")
        write_column("val", multidim_array_attrs)
        write_column("weight", multidim_array_attrs)

    @classmethod
    def concatenate(cls, tables: Sequence[SolutionTable]) -> SolutionTable:
        """
        Concatenate solution tables along the time axis. They are re-ordered
        by increasing start time beforehand.
        """
        _assert_scalar_attr_identical(tables, "name")
        _assert_scalar_attr_identical(tables, "soltype")
        _assert_array_attr_identical(tables, "ant")
        _assert_array_attr_identical(tables, "dir")
        _assert_array_attr_identical(tables, "freq")

        tables = sorted(tables, key=lambda t: t.start_time)

        for cur, nxt in zip(tables[:-1], tables[1:]):
            if cur.start_time <= nxt.start_time <= cur.end_time:
                raise ValueError(
                    "Cannot concatenate soution tables: time ranges overlap"
                )

        def concatenated_column(colname: str) -> NDArray:
            return np.concatenate(
                [getattr(t, colname) for t in tables], axis=0
            )

        first = tables[0]
        return cls(
            first.name,
            first.soltype,
            first.axes,
            first.ant,
            first.dir,
            first.freq,
            concatenated_column("time"),
            concatenated_column("val"),
            concatenated_column("weight"),
        )

    @classmethod
    def from_hdf5_group(cls, group: h5py.Group) -> SolutionTable:
        """
        Read from an HDF5 group.
        """

        def col(col_name: str):
            return group[col_name][:]

        # NOTE: all string attributes are stored as bytes, otherwise DP3 and
        # WSClean can't read them correctly.
        soltype = group.attrs["TITLE"].decode()
        axes = group["val"].attrs["AXES"].decode().split(",")

        return cls(
            group.name,
            soltype,
            axes,
            col("ant"),
            col("dir"),
            col("freq"),
            col("time"),
            col("val"),
            col("weight"),
        )

    def __repr__(self) -> str:
        clsname = type(self).__name__
        return (
            f"{clsname}(name={self.name}, soltype={self.soltype}, "
            f"{len(self.time)}T, {len(self.freq)}F, {len(self.ant)}A, "
            f"{len(self.dir)}D)"
        )


@dataclass(repr=False)
class SolutionSet:
    """
    Maps to a solset, e.g. 'sol000'. One SolutionSet contains one or more
    SolutionTables.

    Example HDF5 layout of a solution set:

    .. code-block::
    sol000: <HDF5 group "/sol000" (3 members)>
        antenna: <HDF5 dataset "antenna": shape (62,), type "|V28">
        phase000: <HDF5 group "/sol000/phase000" (6 members)>
            ant: <HDF5 dataset "ant": shape (62,), type "|S5">
            dir: <HDF5 dataset "dir": shape (9,), type "|S128">
            freq: <HDF5 dataset "freq": shape (16,), type "<f8">
            time: <HDF5 dataset "time": shape (15,), type "<f8">
            val: <HDF5 dataset "val": shape (15, 16, 62, 9), type "<f8">
            weight: <HDF5 dataset "weight": shape (15, 16, 62, 9), type "<f4">
        source: <HDF5 dataset "source": shape (9,), type "|V136">
    """

    name: str
    antenna: NDArray
    source: NDArray
    solution_tables: list[SolutionTable]

    def write_to_hdf5_file(self, file: h5py.File) -> None:
        """
        Create a new group in given HDF5 file, and write the SolutionSet into
        said group. The newly created group will have the same `name`
        attribute.
        """
        group = file.create_group(self.name)

        def write_column(colname: str) -> None:
            group.create_dataset(colname, data=getattr(self, colname))

        write_column("antenna")
        write_column("source")

        for tab in self.solution_tables:
            tab.write_to_hdf5_group(group)

    @classmethod
    def concatenate(cls, solution_sets: Sequence[SolutionSet]) -> SolutionSet:
        """
        Concatenate solution tables along the time axis.
        """
        _assert_scalar_attr_identical(solution_sets, "name")
        _assert_array_attr_identical(solution_sets, "antenna")
        _assert_array_attr_identical(solution_sets, "source")

        # This should look like:
        # {
        #   '/sol000/phase000': <list of SolutionTable>,
        #   '/sol000/amplitude000': <list of SolutionTable>,
        # }
        solution_table_dict: dict[str, list[SolutionTable]] = defaultdict(list)
        for solset in solution_sets:
            for soltab in solset.solution_tables:
                solution_table_dict[soltab.name].append(soltab)

        if not all(
            len(soltab_list) == len(solution_sets)
            for soltab_list in solution_table_dict.values()
        ):
            raise ValueError(
                "Solution sets must contain the same soltab names"
            )

        concatenated_soltabs = [
            SolutionTable.concatenate(soltabs)
            for soltabs in solution_table_dict.values()
        ]

        first = solution_sets[0]
        return cls(
            first.name, first.antenna, first.source, concatenated_soltabs
        )

    @classmethod
    def from_hdf5_group(cls, group: h5py.Group) -> SolutionSet:
        """
        Read from an HDF5 group.
        """
        soltab_keys = set(group.keys()).difference({"antenna", "source"})
        solution_tables = [
            SolutionTable.from_hdf5_group(group[key]) for key in soltab_keys
        ]
        return cls(
            group.name,
            group["antenna"][:],
            group["source"][:],
            solution_tables,
        )

    def __repr__(self) -> str:
        clsname = type(self).__name__
        return (
            f"{clsname}(name={self.name}, {len(self.antenna)}A, "
            f"{len(self.source)}D, solution_tables={self.solution_tables})"
        )


class SolutionFile:
    """
    Maps to a h5parm file. One SolutionFile contains one or more SolutionSets.
    """

    def __init__(self, path: Path) -> None:
        """
        Make a new SolutionFile instance.
        """
        self._path = path

    @property
    def path(self) -> Path:
        """
        Path to the solution file.
        """
        return self._path

    def solution_sets(self) -> list[SolutionSet]:
        """
        Returns a list of all the SolutionSets in this file.
        """
        with h5py.File(self._path) as file:
            return [
                SolutionSet.from_hdf5_group(group) for group in file.values()
            ]

    def solution_table_names(self) -> list[str]:
        """
        Returns a list of the unique solution table names present in any
        solution set within this file, without the leading slash.

        The result should always be one of: ["phase000"], ["amplitude000"] or
        ["amplitude000", "phase000"].
        """
        # NOTE: solution table names are of the form "/sol000/phase000"
        return sorted(
            set(
                soltab.name.strip("/").split("/")[-1]
                for solset in self.solution_sets()
                for soltab in solset.solution_tables
            )
        )

    @classmethod
    def concatenate(
        cls, files: Sequence[SolutionFile], output_path: Path
    ) -> SolutionFile:
        """
        Concatenate solution files and write the output to given path.
        SolutionFile object representing the concatenated output.
        """
        # This should look like:
        # {
        #   '/sol000': <list of SolutionSet>,
        #   '/sol001': <list of SolutionSet>,
        # }
        solution_set_dict: dict[str, list[SolutionSet]] = defaultdict(list)
        for file in files:
            for solution_set in file.solution_sets():
                solution_set_dict[solution_set.name].append(solution_set)

        if not all(
            len(solset_list) == len(files)
            for solset_list in solution_set_dict.values()
        ):
            raise ValueError(
                "Solution files must contain the same solset names"
            )

        with h5py.File(output_path, mode="w") as outfile:
            for solution_sets in solution_set_dict.values():
                SolutionSet.concatenate(solution_sets).write_to_hdf5_file(
                    outfile
                )
        return SolutionFile(output_path)
