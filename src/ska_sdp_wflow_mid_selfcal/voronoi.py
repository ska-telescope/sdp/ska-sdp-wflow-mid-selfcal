import numpy as np
from numpy.typing import NDArray

# pylint: disable=no-name-in-module
from scipy.spatial import Voronoi


def get_edge_mirrored_points(points: NDArray, limits: NDArray) -> NDArray:
    """
    Generates mirror points with respect to each of the 4 edges of the box.
    """
    result = []
    (x_min, y_min), (x_max, y_max) = limits

    for (x, y) in points:
        result.append((2 * x_min - x, y))
        result.append((2 * x_max - x, y))
        result.append((x, 2 * y_min - y))
        result.append((x, 2 * y_max - y))

    return np.asarray(result)


def apply_voronoi(
    points: NDArray,
    *,
    x_min: float,
    y_min: float,
    x_max: float,
    y_max: float,
) -> list[NDArray]:
    """
    Make a voronoi tessellation bounded by a rectangular box around the
    given points.

    Args:
        points: input points in the cartesian plane, as an array with shape
            (num_points, 2)
        x_min : left edge of the box
        y_min : lower edge of the box
        x_max : right edge of the box
        y_max : upper edge of the box

    Returns:
        A list of arrays; every array contains the vertex coordinates of a
        voronoi cell, and has shape (num_points_in_cell, 2)

    Raises:
        ValueError: if any of the points are outside of limits
    """
    limits = np.asarray([(x_min, y_min), (x_max, y_max)])

    assert_all_points_within_limits(points, limits)

    polygons = []

    mirrored_points = get_edge_mirrored_points(points, limits)
    all_points = np.concatenate([points, mirrored_points], axis=0)

    vor = Voronoi(all_points)

    for idx in range(len(points)):
        vertex_indices = vor.regions[vor.point_region[idx]]
        vertices = sort_voronoi_vertices(vor.vertices[vertex_indices])
        polygons.append(vertices)

    return polygons


def assert_all_points_within_limits(points: NDArray, limits: NDArray) -> None:
    """
    Checks that the points are within limits, with strict inequalities

    Currently, these points cannot lie exactly on the image
    boundaries, because generating Voronoi tessellations
    demands mirroring these points about the limits.
    Hence, having a point exactly on the boundary
    risks potential errors in generating the Voronoi regions.
    """
    (x_min, y_min), (x_max, y_max) = limits

    off_limits_indices = [
        idx
        for idx, (x, y) in enumerate(points)
        if not (x_min < x < x_max and y_min < y < y_max)
    ]

    if off_limits_indices:
        raise ValueError(
            "the following point indices are out of bounds: "
            f"{off_limits_indices}"
        )


def sort_voronoi_vertices(vertices: NDArray) -> NDArray:
    """
    Sorts polygon vertices by position angle.
    """
    midpoint = vertices.mean(axis=0)
    x, y = (vertices - midpoint).T
    theta = np.arctan2(y, x)
    order = theta.argsort()
    return vertices[order]
