from __future__ import annotations

import abc
import atexit
import copy
import os
import shutil
import tempfile
from pathlib import Path
from typing import Iterable, Optional, Sequence, Union

ScalarArg = Union[str, bool, int, float, Path]

Arg = Union[ScalarArg, Sequence[ScalarArg]]


def _render_scalar_arg(arg: ScalarArg) -> str:
    if isinstance(arg, Path):
        return arg.as_posix()

    if isinstance(arg, bool):
        return str(arg).lower()

    return str(arg)


class Command(abc.ABC):
    """
    Base class for commands.
    """

    def __init__(
        self,
        executable: str,
        positional_args: Sequence[ScalarArg],
        options: dict[str, Arg],
    ) -> None:
        """
        Create a new Command. How commands are actually rendered as a list of
        arguments is implemented in sub-classes.

        Args:
            executable: name of the binary executable for this command.
            positional_args: sequence of positional arguments for the command
            options: dictionary of optional arguments, where the keys are the
                prefix for the option, and the values the associated values.
        """
        self.executable = executable
        self.positional_args = list(positional_args)
        self.options = options

    @abc.abstractmethod
    def render_option(self, key: str, arg: Arg) -> list[str]:
        """
        Render an optional argument as a list of strings. How this is done
        is different for WSClean and DP3.
        """

    def render(self) -> list[str]:
        """
        Render the command as a list of strings. The rendering order is
        `<EXECUTABLE> <OPTIONS> <POSITIONAL_ARGS>`.
        Options and positional args are alphabetically sorted.
        """
        args = [self.executable]

        options = self.options
        for key in sorted(options):
            val = options[key]
            args.extend(self.render_option(key, val))

        args.extend(_render_scalar_arg(val) for val in self.positional_args)
        return args

    def render_with_modifiers(
        self, modifiers: Sequence[PrefixModifier]
    ) -> list[str]:
        """
        Render the command as a list of strings, applying the given list of
        modifiers. The rendering order is:
        `<PREFIXES> [... <PREFIXES>] <EXECUTABLE> <OPTIONS> <POSITIONAL_ARGS>`.
        Options and positional args are alphabetically sorted.
        """
        if not modifiers:
            return self.render()

        # Render recursively, applying the modifier with the lowest `order`
        # first
        modifiers = sorted(modifiers, key=lambda m: m.order())
        mod = modifiers[0]
        prefixes, modified_command = mod.apply(self)
        return prefixes + modified_command.render_with_modifiers(modifiers[1:])

    def __eq__(self, other: Command) -> bool:
        return (
            self.executable == other.executable
            and self.positional_args == other.positional_args
            and self.options == other.options
        )


class WSCleanCommand(Command):
    """
    Container for all the arguments in a WSClean command.
    """

    def __init__(
        self,
        measurement_sets: list[Path],
        *,
        options: dict[str, Arg],
    ) -> None:
        """
        Create a new WSCleanCommand.

        Args:
            measurement_sets: list of paths to the input measurement sets to
                process
            options: dictionary of additional options, keys are the associated
                prefix to the argument and values are the value of an argument
                in a native Python type.
        """
        super().__init__("wsclean", measurement_sets, options)

    def render_option(self, key: str, arg: Arg) -> list[str]:
        """
        Appropriately render a WSClean option as a list of strings.
        """
        # Boolean options are dealt with first. Examples:
        # key="multiscale", arg=True  is rendered as "-multiscale"
        # key="multiscale", arg=False is ignored

        dashed_key = "-" + key if not key.startswith("-") else key
        if isinstance(arg, bool):
            return [dashed_key] if arg else []

        if isinstance(arg, (list, tuple)):
            rendered_arg = [_render_scalar_arg(item) for item in arg]
        else:
            rendered_arg = [_render_scalar_arg(arg)]
        return [dashed_key] + rendered_arg


class DP3Command(Command):
    """
    Container for all the arguments in a DP3 command.
    """

    def __init__(self, options: dict[str, Arg]) -> None:
        super().__init__("DP3", positional_args=[], options=options)

    def render_option(self, key: str, arg: Arg) -> list[str]:
        """
        Appropriately render a DP3 option as a list of strings.
        """
        if isinstance(arg, (list, tuple)):
            csv = ",".join(_render_scalar_arg(item) for item in arg)
            rendered_arg = f"[{csv}]"
        else:
            rendered_arg = _render_scalar_arg(arg)
        return [f"{key}={rendered_arg}"]


class PrefixModifier(abc.ABC):
    """
    A command modifier that:
    1. Edits the command's arguments and options
    2. Prefixes the rendered command with additional arguments
    """

    @abc.abstractmethod
    def order(self) -> int:
        """
        An arbitrary number that defines the relative order in which multiple
        modifiers should be applied. Lower `order` commands are applied first.
        """

    @abc.abstractmethod
    def apply(self, cmd: Command) -> tuple[list[str], Command]:
        """
        Apply the modifier to the given command. Returns a tuple
        (prefixes, modified_command).
        """


class SingularityExec(PrefixModifier):
    """
    A command modifier that allows to render a command in such a way that it
    can be run inside a singularity container. When applied to a command, it
    does the following:
    1. Finds all the arguments of a command that are Path instances, and
    makes them absolute.
    2. Identifies the set of directories containing these path arguments
    3. Generates the right bind mount argument for each of these directories
    """

    def __init__(self, image_file: Path) -> None:
        """
        Create a new SingularityExec instance.

        Args:
            image_file: path to the singularity image file to use to run
                whatever commands on which this instance will operate.
        """
        self.image_file = image_file

    def order(self) -> int:
        return 1

    def apply(self, cmd: Command) -> tuple[list[str], Command]:
        modified_cmd = copy.deepcopy(cmd)
        bind_mount_pairs = set()

        def _replace_scalar_arg(arg: ScalarArg) -> ScalarArg:
            if not isinstance(arg, Path):
                return arg
            arg_absolute = arg.resolve()
            bind_mount_pairs.add(
                (str(arg_absolute.parent), "/mnt" + str(arg_absolute.parent))
            )
            return Path("/mnt" + str(arg_absolute))

        def _replace_arg(arg: Arg) -> Arg:
            if not isinstance(arg, (list, tuple)):
                return _replace_scalar_arg(arg)
            return [_replace_scalar_arg(item) for item in arg]

        # Replace all Path arguments with absolute versions
        modified_cmd.positional_args = list(
            map(_replace_arg, modified_cmd.positional_args)
        )
        modified_cmd.options = {
            key: _replace_arg(arg) for key, arg in modified_cmd.options.items()
        }

        # Generate prefixes
        bind_mount_args = []
        for host_dir, target_dir in sorted(bind_mount_pairs):
            bind_mount_args.extend(("--bind", f"{host_dir}:{target_dir}"))

        prefixes = [
            "singularity",
            "exec",
            *bind_mount_args,
            str(self.image_file),
        ]
        return prefixes, modified_cmd


class Mpirun(PrefixModifier):
    """
    A command modifier that allows to render a WSClean command so that it runs
    on multiple nodes via mpirun, distributing one frequency band to process
    to each node. Does the following:

    1. Changes the executable from `wsclean` to `wsclean-mp`

    2. Creates a hostfile for the run

    3. Prefixes the command with
    `mpirun -hostfile <HOSTFILE> --bind-to none`
    """

    def __init__(
        self,
        hosts: Optional[Iterable[str]],
        *,
        hostfile_dir: Union[os.PathLike, str, None] = None,
    ) -> None:
        """
        Create a new Mpirun modifier. If `hosts` is None, an empty sequence,
        or contains a single host, then the modifier has no effect.

        Args:
            hosts: Sequence/iterable of host names or addresses to include in
                the hostfile. Can be empty or None.
            hostfile_dir: Path to a directory where to store the
                hostfile. If None, create a temporary directory for the
                occasion, which will be deleted on program exit.
        """
        self.hosts = list(hosts) if hosts is not None else []
        self.hostfile_path = None  # initialised below

        if hostfile_dir is None:
            hostfile_dir = Path(tempfile.mkdtemp())
            atexit.register(shutil.rmtree, hostfile_dir)
        else:
            hostfile_dir = Path(hostfile_dir).resolve()

        if not hostfile_dir.is_dir():
            raise ValueError(f"hostfile_dir does not exist: {hostfile_dir}")

        self.hostfile_path = hostfile_dir / "hostfile"

    def _create_hostfile(self) -> None:
        text = "".join(f"{host} slots=1\n" for host in self.hosts)
        self.hostfile_path.write_text(text)

    def order(self) -> int:
        return 0

    def apply(self, cmd: Command) -> tuple[list[str], Command]:
        # Only applies to WSClean commands
        if not isinstance(cmd, WSCleanCommand):
            return [], cmd

        # No need for MPI for a single host
        if not len(self.hosts) > 1:
            return [], cmd

        self._create_hostfile()

        modified_cmd = copy.deepcopy(cmd)
        modified_cmd.executable = "wsclean-mp"
        prefixes = [
            "mpirun",
            "--hostfile",
            str(self.hostfile_path),
            "--bind-to",
            "none",
        ]
        return prefixes, modified_cmd
