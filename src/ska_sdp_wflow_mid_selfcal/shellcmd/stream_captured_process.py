import time
from subprocess import PIPE, STDOUT, CalledProcessError, Popen
from threading import Thread
from typing import Any, Callable, Iterable


class StreamCapturedProcess:
    """
    A wrapper around subprocess.Popen, where the lines emitted by the standard
    streams of the managed process are consumed by user-defined functions.
    This is used to live-capture stdout/stderr to a logger instance.
    """

    def __init__(
        self, args: list[str], stream_consumer: Callable[[str], Any]
    ) -> None:
        """
        Start a subprocess with live stdout and stderr capture.

        Args:
            args: command-line arguments to pass to subprocess.Popen
            stream_consumer: A callable function that accepts a string argument
                representing a line of standard output/error from the
                subprocess.
        """
        self._stream_consumer = stream_consumer

        # NOTE: redirect stderr to stdout, so we capture only one stream
        # pylint:disable=consider-using-with
        self._process = Popen(
            args,
            stdout=PIPE,
            stderr=STDOUT,
            universal_newlines=True,
        )

        # Thread that captures stdout
        self._thread = Thread(
            target=self._consume_stream,
            args=(self._process.stdout, self._stream_consumer),
        )
        self._thread.daemon = True
        self._thread.start()

    def wait(self) -> None:
        """
        Blocks while waiting for the process to finish.

        Raises:
            subprocess.CalledProcessError: if the process exited with a
            non-zero code.
        """
        # NOTE: this code reproduces the actual implementation
        # of subprocess.check_call(). Ensures that the subprocess gets killed
        # when we receive a SIGINT/SIGTERM.
        try:
            returncode = self._process.wait()
        except:  # noqa: E722
            self._process.kill()
            self._process.wait(timeout=1.0)
            raise

        self._thread.join()
        if returncode:
            raise CalledProcessError(returncode, self._process.args)

    def wait_while_condition(self, condition: Callable[[], bool]) -> None:
        """
        Blocks while waiting for the process to finish as long as `condition`
        evaluates to True. If `condition` flips to False, kill the process
        immediately and wait for it to finish. On a dask cluster, this
        mechanism allows the head node to send workers a signal to kill
        the StreamCapturedProcess they are running.

        Raises:
            subprocess.CalledProcessError: if the process exited with a
            non-zero code.
        """
        tick_seconds = 0.5
        while condition() and self._process.poll() is None:
            time.sleep(tick_seconds)

        if self._process.poll() is None:
            self._process.kill()
        self.wait()

    def _consume_stream(
        self, stream: Iterable[str], consumer: Callable[[str], Any]
    ):
        try:
            for line in stream:
                consumer(line.rstrip())
        # Catch "I/O operation on closed file" if the pipeline gets terminated
        except ValueError:
            pass
