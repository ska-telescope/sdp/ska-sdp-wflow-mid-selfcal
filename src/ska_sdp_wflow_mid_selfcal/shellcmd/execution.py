import logging
import shlex
import time
from typing import Any, Callable, Optional, Sequence

from ska_sdp_wflow_mid_selfcal.logutils import LOGGER_NAME

from .command_utils import Command, PrefixModifier
from .stream_captured_process import StreamCapturedProcess


def check_call_with_stream_capture(
    command_args: list[str],
    stream_consumer: Callable[[str], Any],
    *,
    keep_running_fn: Optional[Callable[[], bool]] = None,
) -> float:
    """
    Executes a command line with stdout/stderr capture and returns the time
    taken to complete.

    Args:
        command_args: The list of command arguments.
        stream_consumer: A function to consume individual lines of output
            stream. Could be e.g. `logging.debug`.
        keep_running_fn: callable function that takes no arguments; keep the
            managed subprocess alive while it evaluates to True. If it flips to
            False, kill the subprocess and raise an Exception. This is used to
            interrupt subprocesses on remote dask workers.

    Returns:
        The time taken for command execution in seconds.

    Raises:
        subprocess.CalledProcessError: if the process exited with a
        non-zero code.
    """
    start_time = time.perf_counter()
    process = StreamCapturedProcess(command_args, stream_consumer)
    if keep_running_fn is not None:
        process.wait_while_condition(keep_running_fn)
    else:
        process.wait()
    return time.perf_counter() - start_time


def execute_with_logger_redirection(
    command: Command,
    modifiers: Sequence[PrefixModifier],
    *,
    extra: Optional[dict[str, Any]] = None,
    logger_name: str = LOGGER_NAME,
    keep_running_fn: Optional[Callable[[], bool]] = None,
) -> None:
    """
    Executes a command with pipeline logger redirection.

    Args:
        command: The command to execute.
        modifiers: List of prefix modifiers for the command.
        extra: Extra data to include in log messages.
        logger_name: Name of the logger to use.
        keep_running_fn: callable function that takes no arguments; keep the
            managed subprocess alive while it evaluates to True. If it flips to
            False, kill the subprocess and raise an Exception. This is used to
            interrupt subprocesses on remote dask workers.

    Raises:
        subprocess.CalledProcessError: if the process exited with a
        non-zero code.
    """
    if extra is None:
        extra = {}
    extra = extra | {"subprocess": command.executable}
    logger = logging.getLogger(logger_name)

    def debug(line: str) -> None:
        return logger.debug(line, extra=extra)

    def info(line: str) -> None:
        return logger.info(line, extra=extra)

    command_args = command.render_with_modifiers(modifiers)

    info(f"Running {command.executable}")
    info(shlex.join(command_args))

    time_seconds = check_call_with_stream_capture(
        command.render_with_modifiers(modifiers),
        debug,
        keep_running_fn=keep_running_fn,
    )

    info(f"{command.executable} finished in {time_seconds:.2f} seconds")
