"""
Provides code to find in which polygonal patch of the sky a given source lies.
We use the ray casting algorithm, see e.g.
https://en.wikipedia.org/wiki/Point_in_polygon
"""
import itertools
from typing import Iterator

from numpy.typing import NDArray


# pylint: disable=invalid-name
def point_in_polygon(x: float, y: float, polygon: NDArray) -> bool:
    """
    Returns True if the point (x, y) lies inside `polygon`.
    `polygon` must be provided as a numpy array of shape (num_vertices, 2),
    and the vertices must be enumerated along its circumference, in any
    direction.
    """
    point = (x, y)
    num_crossings = 0
    for end_point_a, end_point_b in _iterate_edges(polygon):
        res = _ray_cast_check(point, end_point_a, end_point_b)
        if res == ON_EDGE:
            return True
        if res == ISEC:
            num_crossings += 1
    return num_crossings % 2 == 1


# Possible results of the ray casting algorithm
NO_ISEC = 0
ISEC = 1
ON_EDGE = 2


def _iterate_edges(polygon: NDArray) -> Iterator[NDArray]:
    n, __ = polygon.shape
    start_points = itertools.islice(itertools.cycle(polygon), 0, n)
    end_points = itertools.islice(itertools.cycle(polygon), 1, n + 1)
    return zip(start_points, end_points)


# pylint: disable=too-many-return-statements
def _ray_cast_check(
    point_p: tuple[float, float],
    end_point_a: tuple[float, float],
    end_point_b: tuple[float, float],
) -> int:
    """
    Consider a point P and a segment [AB]. This function checks whether the
    half-line starting from P and extending along the x axis towards larger
    values intersects [AB]. A result `ON_EDGE` means that P belongs to [AB].
    """
    x, y = point_p
    xa, ya = end_point_a
    xb, yb = end_point_b

    if not min(ya, yb) <= y <= max(ya, yb):
        return NO_ISEC

    if ya == yb:
        if min(xa, xb) <= x <= max(xa, xb):
            return ON_EDGE
        return NO_ISEC

    if xa == xb:
        if x == xa:
            return ON_EDGE
        if x < xa:
            return ISEC
        return NO_ISEC

    # The infinite line (AB) intersects the horizontal line at height y at some
    # point we call I. If x_I > x, then our "ray" crosses the segment.
    # If x_I = x then our point is on the segment [AB]
    # Equation of (AB) line is y = m * x + p
    m = (yb - ya) / (xb - xa)
    p = ya - m * xa
    x_i = (y - p) / m

    if x == x_i:
        return ON_EDGE
    if x < x_i:
        return ISEC
    return NO_ISEC
