import datetime
import os
from pathlib import Path
from typing import Union


def create_output_directory(path_arg: Union[str, os.PathLike, None]) -> Path:
    """
    Create an output directory based on the provided path argument. Does
    nothing if the given path is a directory that already exists.

    Args:
        path_arg: string, Path-like object, or None representing the path where
            the output directory should be created. If None is provided,
            a uniquely-named directory will be created in the
            current working directory.

    Returns:
        Path: A Path object representing the newly created output directory.

    Raises:
        ValueError: If the provided path argument exists and is a file.

    Examples:
        >>> create_output_directory(None)
        PosixPath('/current/working/directory/selfcal_20220309_143201_123456')

        >>> create_output_directory("/path/to/non_existing_directory")
        PosixPath('/path/to/non_existing_directory')

        >>> create_output_directory("/path/to/existing_directory")
        PosixPath('/path/to/existing_directory')
    """
    if path_arg is None:
        name = datetime.datetime.now().strftime("selfcal_%Y%m%d_%H%M%S_%f")
        outdir = Path.cwd() / name
        outdir.mkdir()
        return outdir

    path = Path(path_arg)

    if not path.exists():
        path.mkdir(parents=True)
        return path

    if path.is_file():
        raise ValueError(f"{path_arg!r} exists and is a file")

    # We already know that path exists and that it is a directory
    return path
