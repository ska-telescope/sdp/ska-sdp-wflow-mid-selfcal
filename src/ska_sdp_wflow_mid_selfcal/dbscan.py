from collections import deque

import numpy as np
from numpy.typing import ArrayLike, NDArray
from scipy.spatial import KDTree


def simple_dbscan(points: ArrayLike, radius: float) -> list[NDArray]:
    """
    A simplified implementation of the DBSCAN algorithm that should work in
    any number of dimensions:
    https://en.wikipedia.org/wiki/DBSCAN

    The simplifications are: there is no concept of noise point (they are
    just considered to be clusters of size 1), and the "min points" parameter
    is thus implicitly assumed to be 1.

    Args:
        points: The input data point coordinates, either a numpy array or
            something convertible to it (e.g. list of tuples), with shape
            (num_points, num_dimensions). If providing a 1D array-like input,
            it is interpreted as a list of scalar values to be clustered.
        radius: The euclidean distance within which points are considered part
            of the same cluster.

    Returns:
        A list of numpy arrays, one per cluster, where each array contains
        the integer indices of the cluster members in the input data.

    Raises:
        ValueError: If `points` does not have the expected shape
    """
    points = np.asarray(points)

    # If given a 1D array of points, assume we're clustering scalar values
    if points.ndim == 1:
        points = points.reshape(-1, 1)

    kdtree = KDTree(points)
    to_visit = set(range(len(points)))
    result: list[set[int]] = []

    while to_visit:
        # Start new cluster
        seed_index = to_visit.pop()
        cluster: set[int] = {seed_index}

        # Then, expand cluster until we can't
        expand_indices = deque([seed_index])

        while expand_indices:
            i = expand_indices.popleft()
            current_pt = points[i]

            # Point indices that are:
            # * Within radius of currently considered point
            # * Neither visited nor queued for expansion yet
            new_expand_indices = set(
                kdtree.query_ball_point(current_pt, radius)
            ).intersection(to_visit)

            cluster.update(new_expand_indices)
            to_visit.difference_update(new_expand_indices)
            expand_indices.extend(new_expand_indices)

        cluster = np.fromiter(cluster, dtype=int, count=len(cluster))
        result.append(cluster)
    return result
