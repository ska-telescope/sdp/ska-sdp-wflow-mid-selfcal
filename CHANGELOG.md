# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## 0.9.0 - 2024-05-20

### Added

* The pipeline can now be launched without a bootstrap sky model. In this case, an initial imaging stage is performed first. The resulting clean image is automatically thresholded, and the clean components located inside the bright emission islands thus identified are used as a bootstrap sky model.
* Wideband deconvolution options for WSClean are now more intelligently calculated as a function of input data parameters and number of nodes available.

### Removed

* Sky model filtering and clustering step, as it was found not to behave optimally on non-point sources. It will be reinstated when it can be improved.

### Changed

* Configuration file format has been changed, and must now contain an `initial_imaging` section. See `config/` directory for documented examples.


## 0.8.0 - 2024-04-03

Integrate WSClean MPI, new config file format with schema validation, add official SLURM batch script.

### Added

* Configuration file schema validation on pipeline startup. Invalid option names or values for DP3 / WSClean are now detected immediately, instead of causing a DP3 / WSClean crash later.
* Stand-alone app `mid-selfcal-validate-config` to perform said validation manually.
* Pipeline will now use WSClean MPI if multiple nodes are available. A list of host names or addresses can be passed via `--mpi-hosts` for WSClean to use.
* Example SLURM batch script for CSD3 in the `scripts/` directory, which demonstrates how to launch a dask cluster, run the system monitor and the pipeline -- all of it on multiple nodes.
* Additional config file examples in `config/`. These are validated as part of the CI pipeline and guaranteed to be valid.

### Changed

* Configuration file format has been changed in a way that is NOT backwards compatible. See `config/` directory for documented examples.

### Removed

* Deleted legacy direction-independent (DI) self-calibration pipeline.
* Deleted `mid-selfcal-dask-cluster-slurm` app. The same functionality can more easily be implemented inside a batch script instead (see above).



## 0.7.1 - 2024-03-20

Additional logging tags for supporting benchmarks, code fixes and improvements.

### Added

* Log records are now tagged with additional attributes: subprocess, worker, stage, selfcal cycle, task index. These are stored in the logfile in JSON lines format and will help performance analysis.
* Pipeline app now exits with proper error code.
* `__version__` attribute is now a PEP440 version string with a git commit hash.

### Changed

* User can now specify the output directory for the pipeline run via the  `--outdir` command-line option. Previously, the user could only specify a base output directory via `--base-outdir` (option has been removed), inside which the pipeline would create a uniquely-named subdir for the run.
* New Dockerfile and Singularity recipe with a new version of WSClean and dependencies,
where `wsclean-mp` now works in facet mode.

### Fixed

* Managed subprocesses now get terminated reliably when the pipeline receives SIGINT / SIGTERM. `StreamCapturedProcess` code improved and simplified along the way.
* When running DDECal in distributed mode, DDECal tasks that fail will now make the whole pipeline fail immediately, as was originally intended.
* On pipeline failure, DDECal tasks submitted to dask cluster but not yet running are now unscheduled. Previously, they ended up getting scheduled and killed immediately.
* Calibration solution chunks now get deleted as expected on cleanup.

### Removed

* `--base-outdir` command-line option (see above).


## 0.7.0 - 2024-02-19

End of PI21 release. Added wideband deconvolution, k-means tesselation, new features to support benchmarking.

### Added

* Added support for wideband deconvolution. The number of output channels used by WSClean is automatically set by the pipeline based on the configuration file parameter `max_fractional_bandwidth`.
* New sky tesselation method `kmeans`, which should be now used as the default. When using this method, the sources are weighted by their stokes I fluxes and then clustered using the K-means algorithm; the cluster centroids are taken to be the calibration patch / facet centres. This yields clusters with better flux balance, and unlike `voronoi_brightest` the method is robust to real-world sky models.
* Integration test for the direction-dependent pipeline. Running `pytest` will execute the integration test, but only if both DP3 and WSClean are installed and available for the DD pipeline to execute them. Running `pytest --sif=<SINGULARITY_IMAGE>` will instead make the DD pipeline run DP3 and WSClean inside the given singularity image.
* An up-to-date model config file is now maintained in the `config/` directory.
* The DD pipeline now also saves logs in JSON lines format as `logfile.jsonl`.
* New app `mid-selfcal-system-monitor`, which regularly prints to standard output a JSON-format dictionary with various system usage metrics. This is useful to run in the background for benchmarking purposes, and can later be correlated with the pipeline logs in JSON lines format.
* The input YAML configuration file is now copied to the pipeline output directory under the name `config.yml` for later reference. The pipeline also logs the contents of the configuration on startup.

### Changed

* It is now mandatory to specify a config file argument to the `mid-selfcal-dd` pipeline. The pipeline will not fall back on a default configuration anymore.

### Fixed

* Updated Dockerfile, Singularity recipe and associated build instructions so that the `mpibig.cpp` patch is not applied anymore, which makes the build process cleaner. The relevant changes have now been integrated in a recent version of WSClean.


## 0.6.1 - 2024-01-18

Added basic sky model filtering, FITS image statistics, and some new diagnostic plots. Follows the first set of experiments with MeerKAT data.

### Added

* Calculation of FITS image statistics after every selfcal cycle.
* Command-line app `mid-selfcal-image-stats` that prints basic FITS image statistics.
* `SkyModel` now has `plot()` method.
* Improved exporting of `SkyModel` to a DS9 region file. When loading said file in DS9, Gaussian sources are now shown as circles with the correct angular dimensions on sky.
* At the end of every selfcal cycle, the sky model returned by WSClean is now thresholded in absolute flux before being clustered. The absolute Stokes I flux threshold value is equal to `N x robust_rms_residual`, where `N` is a user-defined parameter called `flux_threshold_sigma` in the DD pipeline config file (defaults to 20 if not specified), and `robust_rms_residual` refers to that of most recent imaging run. This is to avoid including spurious sources in the next calibration cycle.
* `SkyModel` plots are saved in each selfcal cycle, before thresholding, after thresholding, and after thresholding + clustering.


## 0.6.0 - 2023-12-19

Added clustering of point sources at the end of each self-calibration cycle. Multiple self-calibration cycles can now be performed even with the `voronoi_brightest` tesselation method.

### Added

* At the end of every self-calibration cycle, the sky model returned by WSClean is now replaced by another sky model where every cluster of point sources has been replaced by a single equivalent point source. Clusters are determined using the DBSCAN algorithm with a user-configurable radius (see below). This has two beneficial effects. Firstly, the tesselation code now behaves properly when using the `voronoi_brightest` method (previously, bright point sources were returned as many different CLEAN components by WSClean, which would end up being split across different calibration patches / facets). Secondly, the significantly reduces the number of sources to be predicted in the next calibration stage, which should yield a measurable speedup.
* New optional keyword in the configuration file: `clustering_radius_pixels`. It currently defaults to 5.0 if not specified.
* In every self-calibration cycle, the pipeline now writes the input sky model at the start, and the output clustered skymodel at the end, both in sourcedb format.

### Changed

* New naming conventions for the files written by the pipeline. They are now prefixed with either `NN_` (i.e. the index of the corresponding selfcal cycle, starting at `01`) or `final_` (for the last cycle).


## 0.5.4 - 2023-12-14

Code refactoring and re-organisation release in preparation for upcoming feature additions. No changes from a pipeline user perspective.

### Added

* Parsing and writing of sourcedb skymodel files has been rewritten and separated cleanly from `SkyModel`. It is now handled by the `SourceDB` class inside the `sourcedb` sub-module.
* Added `CalibrationSkyModel` class that is responsible for assigning sources to calibration patches and storing the result. `CalibrationSkyModel` is exportable to a sourcedb file ready to be passed to DDECal.
* Added custom Makefile rule `make python-dev-checks` that runs all checks necessary to pass the stages `lint` and `test` of the CI pipeline.
* Added `pytest.ini` file, increased verbosity of pytest output by default.

### Fixed

* Fixed a bug where both the DI and DD pipeline would report that the input measurement set(s) cannot be found; this would happen when passing the input measurement set(s) as symbolic links and running DP3/WSClean within a singularity container. Symlinks are now resolved before processing.
* Fixed the order in which the DDECal/WSClean argument dictionaries are built. Previously, it would be possible for the user to override some arguments that should be under control of the pipeline, e.g. anything pertaining to input or output paths. Now, the update order is: start from a set of default options, update them with user preferences, and lastly update the DDECal/WSClean options that must be under complete control of the pipeline.

### Changed

* Refactored `SkyModel` and `Tesselation` classes. `SkyModel` has the interface of a sequence, and `Tesselation` has the interface of a set (without the binary set operations).
* Created `tests.common` sub-module where support code common to multiple tests shall be placed.
* Factorised some code used for testing tesselation code in both sky and cartesian coordinates, moved it to `tests.common`.
* Moved all code related to the DI pipeline to `legacy` sub-module. DI pipeline app is still accessible as `mid-selfcal-di` and remains fully functional.
* Moved code related to command line generation and execution to `shellcmd` sub-module.
* The default major loop gain in the DD pipeline (`-mgain` argument of WSClean) is now 0.75 (up from 0.5).


## 0.5.3 - 2023-11-22

Mostly a dummy release to fix previous problems encountered with the GitLab automated release pipeline.

### Fixed

* Fixed detail in Singularity recipe: `DP3_VERSION` env variable now matches the actual DP3 version.

## 0.5.2 - 2023-11-20

Updated documentation, refactored voronoi code to a higher standard. No changes from a pipeline user perspective.

### Added

* Singularity recipe from which one can directly build the DP3 / WSClean singularity image, without requiring an intermediate docker image.

### Fixed

* Documentation has been extensively updated and now properly reflects how to use the pipeline following the recent feature additions (distributed calibration using dask, and voronoi tesselation of the imaging field).


## 0.5.1 - 2023-10-11

Fix voronoi tesselation bug, tesselation method can now be specified in the DD pipeline config file.

### Added

* Added ability to specify the tesselation method to use in the DD pipeline config file. This can be either `voronoi_brightest` or `square_grid`.

### Fixed

* Fixed a bug where the voronoi tesselation code would attempt to create voronoi bins around bright sources outside of the imaging field. The code now only selects the brightest `num_patches` sources that are inside the field.

### Changed

* Configuration file now expects a `method` keyword in the `tesselation` section.


## 0.5.0 - 2023-10-11

Calibration patches are now defined as voronoi bins around the brightest sources of the field.

### Added

* DD pipeline now creates calibration patches around a user-specified number of brightest sources. That number is specified via `num_patches` in the configuration file.

### Removed

* Temporarily removed the old method of creating a N x N grid of calibration patches. The option will be added back soon, after some required refactoring.


## 0.4.1 - 2023-10-06

Use orthographic (sine) projection to create calibration patches and assign sources to them.

### Fixed

- Use the orthographic projection when defining calibration patches in the tangent plane, and when determining which patch contains which sources. This ensures consistency with how WSClean operates internally and the mathematical conventions of radio-interferometry in general (`l` and `m` coordinates are, in fact, orthographic tangent plane coords). Previously, the azimuthal equidistant projection was used to do both tasks, which may have caused issues in some edge cases. For example the facets containing the corners of the image were assigned slightly incorrect vertex coordinates on sky. Also, some sources near the boundary between two facets may have been assigned to the "wrong" one from the point of view of WSClean.

## 0.4.0 - 2023-09-27

Direction-dependent calibration can now be distributed over multiple nodes using dask. The distribution axis is time.

### Added

- Added an application `mid-selfcal-dask-cluster-slurm` which automatically launches and manages a dask cluster within a SLURM job allocation. It retrieves the list of allocated nodes, launches a dask scheduler on the first one, and a worker on all of them via `ssh`.
- The DD pipeline command-line app now accepts an extra argument `--dask-scheduler`, with the address of a dask distributed scheduler, e.g. `localhost:8786`. If specified, every pipeline stage eligible for distribution will be broken down into tasks which are then submitted to this scheduler.
- Added code to read, concatenate and write `H5Parm` calibration solution files, which is the format written by DP3 and read by WSClean.
- If `--dask-scheduler` is specified, DDECal gets distributed across multiple dask workers, along the time axis. One task is created for each solution interval, which generates as many solution files, which are then concatenated into a single on at the end.

### Changed

- Internally refactored the code that manages subprocesses and captures their stdout/err streams in real time. 

## 0.3.1 - 2023-08-22

Adds several improvements to the DD-selfcal pipeline prototype.

### Added

- The imaging centre is now set to the phase centre of the data, which is read from the input MeasurementSet.
- Can now run multiple self-calibration cycles in the DD pipeline (i.e. the loop is now closed).
- The DD pipeline now shuts down gracefully if interrupted or terminated via SIGINT / SIGTERM.
- The DD pipeline now raises an error if any of the calibration patches contain no sources.

## 0.3.0 - 2023-08-14

Added early DD-selfcal pipeline prototype. This is a new major version that breaks backwards compatibility.
The prototype in question currently does a single cycle (i.e. the loop is not closed yet), and it can only process the AA2 Mid simulated datasets; the latter limitation will be removed soon.

### Added

- New DD-selfcal pipeline prototype, that can be called via `mid-selfcal-dd`.

### Changed

- Old DI-selfcal pipeline is now called via `mid-selfcal-di`.


## 0.2.7 - 2023-07-31

This version exposes more WSClean parameters and fixes issues with the FITS to PNG app.

### Added

- Can now tweak the number of threads that DP3 uses via `--dp3-numthreads`.
- Can now adjust additional WSClean parameters using the command-line options `--auto-threshold`, `--mgain` and `--parallel-deconvolution`.

### Changed

- Unless `--parallel-deconvolution <SUBIMG_SIZE>` is specified on the command-line, WSClean won't use parallel deconvolution.

### Fixed

- FITS to PNG app now shrinks data by averaging, instead of max-pooling. On large images, this was excessively enhancing very weak features of the AA2 PSF, to the point where one could mistakenly think that something was wrong with WSClean. The option to shrink the data by max-pooling is still available.
- Colormap bounds in FITS to PNG app are now adjustable.


## 0.2.6 - 2023-06-15

This version adds the ability to run DP3 and WSClean on the host directly. Documentation has been updated to reflect all the development done in PI18.

### Added

- Can now run DP3 and WSClean on the host directly rather than inside a singularity container. To do this, simply omit the command-line argument `--singularity-image`.

## 0.2.5 - 2023-05-18

This version fixes a major issue with DP3 where gaincal would indefinitely freeze on startup on some of the AA2 datasets. The problem would trigger when DP3 was running with 64 or more CPUs available; the fix is to specify 63 threads or less via the `numthreads` DP3 parameter.

### Fixed

- Fixed the DP3 gaincal freeze problem by always setting the number of DP3 gaincal threads to 16.
- In `selfcal_pipeline()`, changed the default value of `clean_iters` to match that of the pipeline app.

### Added

- FITS to PNG command-line plotting app `mid-selfcal-fits2png`, which is quite useful to check selfcal progress without having to download 2GB+ FITS files.
- Added `numpy`, `matplotlib` and `astropy` as dependencies as a result of the above.
- Pipeline now automatically removes intermediate FITS files created by `wsclean-mp` just after it finishes running, and also on pipeline exit (sucessful or not).

### Changed

- Dockerfile: now building casacore v3.5 from source instead of using the casacore Ubuntu packages which are older (v3.2). Explicitly pin casacore and DP3 versions.


## 0.2.4 - 2023-05-11

This version contains minor improvements to make benchmarking easier.

### Added

- Pipeline app now logs the exact arguments with which it was called.
- If running in a SLURM environment, the allocated compute resources are now logged before starting self-calibration: number of nodes, number of CPUs, amount of memory. The latter does not work reliably on CSD3 due to its SLURM scheduler not always setting the env variable `$SLURM_MEM_PER_NODE`.
- The total bytesize of the input data is now logged before starting self-calibration.
- It is now possible to provide an empty list to `--clean-iters`, which results in directly imaging the data without any self-calibration cycles.
- It is now possible to provide an empty list to `--phase-only-cycles` as well.

### Changed

- Altered the definition of the `--clean-iters` argument. It still represents the number of deconvolution iterations to perform in each self-cal cycle, but now excludes the final imaging step, whose number of iterations is now hardcoded to 1 million (effectively, that means cleaning down to the noise). The number of actual self-cal cycles is now equal to the length of the `--clean-iters` list.


## 0.2.3 - 2023-05-10

### Added

- Can now perform an initial calibration step before starting the self-calibration loop. This can be done by providing a file in sourcedb format via the optional argument `--initial-sky-model`.
- Two DP3 gaincal parameters can now be tweaked from the command line via the optional arguments `--gaincal-solint` and `--gaincal-nchan`. The values provided are common to all calibration stages, including the optional initial calibration. If not provided, the DP3 default values are used.

### Changed

- `gaincal.tolerance` is now `1e-3` in all calibration stages, instead of the default `1e-5`.


## 0.2.2 - 2023-05-05

### Added

- The pipeline will now run `wsclean-mp` distributed on multiple nodes if executed in a SLURM environment and if multiple nodes have been allocated. Otherwise, it runs the regular `wsclean`.
- Dockerfile with MPI-enabled WSClean, where WSClean has been patched so that it chunks its large MPI messages into 1 GB pieces (instead of 2 GB, the official maximum MPI message size). This is to work around an issue with the network interfaces on CSD3 nodes that refuse to send 1 GB+ MPI messages.


## 0.2.1 - 2023-05-05

### Added

- Can now process input data split into multiple measurement sets, where each holds one distinct sub-band. Before processing actually starts, the input data are copied and merged into a single, temporary measurement set, on which self-calibration is then performed in-place.

### Changed

- In the pipeline app, input measurement set(s) must now be provided via the required command-line option `--input-ms`, instead of a positional argument.


## 0.2.0 - 2023-04-30

Intermediate release in PI18. While the self-calibration logic remains identical to `v0.1.0`, the code has been fully re-architected to be easily run on CSD3, unit testable, and extended in future iterations.

### Added

- Unit tests.
- Logging.
- Made the code into a proper Python package. Once installed, the pipeline app is accessible in a terminal from anywhere via the command `mid-selfcal-pipeline`.
- Real-time capture of stdout/stderr lines emitted by wsclean and DP3, which are redirected into the pipeline's Python logger.
- When launched, the pipeline creates a unique output sub-directory for the current run, inside which all temporary and output files are written, including the logs as `logfile.txt`. 
- Graceful termination when receiving `SIGINT` or `SIGTERM`.
- Guaranteed removal of intermediate output and temporary files on exit, successful or not.
- Exposed more wsclean parameters via the pipeline's command line: `--size` and `--scale`, which control the output image dimensions and angular pixel scale, respectively.

### Changed

- Both wsclean and DP3 are now run inside singularity containers, which makes the pipeline portable between computing facilities; compiling DP3 and wsclean on bare metal can be a headache (especially wsclean with MPI support). Consequently, the pipeline app now requires to be passed a singularity image file that provides both DP3 and wsclean via `--singularity-image`.
- wsclean is now instructed to write its temporary files in the designed output directory rather than in the same directory as the input measurement set.

### Removed

- Previous pipeline app `selfcal_di.py`.


## 0.1.0 - 2023-03-24

Initial release created in PI17.
