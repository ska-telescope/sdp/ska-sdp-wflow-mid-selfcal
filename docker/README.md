## Notes for developers on Dockerfile and Singularity recipe

We provide both a Dockerfile and a Singularity recipe to build an image containing the binaries that the pipeline needs.

When making changes to those, the Dockerfile should be treated as the "reference", and the Singularity recipe should then be manually obtained using [the `spython` python module](https://github.com/singularityhub/singularity-cli). **Please note however that the conversion is not always perfect and MUST be checked carefully**. Here is an example command:

```
spython recipe Dockerfile > Singularity
```
