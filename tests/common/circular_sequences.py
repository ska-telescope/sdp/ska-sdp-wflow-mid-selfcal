"""
This file contains utility functions that are currently used to check whether
two polygons are close, both in the cartesian plane and on the sky.
"""
import itertools
from typing import Callable, Iterator, Sequence, TypeVar

T = TypeVar("T")


def clip(n: int, n_min: int, n_max: int) -> int:
    """
    Clip number to [n_min, n_max].
    """
    return max(min(n, n_max), n_min)


def circular_traverse_forward(seq: Sequence[T], start: int) -> Iterator[T]:
    """
    Traverse all the elements of the sequence just once, starting at index
    `start`, moving forward and wrapping around.
    """
    n = len(seq)
    start = clip(start, 0, n)
    index_iter = itertools.chain(range(start, n), range(0, start))
    for i in index_iter:
        yield seq[i]


def circular_traverse_backward(seq: Sequence[T], start: int) -> Iterator[T]:
    """
    Traverse all the elements of the sequence just once, starting at index
    `start`, moving backward and wrapping around.
    """
    n = len(seq)
    start = clip(start, 0, n)
    index_iter = itertools.chain(range(start, -1, -1), range(n - 1, start, -1))
    for i in index_iter:
        yield seq[i]


def circular_sequences_close(
    seq_a: Sequence[T],
    seq_b: Sequence[T],
    distance_func: Callable[[T, T], float],
    tol: float,
) -> bool:
    """
    Check whether two circular sequences are close, element-wise, without
    regard for the traversal direction (forward or backward).

    For example, the sequences A = [7, 8, 9, 10] and B = [8, 7, 10, 9] are
    circularly close. We can find an element in B that close to the first
    element in A, then from there we can traverse B circularly in some
    direction so that we enumerate the elements of A exactly.
    """
    if not len(seq_a) == len(seq_b):
        return False

    # Find the index of the element in `seq_b` that is closest to the first
    # element in `seq_a`
    index_b, __ = min(
        enumerate(seq_b), key=lambda tupl: distance_func(seq_a[0], tupl[1])
    )
    forward_close = all(
        distance_func(a, b) <= tol
        for a, b in zip(seq_a, circular_traverse_forward(seq_b, index_b))
    )
    backward_close = all(
        distance_func(a, b) <= tol
        for a, b in zip(seq_a, circular_traverse_backward(seq_b, index_b))
    )
    return forward_close or backward_close
