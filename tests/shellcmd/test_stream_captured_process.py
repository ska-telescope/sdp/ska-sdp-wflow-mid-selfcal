import time
from subprocess import CalledProcessError

import pytest

from ska_sdp_wflow_mid_selfcal.shellcmd import StreamCapturedProcess


def test_stream_capture():
    """
    Check that StreamCapturedProcess does indeed capture the output of stdout
    and stderr.
    """
    code_lines = [
        "import sys",
        "for i in range(2):",
        "    print(f'out {i}', flush=True)",
        "    print(f'err {i}', flush=True, file=sys.stderr)",
    ]

    cmdline = ["python", "-c", "\n".join(code_lines)]

    stdout_lines = []

    def stdout_consumer(line: str):
        stdout_lines.append(line)

    StreamCapturedProcess(cmdline, stdout_consumer).wait()

    assert stdout_lines == ["out 0", "err 0", "out 1", "err 1"]


def test_stream_captured_process_wait_while_condition():
    """
    Self-explanatory.
    """
    time_seconds = 5.0
    cmdline = ["sleep", f"{time_seconds}"]

    def null_consumer(__: str):
        pass

    # The idea here is that wait_while_condition() will trigger the termination
    # of the process immediately, way before the sleep command can complete
    # successfully. In this case, a CalledProcessError will be raised.
    with pytest.raises(CalledProcessError):
        start_time = time.perf_counter()
        StreamCapturedProcess(cmdline, null_consumer).wait_while_condition(
            lambda: False
        )
        end_time = time.perf_counter()
        assert end_time - start_time < time_seconds


def test_stream_captured_process_raises_on_nonzero_exit_code():
    """
    Check that CalledProcessError is raised if StreamCapturedProcess exits with
    non-zero code.
    """
    cmdline = ["ls", "/definitely/non/existent/path/"]

    def null_consumer(__: str):
        pass

    with pytest.raises(CalledProcessError):
        StreamCapturedProcess(cmdline, null_consumer).wait()
