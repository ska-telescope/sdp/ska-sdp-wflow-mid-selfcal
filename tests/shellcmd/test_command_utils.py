# pylint: disable=redefined-outer-name
from pathlib import Path

import pytest

from ska_sdp_wflow_mid_selfcal.shellcmd import (
    DP3Command,
    Mpirun,
    SingularityExec,
    WSCleanCommand,
)


@pytest.fixture
def wsclean_command() -> WSCleanCommand:
    """
    WSCleanCommand used for testing rendering.
    """
    return WSCleanCommand(
        measurement_sets=[
            Path("/path/to/data1.ms"),
            Path("/path/to/data2.ms"),
        ],
        options={
            # Boolean options set to False are NOT rendered
            "apply-facet-beam": False,
            "multiscale": True,
            "name": "final",
            "niter": 42,
            "scale": "1asec",
            "size": (8192, 4096),
            "weight": ("briggs", -0.5),
        },
    )


def test_wsclean_command_rendering(wsclean_command: WSCleanCommand):
    """
    Test that WSCleanCommand renders as expected.
    """
    expected = [
        "wsclean",
        "-multiscale",
        "-name",
        "final",
        "-niter",
        "42",
        "-scale",
        "1asec",
        "-size",
        "8192",
        "4096",
        "-weight",
        "briggs",
        "-0.5",
        "/path/to/data1.ms",
        "/path/to/data2.ms",
    ]
    assert wsclean_command.render() == expected


def test_wsclean_command_rendering_with_singularity_exec(
    wsclean_command: WSCleanCommand,
):
    """
    Test that WSCleanCommand with SingularityExec modifier renders as expected.
    """
    mod = SingularityExec(Path("singularity_image.sif"))
    expected = [
        "singularity",
        "exec",
        "--bind",
        "/path/to:/mnt/path/to",
        "singularity_image.sif",
        "wsclean",
        "-multiscale",
        "-name",
        "final",
        "-niter",
        "42",
        "-scale",
        "1asec",
        "-size",
        "8192",
        "4096",
        "-weight",
        "briggs",
        "-0.5",
        "/mnt/path/to/data1.ms",
        "/mnt/path/to/data2.ms",
    ]
    assert wsclean_command.render_with_modifiers([mod]) == expected


def test_wsclean_command_rendering_with_mpirun(
    wsclean_command: WSCleanCommand,
):
    """
    Test that WSCleanCommand with Mpirun modifier renders as expected.
    """
    host_list = ["host_a", "host_b"]
    expected_hostfile_contents = "host_a slots=1\n" "host_b slots=1\n"
    mpirun = Mpirun(host_list)

    expected_args = [
        "mpirun",
        "--hostfile",
        str(mpirun.hostfile_path),
        "--bind-to",
        "none",
        "wsclean-mp",
        "-multiscale",
        "-name",
        "final",
        "-niter",
        "42",
        "-scale",
        "1asec",
        "-size",
        "8192",
        "4096",
        "-weight",
        "briggs",
        "-0.5",
        "/path/to/data1.ms",
        "/path/to/data2.ms",
    ]
    assert wsclean_command.render_with_modifiers([mpirun]) == expected_args
    assert mpirun.hostfile_path.read_text() == expected_hostfile_contents


def test_mpirun_has_no_effect_with_zero_or_one_hosts(
    wsclean_command: WSCleanCommand,
):
    """
    Test that no mpirun arguments are prepended to the command when just one
    or zero hosts are given.
    """
    expected_args = [
        "wsclean",
        "-multiscale",
        "-name",
        "final",
        "-niter",
        "42",
        "-scale",
        "1asec",
        "-size",
        "8192",
        "4096",
        "-weight",
        "briggs",
        "-0.5",
        "/path/to/data1.ms",
        "/path/to/data2.ms",
    ]
    mpirun_instances = [Mpirun(None), Mpirun([]), Mpirun(["single_node"])]
    for mpirun in mpirun_instances:
        assert wsclean_command.render_with_modifiers([mpirun]) == expected_args


def test_wsclean_command_rendering_with_singularity_exec_and_mpirun(
    wsclean_command: WSCleanCommand,
):
    """
    Test that WSCleanCommand with SingularityExec and Mpirun modifiers
    renders as expected.
    """
    sing = SingularityExec(Path("singularity_image.sif"))
    host_list = ["host_a", "host_b"]
    expected_hostfile_contents = "host_a slots=1\n" "host_b slots=1\n"
    mpirun = Mpirun(host_list)

    expected = [
        "mpirun",
        "--hostfile",
        str(mpirun.hostfile_path),
        "--bind-to",
        "none",
        "singularity",
        "exec",
        "--bind",
        "/path/to:/mnt/path/to",
        "singularity_image.sif",
        "wsclean-mp",
        "-multiscale",
        "-name",
        "final",
        "-niter",
        "42",
        "-scale",
        "1asec",
        "-size",
        "8192",
        "4096",
        "-weight",
        "briggs",
        "-0.5",
        "/mnt/path/to/data1.ms",
        "/mnt/path/to/data2.ms",
    ]
    assert wsclean_command.render_with_modifiers([sing, mpirun]) == expected
    assert mpirun.hostfile_path.read_text() == expected_hostfile_contents


@pytest.fixture
def dp3_command() -> DP3Command:
    """
    DP3Command used for testing rendering.
    """
    options = {
        "msin": [Path("/path/to/input1.ms"), Path("/path/to/input2.ms")],
        "msout": Path("/path/to/output.ms"),
        "steps": ["gaincal"],
        "gaincal.caltype": "scalarphase",
        "gaincal.solint": 50,
        "gaincal.tolerance": 1e-3,
        "gaincal.applysolution": True,
    }
    return DP3Command(options)


def test_dp3_command_rendering(dp3_command: DP3Command):
    """
    Test that DP3Command renders as expected.
    """
    expected = [
        "DP3",
        "gaincal.applysolution=true",
        "gaincal.caltype=scalarphase",
        "gaincal.solint=50",
        "gaincal.tolerance=0.001",
        "msin=[/path/to/input1.ms,/path/to/input2.ms]",
        "msout=/path/to/output.ms",
        "steps=[gaincal]",
    ]
    assert dp3_command.render() == expected


def test_dp3_command_rendering_with_singularity_exec(dp3_command: DP3Command):
    """
    Test that DP3Command with SingularityExec modifier renders as expected.
    """
    mod = SingularityExec(Path("singularity_image.sif"))
    expected = [
        "singularity",
        "exec",
        "--bind",
        "/path/to:/mnt/path/to",
        "singularity_image.sif",
        "DP3",
        "gaincal.applysolution=true",
        "gaincal.caltype=scalarphase",
        "gaincal.solint=50",
        "gaincal.tolerance=0.001",
        "msin=[/mnt/path/to/input1.ms,/mnt/path/to/input2.ms]",
        "msout=/mnt/path/to/output.ms",
        "steps=[gaincal]",
    ]
    assert dp3_command.render_with_modifiers([mod]) == expected
