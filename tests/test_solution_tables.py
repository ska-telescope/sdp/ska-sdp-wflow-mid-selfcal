# pylint: disable=redefined-outer-name
import contextlib
import os
import re
import tempfile
from pathlib import Path

import pytest

from ska_sdp_wflow_mid_selfcal.solution_tables import (
    SolutionFile,
    SolutionSet,
    concatenate_solution_files,
)


@pytest.fixture
def solution_paths_a() -> list[Path]:
    """
    One group of solution table paths on which to test concatenation.
    """
    thisdir = Path(os.path.dirname(__file__))
    tables_dir = thisdir / "data" / "solution_tables"
    return [
        path
        for path in tables_dir.iterdir()
        if re.match(r"solA.h5parm.part\d{3}", path.name)
    ]


@pytest.fixture
def solution_paths_b() -> list[Path]:
    """
    Another group of solution table paths on which to test concatenation.
    """
    thisdir = Path(os.path.dirname(__file__))
    tables_dir = thisdir / "data" / "solution_tables"
    return [
        path
        for path in tables_dir.iterdir()
        if re.match(r"solB.h5parm.part\d{3}", path.name)
    ]


@contextlib.contextmanager
def temporary_output_table_path():
    """
    Context manager that handles a temporary output file for concatenation
    tests.
    """
    with tempfile.TemporaryDirectory() as tempdir:
        yield Path(tempdir) / "output.h5parm"


# pylint:disable=too-many-arguments
def check_solution_set_layout(
    solset: SolutionSet,
    solset_name: str,
    soltab_name_type_mapping: dict[str, str],
    ntime: int,
    nfreq: int,
    nant: int,
    ndir: int,
) -> None:
    """
    Check that the layout of a solution set is as expected.
    `soltab_name_type_mapping` is a dictionary where the keys are the expected
    soltab names in the solution set, and the values the corresponding solution
    types (e.g. amplitude or phase).
    """
    assert solset.name == solset_name
    assert len(solset.antenna) == nant
    assert len(solset.source) == ndir

    name_type_mapping = {t.name: t.soltype for t in solset.solution_tables}
    assert name_type_mapping == soltab_name_type_mapping

    for tab in solset.solution_tables:
        assert tab.axes == ["time", "freq", "ant", "dir"]
        assert tab.ant.shape == (nant,)
        assert tab.dir.shape == (ndir,)
        assert tab.freq.shape == (nfreq,)
        assert tab.time.shape == (ntime,)
        assert tab.val.shape == (ntime, nfreq, nant, ndir)
        assert tab.weight.shape == (ntime, nfreq, nant, ndir)


def test_concatenate_solution_files_a(solution_paths_a: list[Path]):
    """
    Test concatenation of solution tables along the time axis.
    """
    with temporary_output_table_path() as output_path:
        concatenate_solution_files(solution_paths_a, output_path)

        file = SolutionFile(output_path)
        assert file.solution_table_names() == ["phase000"]

        solution_sets = file.solution_sets()
        assert len(solution_sets) == 1

        (solset,) = solution_sets
        check_solution_set_layout(
            solset,
            "/sol000",
            {"/sol000/phase000": "phase"},
            ntime=30,
            nfreq=16,
            nant=62,
            ndir=9,
        )


def test_concatenate_solution_files_b(solution_paths_b: list[Path]):
    """
    Test concatenation of solution tables along the time axis.
    """
    with temporary_output_table_path() as output_path:
        concatenate_solution_files(solution_paths_b, output_path)

        file = SolutionFile(output_path)
        assert file.solution_table_names() == ["phase000"]

        solution_sets = file.solution_sets()
        assert len(solution_sets) == 1

        (solset,) = solution_sets
        check_solution_set_layout(
            solset,
            "/sol000",
            {"/sol000/phase000": "phase"},
            ntime=30,
            nfreq=16,
            nant=62,
            ndir=4,
        )


def test_concatenate_solution_files_raises_if_mismatched_table_shapes(
    solution_paths_a: list[Path], solution_paths_b: list[Path]
):
    """
    Self-explanatory.
    """
    with pytest.raises(ValueError), temporary_output_table_path() as outpath:
        concatenate_solution_files(
            solution_paths_a + solution_paths_b, outpath
        )
