from dataclasses import dataclass
from typing import Final, Sequence

import numpy as np
import pytest
from numpy.typing import NDArray

from ska_sdp_wflow_mid_selfcal.voronoi import apply_voronoi
from tests.common import circular_sequences_close

SQRT_3: Final[float] = 3**0.5


@dataclass
class VoronoiCase:
    """
    Defines a test case for a boxed voronoi tesselation in cartesian coords.
    """

    name: str
    """Name of the test case to be printed in pytest output"""

    points: NDArray
    """Input points around which the voronoi cells will be defined"""

    bounds: list[float]
    """
    List of 4 floats defining the bounding box, in the following order:
    xmin, xmax, ymin, ymax
    """

    expected_polygons: list[NDArray]
    """
    List where the elements are arrays of shape (num_points, 2), each
    representing the vertices of a polygonal voronoi cell, enumerated along the
    edge of the polygon (in any order, clockwise or anti-clockwise).
    """

    def __post_init__(self):
        self.points = np.asarray(self.points, dtype=float)
        self.expected_polygons = [
            np.asarray(arr, dtype=float) for arr in self.expected_polygons
        ]


one_central_point = VoronoiCase(
    name="one_central_point",
    points=[(0.0, 0.0)],
    bounds=[-1, 2, -1, 2],
    expected_polygons=[[(-1, -1), (+2, -1), (+2, +2), (-1, +2)]],
)

two_xaxis_points = VoronoiCase(
    name="two_xaxis_points",
    points=[(-0.5, 0.0), (+0.5, 0.0)],
    bounds=[-2, 2, -1, 1],
    expected_polygons=[
        [(-2, -1), (0, -1), (0, 1), (-2, 1)],
        [(0, -1), (2, -1), (2, 1), (0, 1)],
    ],
)

# Three points at the following polar coordinates:
# (r = 0.5, theta = 0), (r = 0.5, theta = pi/3), (r = 0.5, theta = 2 pi / 3)
equilateral_triangle_in_unit_square = VoronoiCase(
    name="equilateral_triangle_in_unit_square",
    points=[(0.5, 0), (-0.25, SQRT_3 / 4), (-0.25, -SQRT_3 / 4)],
    bounds=[-1, 1, -1, 1],
    expected_polygons=[
        [(0, 0), (1 / SQRT_3, -1), (1, -1), (1, 1), (1 / SQRT_3, 1)],
        [(0, 0), (1 / SQRT_3, 1), (-1, 1), (-1, 0)],
        [(0, 0), (1 / SQRT_3, -1), (-1, -1), (-1, 0)],
    ],
)


four_quadrants = VoronoiCase(
    name="four_quadrants",
    points=[(-5, -5), (5, -5), (5, 5), (-5, 5)],
    bounds=[-10, 10, -10, 10],
    expected_polygons=[
        [(-10, -10), (0, -10), (0, 0), (-10, 0)],
        [(0, -10), (10, -10), (10, 0), (0, 0)],
        [(0, 0), (10, 0), (10, 10), (0, 10)],
        [(-10, 0), (0, 0), (0, 10), (-10, 10)],
    ],
)


voronoi_cases = [
    one_central_point,
    two_xaxis_points,
    equilateral_triangle_in_unit_square,
    four_quadrants,
]


voronoi_case_names = [c.name for c in voronoi_cases]


def polygons_close(poly_a: NDArray, poly_b: NDArray) -> bool:
    """
    Returns True if the vertices in `poly_a` are close to their counterparts
    in `poly_b`.
    """

    def _distance(point_a, point_b) -> float:
        delta_x, delta_y = point_b - point_a
        return np.hypot(delta_x, delta_y)

    return circular_sequences_close(poly_a, poly_b, _distance, tol=1e-9)


def polygon_sequences_close(
    seq_a: Sequence[NDArray], seq_b: Sequence[NDArray]
) -> bool:
    """
    Returns True if both sequences of polygons are close, element-wise.
    """
    return len(seq_a) == len(seq_b) and all(
        polygons_close(poly_a, poly_b) for poly_a, poly_b in zip(seq_a, seq_b)
    )


@pytest.mark.parametrize("vcase", voronoi_cases, ids=voronoi_case_names)
def test_voronoi(vcase: VoronoiCase):
    """
    Test that we get the expected boxed voronoi output on several test cases.
    """
    x_min, x_max, y_min, y_max = vcase.bounds

    polygons = apply_voronoi(
        vcase.points,
        x_min=x_min,
        y_min=y_min,
        x_max=x_max,
        y_max=y_max,
    )

    assert polygon_sequences_close(polygons, vcase.expected_polygons)
