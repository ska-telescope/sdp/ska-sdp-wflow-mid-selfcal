import shutil
import tempfile
from pathlib import Path
from typing import Optional
from zipfile import ZipFile

import pytest

import ska_sdp_wflow_mid_selfcal.apps.dd_pipeline as dd_pipeline
import ska_sdp_wflow_mid_selfcal.apps.fits2png as fits2png


@pytest.fixture(name="singularity_image")
def singularity_image(pytestconfig) -> Optional[str]:
    """
    Path to the optional singularity image inside which to run DP3 / WSClean.
    """
    return pytestconfig.getoption("--sif")


BASE_DIR = Path(__file__).parent.resolve()

SKY_MODEL_PATHS: list[Optional[Path]] = [
    None,
    Path(__file__).parent / "aa2_mid_pb_attenuated_top100.skymodel",
]

SKY_MODEL_IDS = [
    str(path.name) if path else "None" for path in SKY_MODEL_PATHS
]


@pytest.fixture(
    name="skymodel_path", params=SKY_MODEL_PATHS, ids=SKY_MODEL_IDS
)
def skymodel_path(request) -> Optional[str]:
    return request.param


def test_pipeline(
    singularity_image: Optional[str], skymodel_path: Optional[str]
):
    """
    Checks if the mid-self-calibration pipeline runs successfully.
    We also test additional support apps at the end, such as our own fits2png.

    NOTE: `pytestconfig` is the standard fixture name to access pytest's
    global config and command-line arguments.
    https://docs.pytest.org/en/7.1.x/reference/reference.html#pytestconfig
    """
    if not singularity_image:
        if not shutil.which("wsclean"):
            pytest.skip("--Could not find WSClean, skipping")
        if not shutil.which("DP3"):
            pytest.skip("--Could not find DP3, skipping")

    config = BASE_DIR / "aa2_mid_nano.yml"
    num_pixels = 1200
    pixel_scale = 4.0

    zipped_ms = BASE_DIR / "aa2_mid_nano.zip"

    with tempfile.TemporaryDirectory() as tmpdir, ZipFile(
        zipped_ms, "r"
    ) as zipfile:

        zipfile.extractall(tmpdir)
        input_ms = Path(tmpdir) / "aa2_mid_nano.ms"

        cmdline_arguments = [
            "--outdir",
            tmpdir,
            "--num-pixels",
            str(num_pixels),
            "--pixel-scale",
            str(pixel_scale),
            "--config",
            str(config),
            str(input_ms),
        ]
        if singularity_image:
            cmdline_arguments.extend(
                [
                    "--singularity-image",
                    singularity_image,
                ]
            )
        if skymodel_path:
            cmdline_arguments.extend(
                [
                    "--sky-model",
                    str(skymodel_path),
                ]
            )

        # Test pipeline
        assert dd_pipeline.run_program(cmdline_arguments) == 0
        expected_file_names = [
            "initial_skymodel.txt",
            "initial_skymodel.reg",
            "final_image.fits",
            "final_residual.fits",
            "final_skymodel.txt",
            "final_skymodel.reg",
        ]
        for fname in expected_file_names:
            path = Path(tmpdir) / fname
            assert path.is_file()

        # Test fits2png app
        final_image_path = Path(tmpdir) / "final_image.fits"
        assert fits2png.run_program([str(final_image_path)]) == 0
        assert final_image_path.with_suffix(".png").is_file()
