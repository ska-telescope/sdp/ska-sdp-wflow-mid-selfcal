from __future__ import annotations

import math
import tempfile
from dataclasses import dataclass
from pathlib import Path

import pytest

from ska_sdp_wflow_mid_selfcal.sourcedb import SourceDB


@dataclass
class SourceDBCase:
    """ """

    base_name: str
    """
    Base name of the test files.
    """

    sourcedb: SourceDB
    """
    SourceDB object parsed from <BASENAME>.txt, using the sourcedb parser.
    """

    expected_sourcedb: SourceDB
    """
    SourceDB object parsed from <BASENAME>.json, holds the reference data.
    """


def load_sourcedb_cases() -> list[SourceDBCase]:
    """
    Load all sourcedb parsing test cases, which come as a pair of files
    .txt (sourcedb format) and .json (expected parsed output).
    """
    data_dir = Path(__file__).parent / "data"
    txt_paths = [
        path
        for path in data_dir.iterdir()
        if path.is_file() and path.suffix == ".txt"
    ]
    base_names = [path.stem for path in txt_paths]
    json_paths = [path.with_suffix(".json") for path in txt_paths]

    return [
        SourceDBCase(
            base_name=base_name,
            sourcedb=SourceDB.load(txt_path),
            expected_sourcedb=SourceDB.from_json_file(json_path),
        )
        for base_name, txt_path, json_path in zip(
            base_names, txt_paths, json_paths
        )
    ]


SOURCEDB_CASES = load_sourcedb_cases()

SOURCEDB_CASE_NAMES = [case.base_name for case in SOURCEDB_CASES]


def patch_entries_close(
    patch: dict, other: dict, abs_tol: float = 1e-12
) -> bool:
    """
    True if two patch entries have the same 'Name' and 'Patch', and their
    'Ra' and 'Dec' are numerically close.
    """
    return (
        patch["Name"] == other["Name"]
        and patch["Patch"] == other["Patch"]
        and math.isclose(patch["Ra"], other["Ra"], abs_tol=abs_tol)
        and math.isclose(patch["Dec"], other["Dec"], abs_tol=abs_tol)
    )


def source_entries_close(
    source: dict, other: dict, abs_tol: float = 1e-12
) -> bool:
    """
    True if two patch entries have the same field names and values, except for
    their 'Ra' and 'Dec' fields which only need be numerically close.
    """
    assert source.keys() == other.keys()
    non_radec_keys = set(source.keys()).difference({"Ra", "Dec"})
    return (
        all(source[k] == other[k] for k in non_radec_keys)
        and math.isclose(source["Ra"], other["Ra"], abs_tol=abs_tol)
        and math.isclose(source["Dec"], other["Dec"], abs_tol=abs_tol)
    )


def assert_sourcedb_close(
    sourcedb: SourceDB, other: SourceDB, abs_tol: float = 1e-12
) -> None:
    """
    Assert that all patch and source entries for both given SourceDB objects
    are numerically close to one another.
    """
    assert len(sourcedb.patches) == len(other.patches)
    for patch, other_patch in zip(sourcedb.patches, other.patches):
        assert patch_entries_close(patch, other_patch, abs_tol=abs_tol)

    assert len(sourcedb.sources) == len(other.sources)
    for source, other_source in zip(sourcedb.sources, other.sources):
        assert source_entries_close(source, other_source, abs_tol=abs_tol)


@pytest.mark.parametrize("case", SOURCEDB_CASES, ids=SOURCEDB_CASE_NAMES)
def test_sourcedb_load(case: SourceDBCase):
    """
    Check that for each case, the data parsed from the sourcedb files (.txt)
    is the same as what is expected, as given by the corresponding .json file.
    """
    assert_sourcedb_close(case.sourcedb, case.expected_sourcedb)


@pytest.mark.parametrize("case", SOURCEDB_CASES, ids=SOURCEDB_CASE_NAMES)
def test_sourcedb_save(case: SourceDBCase):
    """
    Check that the sequence (load, save, load) produces the same result
    as just load.
    """
    with tempfile.TemporaryDirectory() as tempdir:
        path = Path(tempdir) / "sourcedb.txt"
        case.sourcedb.save(path)
        reloaded_sourcedb = SourceDB.load(path)
        assert_sourcedb_close(reloaded_sourcedb, case.expected_sourcedb)
