## Notes on testing SourceDB code

The `data` directory contains pairs of files:
- One sourcedb file to test on, with extension `.txt`
- One JSON file containing the expected parsed output, with extension `.json`

