from pathlib import Path

from ska_sdp_wflow_mid_selfcal.cleanup import run_cleanup


def test_cleanup(tmp_path):
    """
    Test that the expected files are removed when calling cleanup
    on a typical output directory written by the pipeline.
    """
    outdir = Path(tmp_path)
    subdir = outdir / "subdir"
    subdir.mkdir()

    # Create .tmp file
    tmp_file = subdir / "file.tmp"
    tmp_file.touch()

    # Create mock calibration solution chunk
    sol_chunk = subdir / "solutions.h5parm.part042"
    sol_chunk.touch()

    # Create mock intermediate FITS files
    fits_files = [
        subdir / "hello-0007-residual.fits",
        subdir / "world-psf-0042-tmp.fits",
    ]
    for file in fits_files:
        file.touch()

    # Check that test setup is correct
    assert tmp_file.exists()
    assert sol_chunk.exists()
    for file in fits_files:
        assert file.exists()

    # Now test cleanup
    run_cleanup(outdir)
    assert not tmp_file.exists()
    assert not sol_chunk.exists()
    for file in fits_files:
        assert not file.exists()
