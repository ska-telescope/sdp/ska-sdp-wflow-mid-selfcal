"""
Test the whole process of creating a tesselation of the field and assigning
sources to calibration patches.
"""
from typing import Sequence

import astropy.units as u
from astropy.coordinates import SkyCoord

from ska_sdp_wflow_mid_selfcal.calibration_sky_model import CalibrationSkyModel
from ska_sdp_wflow_mid_selfcal.field import Field
from ska_sdp_wflow_mid_selfcal.skymodel import SkyModel
from ska_sdp_wflow_mid_selfcal.sourcedb import SourceDB
from ska_sdp_wflow_mid_selfcal.tesselation import Tesselation

# Define 3 bright sources around North pole that will be our voronoi
# centres. Define 3 faint sources that we know for sure will end up in
# each of the 3 patches around the bright sources. Add some very bright sources
# that are just outside the field and should be ignored.
INPUT_SOURCEDB_TEXT = """
FORMAT = Name, Type, Patch, Ra, Dec, I, SpectralIndex, ReferenceFrequency, LogarithmicSI
bright_A, POINT, , 0.000deg, 89.000deg, 30.0, [0.0], 950000000.0, true
bright_B, POINT, , 120.000deg, 89.000deg, 20.0, [0.0], 950000000.0, true
bright_C, POINT, , 240.000deg, 89.000deg, 10.0, [0.0], 950000000.0, true
faint_A, POINT, , 30.000deg, 89.500deg, 3.0, [0.0], 950000000.0, true
faint_B, POINT, , 150.000deg, 89.500deg, 2.0, [0.0], 950000000.0, true
faint_C, POINT, , 270.000deg, 89.500deg, 1.0, [0.0], 950000000.0, true
outside_1, POINT, , 0.000deg, 87.499deg, 1984.0, [0.0], 950000000.0, true
outside_2, POINT, , 120.000deg, 87.499deg, 1789.0, [0.0], 950000000.0, true
"""  # noqa: E501

# 2.5 degrees = 9000 asec FoV imaging field around North pole
FIELD = Field(
    imaging_centre=SkyCoord(0.0, 90.0, unit=(u.deg, u.deg)),
    num_pixels=9000.0,
    pixel_scale_asec=1.0,
)

NUM_PATCHES = 3

# Expected behaviour:
# Sources tagged A, B, C end up respectively in patches 0, 1, 2
# Patches 0,1,2 calibration directios are the coordinates fo bright sources
# A, B, C.
# Sources outside field don't appear in the output
# MinorAxis, MajorAxis, Orientation keys are always added by our SkyModel to
# SourceDB implementation.
EXPECTED_OUTPUT_SOURCEDB_TEXT = """
FORMAT = Name, Type, Patch, Ra, Dec, I, SpectralIndex, ReferenceFrequency, LogarithmicSI, MinorAxis, MajorAxis, Orientation

, , patch_000, 0.000deg, 89.000deg
, , patch_001, 120.000deg, 89.000deg
, , patch_002, 240.000deg, 89.000deg

bright_A, POINT, patch_000, 0.000deg, 89.000deg, 30.0, [0.0], 950000000.0, true, 0.0, 0.0, 0.0
bright_B, POINT, patch_001, 120.000deg, 89.000deg, 20.0, [0.0], 950000000.0, true, 0.0, 0.0, 0.0
bright_C, POINT, patch_002, 240.000deg, 89.000deg, 10.0, [0.0], 950000000.0, true, 0.0, 0.0, 0.0
faint_A, POINT, patch_000, 30.000deg, 89.500deg, 3.0, [0.0], 950000000.0, true, 0.0, 0.0, 0.0
faint_B, POINT, patch_001, 150.000deg, 89.500deg, 2.0, [0.0], 950000000.0, true, 0.0, 0.0, 0.0
faint_C, POINT, patch_002, 270.000deg, 89.500deg, 1.0, [0.0], 950000000.0, true, 0.0, 0.0, 0.0
"""  # noqa: E501

# NOTE: We won't be comparing sourcedb text directly, but instead the
# SourceDB objects, to dodge some of the sourcedb text format quirks.
INPUT_SOURCEDB = SourceDB.from_sourcedb_text(INPUT_SOURCEDB_TEXT.strip())
EXPECTED_OUTPUT_SOURCEDB = SourceDB.from_sourcedb_text(
    EXPECTED_OUTPUT_SOURCEDB_TEXT.strip()
)


def assert_dict_sets_equal(
    left: Sequence[dict], right: Sequence[dict]
) -> bool:
    """
    Assert that all dicts in one sequence appear in the other.
    """
    items = [tuple(sorted(d.items())) for d in left]
    other_items = [tuple(sorted(d.items())) for d in right]
    assert set(items) == set(other_items)


def test_calibration_sky_model_end_to_end_creation():
    """
    Test the whole process of creating a tesselation of the field and
    assigning sources to calibration patches.
    """
    sky_model = SkyModel.from_sourcedb(INPUT_SOURCEDB)
    tesselation = Tesselation.voronoi_brightest(FIELD, sky_model, NUM_PATCHES)
    csm = CalibrationSkyModel(sky_model, tesselation)
    output_sourcedb = csm.as_sourcedb()

    assert_dict_sets_equal(
        output_sourcedb.sources, EXPECTED_OUTPUT_SOURCEDB.sources
    )
    assert_dict_sets_equal(
        output_sourcedb.patches, EXPECTED_OUTPUT_SOURCEDB.patches
    )
