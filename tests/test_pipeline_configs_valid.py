"""
Test that example pipeline config files provided in the dedicated directory
are all valid.
"""

from pathlib import Path

import pytest

from ska_sdp_wflow_mid_selfcal.apps.validate_config import run_program
from ska_sdp_wflow_mid_selfcal.config import validate_config_file


def get_config_dir() -> Path:
    """
    Path where the example config files are stored.
    """
    module_basedir = Path(__file__).parent.parent
    return module_basedir / "config"


CONFIG_DIR = get_config_dir()


def get_config_filenames() -> list[str]:
    """
    List of config filenames (without leading path) to be tested.
    """
    return [path.name for path in get_config_dir().glob("*.yml")]


CONFIG_FILENAMES = get_config_filenames()


@pytest.mark.parametrize("filename", CONFIG_FILENAMES, ids=CONFIG_FILENAMES)
def test_config_file_valid(filename: str):
    """
    Self-explanatory.
    """
    config_path = CONFIG_DIR / filename
    validate_config_file(config_path)


@pytest.mark.parametrize("filename", CONFIG_FILENAMES, ids=CONFIG_FILENAMES)
def test_config_file_valid_via_cli(filename: str):
    """
    Same thing, via the validation command-line app.
    """
    config_path = CONFIG_DIR / filename
    exitcode = run_program([str(config_path)])
    assert exitcode == 0
