# pylint: disable=redefined-outer-name
import astropy.units as u
import numpy as np
import pytest
from astropy.coordinates import SkyCoord
from numpy.typing import NDArray

from ska_sdp_wflow_mid_selfcal.coordinate_conversions import (
    orthographic_from_sky,
    sky_from_orthographic,
)


@pytest.fixture
def origin() -> SkyCoord:
    """
    Point on sky around which we're testing tangent plane coordinate
    conversions. We pick something close to the North pole which is more
    likely to trigger problems.
    """
    return SkyCoord(5.0, 89.0, unit=(u.deg, u.deg))


@pytest.fixture
def coords_sky(origin: SkyCoord) -> SkyCoord:
    """
    Define a set of test points on sky from `origin`. We do this by specifying
    angular offsets towards various directions defined by a position angle
    East of North (0 deg = towards North, 90 deg = towards East of North).
    """
    pos_and_sep = np.asarray(
        [
            # origin
            (0.0, 0.0),
            # 30 degrees offset towards north
            (0.0, 30.0),
            # 45 degrees offset towards east of north
            (90.0, 45.0),
            (120.0, 60.0),
            (240.0, 90.0),
        ]
    )
    pos, sep = pos_and_sep.T
    return origin.directional_offset_by(pos * u.deg, sep * u.deg)


@pytest.fixture
def coords_orthographic() -> NDArray:
    """
    The orthographic tangent plane coordinates that correspond to `coords_sky`
    and `origin`.
    """
    half_sqrt_2 = 2**0.5 / 2
    half_sqrt_3 = 3**0.5 / 2
    return np.asarray(
        [
            (0.0, 0.0),
            (0.0, 0.5),
            (half_sqrt_2, 0.0),
            (half_sqrt_3**2, -half_sqrt_3 / 2),
            (-half_sqrt_3, -0.5),
        ]
    )


def test_orthographic_from_sky(
    coords_sky: SkyCoord, coords_orthographic: NDArray, origin: SkyCoord
):
    """
    Self-explanatory.
    """
    result = orthographic_from_sky(coords_sky, origin)
    assert np.allclose(result, coords_orthographic, atol=1e-14)


def test_sky_from_orthographic(
    coords_sky: SkyCoord, coords_orthographic: NDArray, origin: SkyCoord
):
    """
    Self-explanatory.
    """
    result = sky_from_orthographic(coords_orthographic, origin)
    sep_radians = coords_sky.separation(result).to(u.rad).value
    assert np.allclose(sep_radians, 0.0, atol=1e-14)
