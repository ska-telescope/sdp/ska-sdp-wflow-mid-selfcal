import os
import re
import tempfile
from pathlib import Path
from typing import Iterator

import pytest

from ska_sdp_wflow_mid_selfcal.output_directory_creation import (
    create_output_directory,
)


@pytest.fixture(name="tempdir")
def fixture_tempdir() -> Iterator[str]:
    """
    Context manager creating a providing a temporary directory for the tests
    below.
    """
    with tempfile.TemporaryDirectory() as temp:
        yield temp


def test_output_directory_creation_none(tempdir: str):
    """
    Test create_output_directory(None)
    """
    cwd_before = Path.cwd()
    try:
        os.chdir(tempdir)
        outdir = create_output_directory(None)
        assert outdir.is_dir()
        assert outdir.parent == Path(tempdir)
        assert re.match(r"selfcal_\d{8}_\d{6}_\d{6}", outdir.name)
    finally:
        os.chdir(cwd_before)


def test_output_directory_creation_existing_file(tempdir: str):
    """
    Test create_output_directory() on a path to an existing file
    """
    file_path = Path(tempdir) / "somefile"
    file_path.touch()
    with pytest.raises(ValueError):
        create_output_directory(str(file_path))


def test_output_directory_creation_existing_directory(tempdir: str):
    """
    Test create_output_directory() on a path to an existing directory
    """
    dir_path = Path(tempdir) / "already/exists"
    dir_path.mkdir(parents=True)
    outdir = create_output_directory(str(dir_path))
    assert outdir.is_dir()
    assert outdir == dir_path


def test_output_directory_creation_non_existent_path(tempdir: str):
    """
    Test create_output_directory() on a path that does not exist
    """
    dir_path = Path(tempdir) / "does/not/exist"
    outdir = create_output_directory(str(dir_path))
    assert outdir.is_dir()
    assert outdir == dir_path
