# pylint: disable=redefined-outer-name
import pytest

from ska_sdp_wflow_mid_selfcal.skymodel import (
    FluxModel,
    Shape,
    SkyModel,
    Source,
)


@pytest.fixture
def basic_sky_model() -> SkyModel:
    """
    Simple sky model for testing DS9 region file export.
    """
    flux_model = FluxModel(
        stokes_i=0.1,
        reference_frequency_hz=1420000000.0,
        spectral_index=(2.0, 0.0),
        logarithmic_si=False,
    )
    shape = Shape.point()

    source_1 = Source(
        name="source_1",
        ra_deg=0.0,
        dec_deg=0.0,
        shape=shape,
        flux_model=flux_model,
    )
    source_2 = Source(
        name="source_2",
        ra_deg=1.0,
        dec_deg=-1.0,
        shape=shape,
        flux_model=flux_model,
    )
    return SkyModel(sources=[source_1, source_2])


@pytest.fixture
def expected_region_file_contents() -> str:
    """
    Self-explanatory.
    """
    lines = [
        'global font="helvetica 12 bold roman"',
        "fk5",
        'point(0.0,0.0) # color=#00ddff text="source_1 (100.0 mJy)"',
        'point(1.0,-1.0) # color=#00ddff text="source_2 (100.0 mJy)"',
    ]
    return "\n".join(lines) + "\n"


def test_export_tesselation_to_ds9_format(
    basic_sky_model: SkyModel, expected_region_file_contents: str
):
    """
    Self-explanatory.
    """
    assert basic_sky_model.as_ds9_text() == expected_region_file_contents
