# pylint: disable=redefined-outer-name
import numpy as np
import pytest
from numpy.typing import NDArray

from ska_sdp_wflow_mid_selfcal.dbscan import simple_dbscan


@pytest.fixture
def points() -> NDArray:
    """
    Test points arranged in three compact clusters, one of which is far away
    from the other two. That way, we can test the behaviour of the algorithm
    with different `radius` parameters.
    """
    compact_cluster = [
        [-0.06, +0.07],
        [-0.01, -0.09],
        [+0.02, +0.05],
    ]
    compact_cluster = np.asarray(compact_cluster)
    offset_2 = np.asarray([(1.0, 0.0)])
    offset_3 = np.asarray([(0.0, 10.0)])

    cluster_1 = compact_cluster  # indices 0,1,2
    cluster_2 = compact_cluster + offset_2  # indices 3,4,5
    cluster_3 = compact_cluster + offset_3  # indices 6,7,8. Further away.
    return np.concatenate((cluster_1, cluster_2, cluster_3), axis=0)


def assert_clustering_results_equal(
    result: list[NDArray], expected_result: list[NDArray]
):
    """
    Check that clustering results match expectation.
    """
    assert len(result) == len(expected_result), "Different numbers of clusters"
    for indices in result:
        match_found = False
        for other_indices in expected_result:
            if set(indices) == set(other_indices):
                match_found = True
                break
        assert match_found, f"Cluster {indices} has no match"


def test_simple_dbscan_one_cluster(points: NDArray):
    """
    Run simple_dbscan() with a radius parameter such that a single output
    cluster is expected.
    """
    num_points, __ = points.shape
    clusters = simple_dbscan(points, radius=20.0)
    expected_clusters = [np.arange(num_points)]
    assert_clustering_results_equal(clusters, expected_clusters)


def test_simple_dbscan_two_clusters(points: NDArray):
    """
    Same as above, expecting two clusters.
    """
    clusters = simple_dbscan(points, radius=2.0)
    expected_clusters = [
        np.asarray([0, 1, 2, 3, 4, 5]),
        np.asarray([6, 7, 8]),
    ]
    assert_clustering_results_equal(clusters, expected_clusters)


def test_simple_dbscan_three_clusters(points: NDArray):
    """
    Same as above, expecting three clusters.
    """
    clusters = simple_dbscan(points, radius=0.2)
    expected_clusters = [
        np.asarray([0, 1, 2]),
        np.asarray([3, 4, 5]),
        np.asarray([6, 7, 8]),
    ]
    assert_clustering_results_equal(clusters, expected_clusters)


def test_simple_dbscan_nine_clusters(points: NDArray):
    """
    Same as above, expecting nine clusters.
    """
    num_points, __ = points.shape
    clusters = simple_dbscan(points, radius=1.0e-3)
    expected_clusters = [np.asarray([i]) for i in range(num_points)]
    assert_clustering_results_equal(clusters, expected_clusters)


def test_simple_dbscan_on_empty_input():
    """
    Test that no errors are raised on empty input.
    """
    assert not simple_dbscan([], radius=1.0)
