# pylint: disable=redefined-outer-name
import astropy.units as u
import numpy as np
import pytest
from astropy.coordinates import SkyCoord
from astropy.coordinates import concatenate as skycoord_concatenate
from numpy.typing import NDArray

from ska_sdp_wflow_mid_selfcal.coordinate_conversions import (
    sky_from_orthographic,
)
from ska_sdp_wflow_mid_selfcal.field import Field
from ska_sdp_wflow_mid_selfcal.skymodel import (
    FluxModel,
    Shape,
    SkyModel,
    Source,
)
from ska_sdp_wflow_mid_selfcal.tesselation import Patch, Tesselation
from tests.common import circular_sequences_close


@pytest.fixture
def basic_tesselation() -> list[Patch]:
    """
    Tesselation to save to DS9 region file.
    """

    def _radec(ra, dec) -> SkyCoord:
        return SkyCoord(ra, dec, unit=(u.deg, u.deg))

    calibration_directions = [
        _radec(0.5, 0.0),  # east
        _radec(1.5, 0.0),  # west
    ]

    polygons_sky = [
        _radec([1.0, 0.0, 0.0, 1.0], [1.0, 1.0, -1.0, -1.0]),  # east
        _radec([1.0, 2.0, 2.0, 1.0], [1.0, 1.0, -1.0, -1.0]),  # west
    ]

    origin = _radec(0.0, 0.0)
    return Tesselation(calibration_directions, polygons_sky, origin)


@pytest.fixture
def expected_region_file_contents() -> str:
    """
    Self-explanatory.
    """
    lines = [
        'global color=green width=1 font="helvetica 12 normal roman"',
        "fk5",
        "polygon(1.,1.,0.,1.,0.,-1.,1.,-1.)",
        'point(0.5,0.) # text="patch_000"',
        "polygon(1.,1.,2.,1.,2.,-1.,1.,-1.)",
        'point(1.5,0.) # text="patch_001"',
    ]
    return "\n".join(lines) + "\n"


def test_export_tesselation_to_ds9_format(
    basic_tesselation: Tesselation, expected_region_file_contents: str
):
    """
    Self-explanatory.
    """
    assert basic_tesselation.as_ds9_text() == expected_region_file_contents


@pytest.fixture
def field() -> Field:
    """
    Field to be used in Tesselation creation test. We choose a centre close to
    a celestial pole and a wide field of view for extra chances to trigger
    edge cases.
    """
    centre = SkyCoord(5.0, 89.0, unit=(u.deg, u.deg))
    return Field(centre, num_pixels=216_000, pixel_scale_asec=1.0)


@pytest.fixture
def bright_source_coords_lm(field: Field) -> NDArray:
    """
    Bright source coordinates in tangent plane to be used in Tesselation
    creation test. We expect calibration patches to be defined around these
    source positions.
    """
    # Place our bright sources such that they create patch boundaries
    # aligned with E-W and N-S directions
    x = field.l_max / 2
    y = field.m_max / 2
    return np.asarray([(x, y), (-x, y), (-x, -y), (x, -y)])


@pytest.fixture
def patch_polygons_lm(bright_source_coords_lm: NDArray) -> list[NDArray]:
    """
    List of arrays of polygon vertices in tangent plane. These are the
    (l, m) coordinates of the calibration patches that we expect.
    """
    polygons = []
    for x, y in bright_source_coords_lm:
        poly = np.asarray(
            [(0.0, 0.0), (2 * x, 0.0), (2 * x, 2 * y), (0.0, 2 * y)]
        )
        polygons.append(poly)
    return polygons


@pytest.fixture
def bright_source_coords_sky(
    field: Field, bright_source_coords_lm: NDArray
) -> SkyCoord:
    """
    Sky coordinates of the bright sources whose tangent plane positions have
    been defined above.
    """
    return sky_from_orthographic(bright_source_coords_lm, field.imaging_centre)


@pytest.fixture
def outside_source_coords_sky(field: Field) -> SkyCoord:
    """
    Sky coordinates of additional sources that lie just outside of the field.
    These are use to test that the Tesselation code ignores them and won't
    define calibration patches around them.
    """
    epsilon = 1e-6
    x = field.l_max + epsilon
    y = field.m_max + epsilon
    coords_lm = np.asarray([(x, 0), (0, y), (-x, 0), (0, -y)])
    return sky_from_orthographic(coords_lm, field.imaging_centre)


def make_point_source(name: str, coord: SkyCoord, flux_jy: float) -> Source:
    """
    Convenience method to create a point source.
    """
    shape = Shape.point()

    def _make_flux_model(flux_jy: float) -> FluxModel:
        return FluxModel(
            stokes_i=flux_jy,
            reference_frequency_hz=1.0e9,
            spectral_index=(0.0,),
            logarithmic_si=True,
        )

    return Source(
        name=name,
        ra_deg=coord.ra.to(u.deg).value,
        dec_deg=coord.dec.to(u.deg).value,
        shape=shape,
        flux_model=_make_flux_model(flux_jy),
    )


@pytest.fixture
def sky_model_bright_sources(bright_source_coords_sky: SkyCoord) -> SkyModel:
    """
    Sky model containing four point sources at `bright_source_coords_sky`
    """
    bright_sources = [
        make_point_source(f"bright_{index}", coord, flux_jy=1.0)
        for index, coord in enumerate(bright_source_coords_sky)
    ]
    return SkyModel(bright_sources)


@pytest.fixture
def sky_model_outside_sources(outside_source_coords_sky: SkyCoord) -> SkyModel:
    """
    Sky model containing four point sources at `outside_source_coords`.
    These are barely outside the field and brighter than the "bright sources"
    defined above.
    """
    outside_sources = [
        make_point_source(f"outside_{index}", coord, flux_jy=2.0)
        for index, coord in enumerate(outside_source_coords_sky)
    ]
    return SkyModel(outside_sources)


@pytest.fixture
def sky_model(
    sky_model_bright_sources: SkyModel, sky_model_outside_sources: SkyModel
) -> SkyModel:
    """
    Sky model to be used in Tesselation creation test.

    The sky model includes four point sources at `bright_source_coords_sky` and
    additional, even brighter point sources at `outside_source_coords`.
    """
    return sky_model_bright_sources + sky_model_outside_sources


@pytest.fixture
def expected_tesselation(
    field: Field,
    bright_source_coords_lm: NDArray,
    patch_polygons_lm: list[NDArray],
) -> Tesselation:
    """
    The Tesselation that we expect to be created. We will NOT compare patch
    names.
    """
    calibration_directions = skycoord_concatenate(
        [
            sky_from_orthographic(centre_lm, field.imaging_centre)
            for centre_lm in bright_source_coords_lm
        ]
    )
    polygons_sky = [
        sky_from_orthographic(polygon_lm, field.imaging_centre)
        for polygon_lm in patch_polygons_lm
    ]
    return Tesselation(
        calibration_directions, polygons_sky, field.imaging_centre
    )


def patches_close(
    patch: Patch, other_patch: Patch, tol_arcsec: float = 1e-6
) -> bool:
    """
    Check if given patches are within `tol_arcsec` arcseconds of each other.
    Both their associated calibration directions and vertices must differ by
    no more than that tolerance.
    """

    def distance_arcsec(coord_a: SkyCoord, coord_b: SkyCoord) -> float:
        val = coord_a.separation(coord_b).to(u.arcsec).value
        return val

    centres_dist = distance_arcsec(
        patch.calibration_direction, other_patch.calibration_direction
    )
    if not centres_dist <= tol_arcsec:
        return False
    return circular_sequences_close(
        patch.vertices, other_patch.vertices, distance_arcsec, tol_arcsec
    )


def assert_tesselations_close(
    tess: Tesselation, other_tess: Tesselation
) -> None:
    """
    Assert that two tesselations are close. That means:
    - Same number of patches
    - Every patch in the first one has a close counterpart in the other
    """
    assert len(tess) == len(other_tess)

    def _has_close_counterpart(patch: Patch) -> bool:
        for other_patch in other_tess:
            if patches_close(patch, other_patch):
                return True
        return False

    for patch in tess:
        if not _has_close_counterpart(patch):
            raise AssertionError(
                f"{patch} has no close counterpart in other tesselation"
            )


def test_tesselation_voronoi_brightest(
    field: Field, sky_model: SkyModel, expected_tesselation: Tesselation
):
    """
    Test that we get the expected Tesselation using the voronoi_brightest
    method.
    """
    tesselation = Tesselation.create(
        field, sky_model, num_patches=4, method="voronoi_brightest"
    )
    assert_tesselations_close(tesselation, expected_tesselation)


def test_tesselation_square_grid(
    field: Field, sky_model: SkyModel, expected_tesselation: Tesselation
):
    """
    Test that we get the same Tesselation using the square_grid method. We have
    designed the test case specifically for that to be true.
    """
    tesselation = Tesselation.create(
        field, sky_model, num_patches=4, method="square_grid"
    )
    assert_tesselations_close(tesselation, expected_tesselation)


def test_tesselation_voronoi_brightest_raises_if_no_sources_in_field(
    field: Field, sky_model_outside_sources: SkyModel
):
    """
    Test that the expected error is raised when no bright sources are in the
    field to create a voronoi tesselation.
    """
    with pytest.raises(ValueError):
        Tesselation.create(
            field,
            sky_model_outside_sources,
            num_patches=4,
            method="voronoi_brightest",
        )
