from ska_sdp_wflow_mid_selfcal.imaging import (
    compute_min_deconv_channels,
    compute_wideband_options,
)


def test_compute_min_deconv_channels():
    """
    Test the code that calculates the minimum acceptable value for the
    `-deconvolution-channels` option in WSClean.
    """
    test_cases = {
        # MeerKAT full band
        (856.0, 856.0, 1024, 0.05): 20,
        # MeerKAT 1/8-th band
        (856.0, 107.0, 128, 0.05): 3,
        # Check that result does not exceed total num channels, even if
        # max_fractional_bandwidth is tiny
        (856.0, 107.0, 128, 0.00001): 128,
        # Check that result is at least 1, even for tiny fractional bandwidths
        (856.0, 0.001, 128, 0.05): 1,
    }

    for args, expected_result in test_cases.items():
        bottom_freq, bandwidth, num_channels, max_fractional_bandwidth = args
        result = compute_min_deconv_channels(
            bottom_freq,
            bandwidth,
            num_channels,
            max_fractional_bandwidth=max_fractional_bandwidth,
        )
        assert isinstance(result, int)
        assert result == expected_result


def test_compute_wideband_options():
    """
    Test the code that calculates the WSClean options deconvolution-channels
    and channels-out.
    """
    # Parameters for MeerKAT full band
    bottom_freq = 856.0
    channel_width = 0.8359375

    for num_channels in (1, 16, 64, 256, 1024):
        total_bandwidth = num_channels * channel_width

        min_deconv_chans = compute_min_deconv_channels(
            bottom_freq,
            total_bandwidth,
            num_channels,
            max_fractional_bandwidth=0.05,
        )

        for num_nodes in (1, 2, 3, 4, 5, 6, 8, 10, 12, 16, 20, 24, 32):
            opts = compute_wideband_options(
                channels_in=num_channels,
                min_deconv_chans=min_deconv_chans,
                num_nodes=num_nodes,
            )

            # Allow result to be empty, but only if min_deconv_chans is 1
            if min_deconv_chans > 1:
                assert opts
            if not opts:
                continue

            # Make sure we have the expected options and values
            assert 1 <= opts["channels-out"] <= num_channels
            assert 1 <= opts["deconvolution-channels"] <= opts["channels-out"]
            assert (
                1 <= opts["fit-spectral-pol"] <= opts["deconvolution-channels"]
            )
            assert (
                min_deconv_chans
                <= opts["deconvolution-channels"]
                <= 2 * min_deconv_chans
            )
            assert (
                isinstance(opts["join-channels"], bool)
                and opts["join-channels"]
            )
