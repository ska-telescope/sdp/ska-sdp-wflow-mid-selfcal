import numpy as np

from ska_sdp_wflow_mid_selfcal.point_in_polygon import point_in_polygon


def test_point_in_polygon():
    """
    Test the point to polygon matching code that operates in planar geometry.

    We use a minimal setup to trigger all the edge cases of the algorithm,
    with 5-vertex polygon that is concave and has one edge aligned with the X
    axis, and another with the Y axis. Something that looks like:

    ```
            + B    + C



            + E

     + A           + D
    ```
    """
    poly = np.asarray(
        [
            (-1, 0),  # A
            (0, 3),  # B
            (1, 3),  # C
            (1, 0),  # D
            (0, 1),  # E
        ]
    )

    # All vertices are inside by definition
    for x, y in poly:
        assert point_in_polygon(x, y, poly)

    # Examples of points inside
    assert point_in_polygon(0, 1.5, poly)
    assert point_in_polygon(0.8, 2.4, poly)
    assert point_in_polygon(-0.2, 1.2, poly)

    # Examples of points outside
    assert not point_in_polygon(-10, 1, poly)
    assert not point_in_polygon(0.0, 0.999, poly)
    assert not point_in_polygon(1.001, 0.0, poly)

    # This one tests an edge case where the "casted ray" ends up covering
    # a segment parallel with the X axis, in this case [BC]
    assert not point_in_polygon(-10, 3, poly)

    # Examples of points on edge
    assert point_in_polygon(0.5, 0.5, poly)  # in [AE]
