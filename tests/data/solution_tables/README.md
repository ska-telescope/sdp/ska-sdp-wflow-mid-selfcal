`solA` files come from a DDECal run in `scalarphase` mode on the 1-hour, 16-channel, 62-antenna AA2 Mid dataset using 9 facets.

`solB` files come from a DDECal run in `scalarphase` mode on the same dataset, but using only 4 facets.
