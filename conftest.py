def pytest_addoption(parser):
    parser.addoption(
        "--sif",
        action="store",
        help=(
            "Optional path to a singularity image containing DP3 and WSClean. "
            "If specified, then during the integration test, the pipeline "
            "will run DP3 and WSClean inside a container based on that image."
        ),
    )
